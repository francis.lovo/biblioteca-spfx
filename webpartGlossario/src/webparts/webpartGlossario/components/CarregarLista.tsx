/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";

interface ILista {
  titulo: string;
  descricao: string;
  img: string;
}

interface ICarregarLista {
  setItems: any;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const CarregarLista = ({ setItems }: ICarregarLista) => {
  React.useEffect(() => {
    async function getNotification() {
      const webUrl =
        "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('Glossario')/items";

      try {
        const response = await fetch(webUrl, {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        });

        if (response.ok) {
          const aux: ILista[] = [];
          const data = await response.json();

          console.log("Lista: ", data.d.results);

          data.d.results.forEach((result: any) => {
            console.log("JSON: ", JSON.parse(result.Imagem));

            const jsonImg = JSON.parse(result.Imagem);

            aux.push({
              titulo: result.Title,
              descricao: result.Descricao,
              img:
                jsonImg && jsonImg.serverUrl
                  ? jsonImg.serverUrl.concat(jsonImg.serverRelativeUrl)
                  : undefined,
            });
          });
          setItems(aux);
        } else {
          console.error("Falha na requisição");
        }
      } catch (error) {
        console.error("Erro na requisição: ", error);
      }
    }
    getNotification();
  }, []);

  return <></>;
};
