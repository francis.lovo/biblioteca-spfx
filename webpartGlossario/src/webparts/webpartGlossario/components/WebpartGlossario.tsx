/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { IWebpartGlossarioProps } from "./IWebpartGlossarioProps";
import { ArqText, ArqGlossary, ArqModal } from "biblioteca-portal";
import { CarregarLista } from "./CarregarLista";

interface ILista {
  titulo: string;
  descricao: string;
  img: string;
}

export default class WebpartGlossario extends React.Component<
  IWebpartGlossarioProps,
  {}
> {
  state = {
    itemClicked: {
      titulo: undefined,
      descricao: undefined,
      img: undefined,
    },

    items: [],
    itemsNames: [],
    openModal: false,
    carregado: false,
  };
  popularArray = (): string[] => {
    const aux: string[] = [];
    this.state.items.forEach((item: any) => {
      aux.push(item.titulo);
    });
    return aux;
  };
  componentDidUpdate(): void {
    if (
      this.state.items &&
      this.state.items.length > 0 &&
      !this.state.carregado
    ) {
      this.setState({ itemsNames: this.popularArray(), carregado: true });
    }
  }
  public render(): React.ReactElement<IWebpartGlossarioProps> {
    const onClickItem = (itemClicked: string): void => {
      this.state.items.forEach((item: ILista) => {
        if (item.titulo === itemClicked) {
          this.setState({ itemClicked: item });
        }
      });
      this.setState({ openModal: true });
    };
    const closeModal = (): void => {
      this.setState({ openModal: false });
    };
    const setItems = (items: ILista): void => {
      this.setState({ items: items });
    };

    return (
      <div>
        <div
              style={{
                background: "#CC092f",
                padding: "10px",
                textAlign: "center",
                color: "white",
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                height: "40px",
              }}
            >
              <ArqText
                fontSize="24px"
                color="#ffffff"
                fontWeight="bold"
                text="Glossário"
              />
            </div>
        <CarregarLista setItems={setItems} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <div
            style={{
              width: "100% !important",
            }}
          >
          
            <div
              style={{
                marginTop: "20px",
              }}
            >
              {this.state.itemsNames && this.state.itemsNames.length > 0 && (
                <ArqGlossary
                  items={this.state.itemsNames}
                  onClick={onClickItem}
                />
              )}
            </div>
          </div>
        </div>
        {!!this.state.openModal &&
          this.state.itemClicked &&
          this.state.itemClicked.titulo && (
            <ArqModal
              onClose={closeModal}
              isOpen={this.state.openModal}
              padding={20}
              modalWidth="50%"
            >
              <div
                style={{
                  overflowY: "auto",
                  maxHeight: 500,
                }}
              >
                <div
                  style={{
                    width: "100%",
                    marginBottom: 20,
                  }}
                >
                  <ArqText
                    text={this.state.itemClicked.titulo}
                    color="#484848"
                    fontSize="18px"
                    fontWeight="bold"
                  />
                </div>
                {this.state.itemClicked.descricao && (
                  <ArqText
                    text={this.state.itemClicked.descricao}
                    color="#484848"
                    fontSize="14px"
                  />
                )}
                {this.state.itemClicked.img && (
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: 20,
                    }}
                  >
                    <img
                      src={this.state.itemClicked.img}
                      alt={this.state.itemClicked.titulo}
                      style={{
                        width: "50%",
                      }}
                    />
                  </div>
                )}
                {/*this.state.itemClicked.author && (
                  <div style={{ marginTop: 20, width: "100%" }}>
                    <ArqText
                      text={"Criado por " + this.state.itemClicked.author}
                      color="#cacaca"
                      fontSize="12px"
                    />
                  </div>
                )}
                {this.state.itemClicked.update &&
                  this.state.itemClicked.update.author && (
                    <div style={{ marginTop: 10, width: "100%" }}>
                      <ArqText
                        text={
                          "Editado por " +
                          this.state.itemClicked.update.author +
                          (this.state.itemClicked.update.date
                            ? " " + this.state.itemClicked.update.date
                            : "")
                        }
                        color="#cacaca"
                        fontSize="12px"
                      />
                    </div>
                      )*/}
              </div>
            </ArqModal>
          )}
      </div>
    );
  }
}
