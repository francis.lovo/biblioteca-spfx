import * as React from "react";
import { IWebpartSocialProps } from "./IWebpartSocialProps";
import { ArqText } from "biblioteca-portal";

export default class WebpartSocial extends React.Component<
  IWebpartSocialProps,
  {}
> {
  public render(): React.ReactElement<IWebpartSocialProps> {
    return (
      <div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
          }}
        >
          <ArqText
            text="Social"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
      </div>
    );
  }
}
