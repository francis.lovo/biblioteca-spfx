import moment from "moment";
export const formatarData = (dataStr: string): string => {
  const agora = moment.utc();
  const data = moment.utc(dataStr);

  const diferenca = data.diff(agora, "seconds");
  const diferencaEmMinutos = Math.abs(diferenca) / 60;
  const diferencaEmHoras = diferencaEmMinutos / 60;
  const diferencaEmDias = Math.abs(diferenca) / (60 * 60 * 24);
  const diferencaEmMeses = Math.abs(diferenca) / (60 * 60 * 24 * 30);

  if (diferenca < 0) {
    if (diferencaEmMinutos < 60) {
      return `Há ${Math.floor(diferencaEmMinutos)} minutos`;
    } else if (diferencaEmHoras < 24) {
      return `Há ${Math.floor(diferencaEmHoras)} horas`;
    } else if (diferencaEmDias === 1) {
      return `Ontem às ${data.format("HH:mm")}`;
    } else if (diferencaEmDias < 30) {
      return `Há ${Math.floor(diferencaEmDias)} dias`;
    } else {
      return `Há ${Math.floor(diferencaEmMeses)} meses`;
    }
  } else {
    if (diferencaEmHoras < 1) {
      return `Em ${Math.floor(diferencaEmMinutos)} minutos`;
    } else if (diferencaEmHoras < 24) {
      return `Em ${Math.floor(diferencaEmHoras)} horas`;
    } else if (diferencaEmDias < 30) {
      return `Em ${Math.floor(diferencaEmDias)} dias`;
    } else {
      return `Em ${Math.floor(diferencaEmMeses)} meses`;
    }
  }
};
