import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const StyledSwitchLabel = styled.div`
  font-family: Bradesco Regular Sans;
  color: #4f4f4f;
  margin-left: 10px;
  display: flex;
  align-items: center;
`;
export const SwitchContainer = styled.label<{ isOn?: boolean }>`
  position: relative;
  display: inline-flex;
  align-items: center;
  cursor: "pointer";

  input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .switch {
    position: relative;
    display: inline-block;
    width: 42px;
    height: 18px;
    margin-left: 4px;
    border-radius: 26px;
    background-color: ${({ isOn }) => (isOn ? RedBradesco : "#dedede")};
    transition: background-color 0.2s;
  }

  .switch:before {
    content: "";
    position: absolute;
    width: 14px;
    height: 14px;
    border-radius: 50%;
    background-color: white;
    top: 2px;
    left: ${({ isOn }) => (isOn ? "26px" : "2px")};
    transition: left 0.2s;
  }
  &:hover {
    .switch {
      background-color: ${({ isOn }) => (isOn ? RedBradesco : "#bababa")};
    }
  }
`;

export const SwitchInput = styled.input.attrs({ type: "checkbox" })`
  margin: 0;
`;
