import React from "react";
import * as S from "./styles";

export interface IArqSwitchProps {
  onChange?: (value: boolean) => void;
  isOn?: boolean;
  label?: string;
  disabled?: boolean;
}
export interface MyState {
  isChecked: boolean;
}
export class ArqSwitch extends React.Component<IArqSwitchProps, MyState> {
  state: MyState = {
    isChecked: this.props.isOn ? this.props.isOn : false,
  };

  componentDidUpdate(prevProps: Readonly<IArqSwitchProps>): void {
    if (prevProps.isOn !== this.props.isOn) {
      this.setState({ isChecked: true });
    }
  }
  public render(): JSX.Element {
    const handleSwitch = (): void => {
      if (!this.props.disabled || !this.state.isChecked === true) {
        this.setState({ isChecked: !this.state.isChecked });
        if (this.props.onChange) {
          this.props.onChange(this.state.isChecked);
        }
      }
    };
    return (
      <>
        <S.SwitchContainer isOn={this.state.isChecked}>
          <S.SwitchInput
            checked={this.state.isChecked}
            onChange={handleSwitch}
          />
          <div className="switch" />
          <S.StyledSwitchLabel>{this.props.label}</S.StyledSwitchLabel>
        </S.SwitchContainer>
      </>
    );
  }
}
