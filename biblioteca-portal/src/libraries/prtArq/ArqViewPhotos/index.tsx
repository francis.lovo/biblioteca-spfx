import React from "react";
import styled from "styled-components";
import { ArqModal } from "../ArqModal";
import { LightBlueBradesco } from "../../../colors";

export interface IArqViewPhotosProps {
  isOpen?: boolean;
  onClose?: () => void;
  photos?: string[];
  titles?: string[];
  subtitles?: string[];
}

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
const StyledPhoto = styled.img`
  width: 100%;
  height: auto;
`;
const StyledTextContainer = styled.div`
  width: 100%;
  padding-top: 15px;
  padding-bottom: 15px;
  display: flex;
  flex-direction: column;
  align-items: start;
  justify-content: center;
`;
const StyledTitle = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  font-size: 16px;
  margin-bottom: 10px;
  margin-left: 15px;
  margin-right: 15px;
`;
const StyledSubTitle = styled.div`
  font-family: Bradesco Regular Sans;
  color: #4f4f4f;
  font-size: 14px;
  margin-left: 15px;
  margin-right: 15px;
`;
const StyledLeftArrow = styled.div`
  display: flex;
  position: absolute;
  width: 25px;
  height: 25px;
  z-index: 1000;
  border-radius: 50px;
  top: 40%;
  left: 8%;
  justify-content: center;
  align-items: center;
  background: #fff;
  cursor: pointer;
`;
const StyledRightArrow = styled.div`
  display: flex;
  position: absolute;
  width: 25px;
  height: 25px;
  z-index: 1000;
  border-radius: 50px;
  top: 40%;
  right: 8%;
  justify-content: center;
  align-items: center;
  background: #fff;
  cursor: pointer;
`;
const StyledIcon = styled.i`
  color: ${LightBlueBradesco};
  font-size: 14px;
  font-weight: 900;
`;
export interface MyState {
  selectedPhoto: number;
}

export class ArqViewPhotos extends React.Component<
  IArqViewPhotosProps,
  MyState
> {
  state: MyState = {
    selectedPhoto: 0,
  };
  public render(): JSX.Element {
    const rightPhoto = (): void => {
      if (this.props.photos && this.props.photos.length > 0) {
        this.setState({
          selectedPhoto:
            this.state.selectedPhoto === this.props.photos.length - 1
              ? 0
              : this.state.selectedPhoto + 1,
        });
      }
    };
    const leftPhoto = (): void => {
      if (this.props.photos && this.props.photos.length > 0) {
        this.setState({
          selectedPhoto:
            this.state.selectedPhoto === 0
              ? this.props.photos.length - 1
              : this.state.selectedPhoto - 1,
        });
      }
    };
    return (
      <>
        {!!this.props.isOpen && (
          <StyledLeftArrow onClick={leftPhoto}>
            <StyledIcon className="icon-icon-seta-esquerda-a" />
          </StyledLeftArrow>
        )}
        <ArqModal
          isOpen={this.props.isOpen}
          onClose={this.props.onClose}
          padding={0}
          modalWidth="70%"
        >
          <StyledContainer>
            {this.props.photos && this.props.photos.length > 0 && (
              <StyledPhoto
                src={this.props.photos[this.state.selectedPhoto]}
                alt="InfoPhoto"
              />
            )}
            <StyledTextContainer>
              {this.props.titles &&
                this.props.titles.length > 0 &&
                this.props.titles[this.state.selectedPhoto] && (
                  <StyledTitle>
                    {this.props.titles[this.state.selectedPhoto]}
                  </StyledTitle>
                )}
              {this.props.subtitles &&
                this.props.subtitles.length > 0 &&
                this.props.subtitles[this.state.selectedPhoto] && (
                  <StyledSubTitle>
                    {this.props.subtitles[this.state.selectedPhoto]}
                  </StyledSubTitle>
                )}
            </StyledTextContainer>
          </StyledContainer>
        </ArqModal>
        {!!this.props.isOpen && (
          <StyledRightArrow onClick={rightPhoto}>
            <StyledIcon className="icon-icon-seta-direita-a" />
          </StyledRightArrow>
        )}
      </>
    );
  }
}
