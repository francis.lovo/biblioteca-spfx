import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export interface IArqModalProps {
  isOpen?: boolean;
  topLine?: boolean;
  modalWidth?: string;
  padding?: number;
}

export const StyledModal = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 999;
  background-color: rgba(0, 0, 0, 0.4);
  font-family: Bradesco Regular Sans;
`;
export const StyledModalContent = styled.div<IArqModalProps>`
  position: fixed;
  top: 10%;
  left: 50%;
  transform: translateX(-50%);
  background-color: #fff;
  display: ${({ isOpen }) => (isOpen ? "flex" : "none")};
  flex-direction: column;
  justify-content: start;
  align-items: center;
  width: ${({ modalWidth }) => modalWidth};
  min-height: 30%;
  z-index: 1001;
  box-sizing: border-box;
  border-radius: 3px;
  box-shadow: 0 4px 8px rgba(72, 91, 98, 0.16), 0 4px 4px rgba(72, 91, 98, 0.24);
  padding: ${({ padding }) => padding}px;
  border-top: ${({ topLine }) => (topLine ? "3px solid " + RedBradesco : "")};
`;
export const StyledClose = styled.span`
  position: fixed;
  top: 15px;
  right: 10px;
`;
