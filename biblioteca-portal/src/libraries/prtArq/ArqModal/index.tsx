import React from "react";
import * as S from "./styles";
import { ArqIconButton } from "../ArqIconButton";

export interface IArqModalProps extends Partial<S.IArqModalProps> {
  children?: React.ReactNode;
  onClose?: () => void;
  noCloseButton?: boolean;
}

export class ArqModal extends React.Component<IArqModalProps, {}> {
  public render(): JSX.Element {
    return (
      <S.StyledModal className="modal">
        <S.StyledModalContent
          isOpen={this.props.isOpen}
          topLine={this.props.topLine}
          modalWidth={this.props.modalWidth}
          padding={this.props.padding}
        >
          {!this.props.noCloseButton && (
            <S.StyledClose onClick={this.props.onClose}>
              <ArqIconButton
                icon="icon-icon-nav-fechar"
                fontSize="20px"
                color="#aaa"
              />
            </S.StyledClose>
          )}
          {this.props.children}
        </S.StyledModalContent>
      </S.StyledModal>
    );
  }
}
