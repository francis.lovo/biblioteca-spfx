/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { ArqIconButton } from "../ArqIconButton";
import * as S from "./styles";

export interface IArqUploadFolderProps {
  label?: string;
  files: any;
  setFiles: React.Dispatch<any>;
  color?: string;
  hoverColor?: string;
  plusIcon?: boolean;
}

export class ArqUploadFolder extends React.Component<
  IArqUploadFolderProps,
  {}
> {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private buttonUpload: React.RefObject<any> = React.createRef();
  public render(): JSX.Element {
    const handleClick = (): void => {
      console.log("Clicou uploader");
      if (this.buttonUpload) {
        console.log("entrou no if");
        this.buttonUpload.current.click();
      }
    };
    const handleChange = (event: any): void => {
      const quantidadeArquivos = event.target.files.length;
      const arrayAux: any[] = [];

      for (let index = 0; index < quantidadeArquivos; index++) {
        arrayAux.push(event.target.files[index]);
      }
      this.props.setFiles(arrayAux);
    };
    return (
      <>
        {!!this.props.label && <S.StyledText>{this.props.label}</S.StyledText>}
        <S.StyledUploaderContainer>
          <S.StyleduploaderButton
            onClick={handleClick}
            color={this.props.color}
            hoverColor={this.props.hoverColor}
          >
            <input
              type="file"
              style={{ display: "none" }}
              ref={this.buttonUpload}
              onChange={handleChange}
            />
            Escolha o Arquivo
          </S.StyleduploaderButton>
          {this.props.plusIcon && (
            <ArqIconButton
              icon="icon-icon-nav-adicionar"
              color={this.props.color}
              hoverColor={this.props.hoverColor}
              onClick={handleClick}
            />
          )}
        </S.StyledUploaderContainer>
        {this.props.files && this.props.files.length > 0 && (
          <S.StyledFileName>{this.props.files[0].name}</S.StyledFileName>
        )}
      </>
    );
  }
}
