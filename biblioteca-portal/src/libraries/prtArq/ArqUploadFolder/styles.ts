import styled from "styled-components";

export interface IArqUploadFolderProps {
  color?: string;
  hoverColor?: string;
}

export const StyledUploaderContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`;
export const StyleduploaderButton = styled.button<IArqUploadFolderProps>`
  ackground-color: ${({ color }) => color};
  color: white;
  cursor: pointer;
  height: 40px;
  width: max-content;
  padding-left: 10px;
  padding-right: 10px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: 1px solid ${({ color }) => color};
  border-radius: 5px;
  &:hover {
    border: 1px solid ${({ hoverColor }) => hoverColor}};
    background-color: ${({ hoverColor }) => hoverColor}};
  }
  font-family: Bradesco Regular Sans;
`;
export const StyledFileName = styled.div`
  font-family: Bradesco Regular Sans;
  color: #4f4f4f;
  font-size: 10px;
  margin-top: 5px;
`;
export const StyledText = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  margin-bottom: 10px;
`;
