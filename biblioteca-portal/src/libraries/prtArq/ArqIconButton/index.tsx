import * as React from "react";
import * as S from "./styles";
import { ApprovedIcons } from "../../../types";

export interface IArqIconButtonProps extends Partial<S.IArqIconButtonProps> {
  icon: ApprovedIcons;
  onClick?: () => void;
}

export class ArqIconButton extends React.Component<IArqIconButtonProps, {}> {
  public render(): JSX.Element {
    return (
      <S.StyledIcon
        className={this.props.icon}
        onClick={this.props.onClick}
        fontSize={this.props.fontSize}
        color={this.props.color}
        hoverColor={this.props.hoverColor}
        fontWeight={this.props.fontWeight}
      />
    );
  }
}
