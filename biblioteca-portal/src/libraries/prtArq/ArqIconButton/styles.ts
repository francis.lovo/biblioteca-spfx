import styled, { css } from "styled-components";

export interface IArqIconButtonProps {
  fontSize?: string;
  color?: string;
  hoverColor?: string;
  fontWeight?: string;
}

export const StyledIcon = styled.i<IArqIconButtonProps>`
  font-size: ${({ fontSize }) => (fontSize ? fontSize : "24px")};
  display: flex;
  justify-content: center;
  color: ${({ color }) => (color ? color : "gray")};
  cursor: pointer;
  ${({ hoverColor }) =>
    hoverColor
      ? css`
          &:hover {
            color: ${hoverColor};
          }
        `
      : ""}
  width: max-content;
  font-weight: ${({ fontWeight }) => (fontWeight ? fontWeight : "normal")};
`;
