/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import * as S from "./styles";
import moment from "moment";
import { ArqIconButton } from "../ArqIconButton";

export interface IArqDatePickerProps {
  label?: string;
  value?: string;
  onChange?: (value: string) => void;
  min?: string;
  max?: string;
}
export interface MyState {
  dateValue: string | undefined;
}

export class ArqDatePicker extends React.Component<
  IArqDatePickerProps,
  MyState
> {
  state: MyState = {
    dateValue: this.props.value,
  };
  private inputDate: React.RefObject<any>;
  constructor(props: {}) {
    super(props);
    this.inputDate = React.createRef<any>();
  }
  componentDidUpdate(prevProps: Readonly<IArqDatePickerProps>): void {
    if (prevProps.value !== this.props.value) {
      this.setState({
        dateValue:
          this.props.value === undefined
            ? undefined
            : moment(this.props.value).format("DD/MM/YYYY"),
      });
    }
  }
  public render(): JSX.Element {
    const handleClick = (): void => {
      console.log("CLICK: ", this.inputDate);
      console.log("Current: ", this.inputDate.current);

      if (this.inputDate && this.inputDate.current) {
        this.inputDate.current.showPicker();
      }
    };
    const onChangeDate = (): void => {
      if (
        this.inputDate &&
        this.inputDate.current &&
        this.inputDate.current.value
      ) {
        this.setState({
          dateValue: moment(this.inputDate.current.value).format("DD/MM/YYYY"),
        });
        if (this.props.onChange) {
          this.props.onChange(this.inputDate.current.value);
        }
      } else {
        this.setState({
          dateValue: undefined,
        });

        if (this.props.onChange) {
          this.props.onChange(this.inputDate.current.value);
        }
      }
    };
    return (
      <div>
        {!!this.props.label && (
          <S.StyledTextFieldContainer>
            {this.props.label}
          </S.StyledTextFieldContainer>
        )}
        <S.StyledDatePicker onClick={handleClick}>
          <S.StyledText selected={this.state.dateValue !== undefined}>
            {this.state.dateValue ? this.state.dateValue : "00/00/00"}
          </S.StyledText>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: "100%",
            }}
          >
            <ArqIconButton icon="icon-icon-doc-calendario" color="#b8b8b8" />
            <S.StyledHiddenIput
              type="date"
              ref={this.inputDate}
              onChange={onChangeDate}
              min={this.props.min}
              max={this.props.max}
            />
          </div>
        </S.StyledDatePicker>
      </div>
    );
  }
}
