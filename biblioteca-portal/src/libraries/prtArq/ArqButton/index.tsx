import * as React from "react";
import * as S from "./styles";

export interface IArqButtonProps extends Partial<S.IArqButtonProps> {
  text?: string;
  onClick?: () => void;
}
export class ArqButton extends React.Component<IArqButtonProps, {}> {
  public render(): JSX.Element {
    return (
      <S.StyledButton
        onClick={this.props.onClick}
        color={this.props.color}
        hoverColor={this.props.hoverColor}
        styleType={this.props.styleType}
      >
        {this.props.text}
      </S.StyledButton>
    );
  }
}
