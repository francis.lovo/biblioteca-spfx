import * as React from "react";
import * as S from "./styles";
import { ArqIconButton } from "../ArqIconButton";

export interface IArqCheckboxProps {
  selected?: boolean;
  onChange?: (value: boolean) => void;
  label?: string;
}
export interface MyState {
  checked: boolean;
}

export class ArqCheckBox extends React.Component<IArqCheckboxProps, MyState> {
  state: MyState = {
    checked: this.props.selected ? this.props.selected : false,
  };
  public render(): JSX.Element {
    const handleClickCheckbox = (): void => {
      if (this.props.onChange) {
        this.props.onChange(!this.state.checked);
      }
      this.setState({ checked: !this.state.checked });
    };
    return (
      <S.StyledCheckboxContainer onClick={handleClickCheckbox}>
        <S.StyledCheckbox>
          {!!this.state.checked && (
            <ArqIconButton
              icon="icon-icon-nav-check"
              color="#4f4f4f"
              fontSize="16px"
            />
          )}
        </S.StyledCheckbox>
        {this.props.label && (
          <S.StyledCheckboxLabel>{this.props.label}</S.StyledCheckboxLabel>
        )}
      </S.StyledCheckboxContainer>
    );
  }
}
