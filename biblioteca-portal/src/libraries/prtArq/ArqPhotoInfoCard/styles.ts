import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const StyledContainer = styled.div`
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  width: 100%;
  font-family: Bradesco Regular Sans;
  border-radius: 4px;
  display: flex;
  flex-direction: row;
`;
export const StyledTextsContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 15px;
  align-items: start;
  justify-content: center;
`;
export const StyledTitle = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: #4d4e53;
`;
export const StyledName = styled.div`
  font-size: 14px;
  font-weight: bold;
  color: #4d4e53;
  margin-top: 10px;
`;
export const StyledEmail = styled.div`
  font-size: 12px;
  color: ${RedBradesco};
  margin-top: 5px;
`;
export const StyledNoPhoto = styled.div`
  background-color: gray;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  width: 100px;
`;
export const StyledPhoto = styled.div`
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  width: 100px;
`;
export const StyledImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;
