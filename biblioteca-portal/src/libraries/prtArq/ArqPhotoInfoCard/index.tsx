import React from "react";
import * as S from "./styles";

export interface IArqPhotoInfoCardProps {
  photo?: string;
  title?: string;
  name?: string;
  email?: string;
}
export class ArqPhotoInfoCard extends React.Component<
  IArqPhotoInfoCardProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <S.StyledContainer>
        {!this.props.photo && <S.StyledNoPhoto />}
        {!!this.props.photo && (
          <S.StyledPhoto>
            <S.StyledImg src={this.props.photo} alt="InfoPhoto" />
          </S.StyledPhoto>
        )}
        <S.StyledTextsContainer>
          <S.StyledTitle>{this.props.title}</S.StyledTitle>
          {!!this.props.name && <S.StyledName>{this.props.name}</S.StyledName>}
          {!!this.props.email && (
            <S.StyledEmail>{this.props.email}</S.StyledEmail>
          )}
        </S.StyledTextsContainer>
      </S.StyledContainer>
    );
  }
}
