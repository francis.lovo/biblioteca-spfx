import React from "react";
import * as S from "./styles";
import { ArqIconButton } from "../ArqIconButton";

export interface IArqNotificationCardProps
  extends Partial<S.IArqNotificationCardProps> {
  title?: string;
  isNew?: boolean;
  onClick?: () => void;
}

export class ArqNotificationCard extends React.Component<
  IArqNotificationCardProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <S.StyledCardContainer>
        <S.StyledTextContainer>
          <S.StyledTitle text={this.props.text} footer={this.props.footer}>
            {this.props.title}
          </S.StyledTitle>
          {!!this.props.text && (
            <S.StyledSubtitle footer={this.props.footer}>
              {this.props.text}
            </S.StyledSubtitle>
          )}
          {!!this.props.footer && (
            <S.StyledFooter>{this.props.footer}</S.StyledFooter>
          )}
        </S.StyledTextContainer>
        <S.StyledIconContainer>
          {this.props.isNew && (
            <div
              style={{
                width: 10,
                height: 10,
                backgroundColor: "#0E81ED",
                borderRadius: "50%",
                position: "absolute",
                top: "-5px",
                right: "5px",
              }}
            />
          )}
          <ArqIconButton
            icon="icon-icon-residencial-lixeira"
            fontSize="18px"
            color="#dcdcdc"
            onClick={this.props.onClick}
          />
        </S.StyledIconContainer>
      </S.StyledCardContainer>
    );
  }
}
