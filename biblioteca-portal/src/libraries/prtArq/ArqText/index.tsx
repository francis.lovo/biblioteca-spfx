import React from "react";
import * as S from "./styles";

export interface IArqTextPorps {
  fontSize?: string;
  color?: string;
  fontWeight?: string;
  text: string;
}

export class ArqText extends React.Component<IArqTextPorps, {}> {
  public render(): JSX.Element {
    return (
      <S.StyledTextContainer
        fontWeight={this.props.fontWeight}
        fontSize={this.props.fontSize}
        color={this.props.color}
      >
        {this.props.text}
      </S.StyledTextContainer>
    );
  }
}
