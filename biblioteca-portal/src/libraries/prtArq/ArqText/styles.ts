import styled from "styled-components";

export const StyledTextContainer = styled.div<{
  fontWeight?: string;
  color?: string;
  fontSize?: string;
}>`
  color: ${({ color }) => (color ? color : "#484848")};
  font-size: ${({ fontSize }) => (fontSize ? fontSize : "14px")};
  font-weight: ${({ fontWeight }) => (fontWeight ? fontWeight : "normal")};
  font-family: Bradesco Regular Sans;
`;
