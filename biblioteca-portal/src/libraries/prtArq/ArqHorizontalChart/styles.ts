import styled, { css } from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  salign-items: flex-start;
  font-family: "Bradesco Regular Sans";
  width: 100%;
  height:100%;
`;

export const BarChartContainer = styled.div`
  display: flex;
  width: 100%;
`;

export const BarChart = styled.div`
  width: 100%;
`;

export const BarContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 15px;
`;

export const EmptyBar = styled.div`
  width: 100%;
  height: 15px;
  background-color: #ccc;
  border-radius: 7.5px;
`;

export const FilledBar = styled.div<{ progress?: number }>`
  width: ${(props) => props.progress}%;
  height: 100%;
  background-color: #ff605e;
  border-top-left-radius: 7.5px;
  border-bottom-left-radius: 7.5px;
  ${({ progress }) =>
    progress === 100
      ? css`
          border-top-right-radius: 7.5px;
          border-bottom-right-radius: 7.5px;
        `
      : ""}
`;

export const Text = styled.span`
  margin-right: 10px;
`;
