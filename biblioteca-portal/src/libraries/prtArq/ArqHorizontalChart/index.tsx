import React from "react";
import * as S from "./styles";

export interface IArqHorizointalChartProps {
  labels?: string[];
  porcents?: number[];
}

export class ArqHorizontalChart extends React.Component<
  IArqHorizointalChartProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <S.Container>
        <S.BarChartContainer>
          <S.BarChart>
            {this.props.porcents &&
              this.props.porcents.map((numb: number, index) => {
                return (
                  <S.BarContainer key={index}>
                    {this.props.labels &&
                      this.props.labels.length > 0 &&
                      this.props.labels[index] && (
                        <S.Text>{this.props.labels[index]}</S.Text>
                      )}
                    <S.EmptyBar>
                      <S.FilledBar progress={numb} />
                    </S.EmptyBar>
                  </S.BarContainer>
                );
              })}
          </S.BarChart>
        </S.BarChartContainer>
      </S.Container>
    );
  }
}
