/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import {
  BarChart,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  Bar,
} from "recharts";

export interface IArqNewBarCharProps {
  width: number;
  height: number;
  data: any[];
  colors: string[];
  names: string[];
}

export class ArqNewBarChart extends React.Component<IArqNewBarCharProps, {}> {
  public render(): JSX.Element {
    return (
      <BarChart
        width={this.props.width}
        height={this.props.height}
        data={this.props.data}
        layout="horizontal"
        margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
      >
        <XAxis type="category" dataKey="name" />
        <YAxis type="number" />
        <CartesianGrid strokeDasharray="3" />
        <Tooltip />
        <Legend />
        {this.props.names &&
          this.props.names.length > 0 &&
          this.props.names.map((nameBar, index) => {
            return <Bar dataKey={nameBar} fill={this.props.colors[index]} />;
          })}
      </BarChart>
    );
  }
}
