import styled from "styled-components";

export const RadioLabel = styled.label`
  display: flex;
  align-items: center;
  margin-bottom: 8px;
  cursor: pointer;
  font-family: "Bradesco Regular Sans";
  width: max-content;
  color: #484848;
`;
