/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import * as S from "./styles";
import "./style.css";

export interface IArqRadioButtonProps {
  label?: string;
  isSelected?: boolean;
  value?: string;
  onChange?: (value: string) => void;
  name?: string;
}
export class ArqRadioButton extends React.Component<IArqRadioButtonProps, {}> {
  public render(): JSX.Element {
    const handleOptionChange = (event: any): void => {
      if (this.props.onChange) {
        this.props.onChange(event.target.value);
      }
    };

    return (
      <S.RadioLabel>
        <input
          type="radio"
          name={this.props.name}
          value={this.props.value}
          onChange={handleOptionChange}
          checked={this.props.isSelected}
        />
        {this.props.label}
      </S.RadioLabel>
    );
  }
}
