import styled from "styled-components";
import { LightBlueBradesco } from "../../../colors";

export const StyledChapterButton = styled.a`
  text-decoration: none;
  display: flex;
  background-color: rgb(251, 249, 249);
  color: rgb(57, 57, 57);
  padding: 10px 20px;
  border: none;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  height: 70px;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  font-family: Bradesco Regular Sans;
`;
export const StyledChapterButtonIcon = styled.i`
  color: ${LightBlueBradesco};
  font-size: 20px;
`;
export const StyledChapterButtonTitle = styled.div`
  display: flex;
  align-items: left;
  justify-content: flex-start;
  text-align: left;
  font-weight: "bold";
`;
export const StyledChapterButtonSubtitle = styled.div`
  margin-top: 4px;
  color: rgb(167, 167, 167);
  display: flex;
  align-items: center;
  justify-content: flex-start;
  text-align: left;
  font-size: 12px;
  font-weight: light;
`;

export const StyledChapterButtonTexts = styled.div`
  display: flex;
  flex-direction: column;
  align-items: left;
  text-align: left;
  padding-left: 25px;
`;
export const StyledChapterButtonImage = styled.img`
  width: 64px;
  margin-right: 10px;
  margin-bottom: 10px;
`;
export const StyledChapterButtonIconTextContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
