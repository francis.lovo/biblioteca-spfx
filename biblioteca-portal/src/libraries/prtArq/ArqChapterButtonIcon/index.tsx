import * as React from "react";
import * as S from "./styles";

export interface IArqChapterButtonIconProps {
  title?: string;
  subtitle?: string;
  link?: string;
  icon?: string | React.ReactNode;
}

export class ArqChapterButtonIcon extends React.Component<
  IArqChapterButtonIconProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <S.StyledChapterButton href={this.props.link}>
        <S.StyledChapterButtonIconTextContainer>
          {this.props.icon && typeof this.props.icon === "string" ? (
            <S.StyledChapterButtonImage
              src={this.props.icon}
              alt="IconInicio"
            />
          ) : (
            this.props.icon
          )}
          <S.StyledChapterButtonTexts>
            <S.StyledChapterButtonTitle>
              {this.props.title}
            </S.StyledChapterButtonTitle>
            {!!this.props.subtitle && (
              <S.StyledChapterButtonSubtitle>
                {this.props.subtitle}
              </S.StyledChapterButtonSubtitle>
            )}
          </S.StyledChapterButtonTexts>
        </S.StyledChapterButtonIconTextContainer>
        <S.StyledChapterButtonIcon className="icon-icon-seta-direita-b" />
      </S.StyledChapterButton>
    );
  }
}