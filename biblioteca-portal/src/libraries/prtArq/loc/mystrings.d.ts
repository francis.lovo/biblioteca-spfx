declare interface IPrtArqLibraryStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'PrtArqLibraryStrings' {
  const strings: IPrtArqLibraryStrings;
  export = strings;
}
