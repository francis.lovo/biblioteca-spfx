import styled from "styled-components";
export const TableWrapper = styled.table`
  width: 100%;
  border-collapse: collapse;
  font-family: "Bradesco Regular Sans";
  width: 100%;
`;

export const TableHeader = styled.th`
  background-color: #cc092f;
  font-size: 12px;
  color: white;
  padding: 10px;
  &:first-child {
    border-top-left-radius: 5px;
  }
  &:last-child {
    border-top-right-radius: 5px;
  }
`;

export const TableRow = styled.tr<{ even: boolean; hover: boolean }>`
  background-color: ${(props) => (props.even ? "white" : "#f0f0f0")};
  ${({ hover }) =>
    hover
      ? `&:hover {
            background-color: #dddddd;
            cursor: pointer;
          }`
      : ""}
`;

export const TableCell = styled.td`
  padding: 10px;
  height: 30px;
`;
