import React from "react";
import * as S from "./styles";

export interface IArqTableProps {
  topLabels?: string[];
  tableItems?: string[][];
  hoverItems?: boolean;
  onClickRow?: (item: number) => void;
}

export class ArqTable extends React.Component<IArqTableProps, {}> {
  public render(): JSX.Element {
    return (
      <S.TableWrapper>
        <thead>
          <tr>
            {!!this.props.topLabels &&
              this.props.topLabels.length > 0 &&
              this.props.topLabels.map((label) => {
                return (
                  <S.TableHeader
                    colSpan={
                      this.props.topLabels &&
                      this.props.tableItems &&
                      this.props.tableItems.length > 0 &&
                      this.props.topLabels.length <
                        this.props.tableItems[0].length
                        ? this.props.tableItems[0].length
                        : 1
                    }
                  >
                    {label}
                  </S.TableHeader>
                );
              })}
          </tr>
        </thead>
        <tbody>
          {!!this.props.tableItems &&
            this.props.tableItems.length > 0 &&
            this.props.tableItems.map((tableItem, index) => (
              <S.TableRow
                key={index}
                even={index % 2 === 0}
                hover={this.props.hoverItems ? true : false}
                onClick={
                  this.props.onClickRow
                    ? () => {
                        if (this.props.onClickRow) this.props.onClickRow(index);
                      }
                    : () => {
                        console.log();
                      }
                }
              >
                {tableItem.map((item) => {
                  return <S.TableCell>{item}</S.TableCell>;
                })}
              </S.TableRow>
            ))}
        </tbody>
      </S.TableWrapper>
    );
  }
}
