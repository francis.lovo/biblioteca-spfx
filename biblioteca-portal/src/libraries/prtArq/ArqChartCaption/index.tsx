import * as React from "react";
import styled from "styled-components";

export interface IArqChartCaptionProps {
  label?: string;
  color?: string;
}
export const StyledCaptionContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const StyledCaptionColor = styled.div<{ color: string }>`
  background: ${({ color }) => color};
  width: 35px;
  height: 20px;
`;
export const StyledCaptionLabel = styled.div`
  font-family: Bradesco Regular Sans;
  font-size: 12px;
  color: #4f4f4f;
  margin-left: 5px;
`;

export class ArqChartCaption extends React.Component<
  IArqChartCaptionProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <StyledCaptionContainer>
        <StyledCaptionColor color={this.props.color ? this.props.color : ""} />
        <StyledCaptionLabel>{this.props.label}</StyledCaptionLabel>
      </StyledCaptionContainer>
    );
  }
}
