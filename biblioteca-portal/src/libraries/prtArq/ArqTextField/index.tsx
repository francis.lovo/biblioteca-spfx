import React from "react";
import * as S from "./styles";

export interface IArqTextFieldProps {
  label?: string;
  onChange?: (text: string) => void;
  placeholder?: string;
  value?: string;
  emptyField?: boolean;
}

export class ArqTextField extends React.Component<IArqTextFieldProps, {}> {
  public render(): JSX.Element {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handleOnChange = (event: any): void => {
      if (this.props.onChange) {
        this.props.onChange(event.target.value);
      }
    };

    return (
      <>
        <S.StyledTextFieldContainer>
          {this.props.label}
        </S.StyledTextFieldContainer>
        <S.StyledTextField
          type="text"
          onChange={handleOnChange}
          placeholder={this.props.placeholder}
          value={this.props.value}
          emptyField={this.props.emptyField || false}
        />
      </>
    );
  }
}
