import styled from 'styled-components';

export const StyledTextField = styled.input<{ emptyField: boolean }>`
  border: 1px solid ${props => props.emptyField ? 'red' : '#b8b8b8'};
  padding: 10px;
  width: 100%;
  border-radius: 4px;
  font-family: Bradesco Regular Sans;
  :focus-visible {
    outline: none;
  }
  box-sizing: border-box;
`;
export const StyledTextFieldContainer = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4d4e53;
  margin-bottom: 10px;
`;
