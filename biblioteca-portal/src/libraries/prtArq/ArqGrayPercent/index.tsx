import React from "react";
import * as S from "./styles";

export interface IArqGrayPercent {
  percent?: number;
}
export class ArqGrayPercent extends React.Component<IArqGrayPercent, {}> {
  public render(): JSX.Element {
    return (
      <S.GrayPercentWrapper>
        <S.GrayProgressBar percent={this.props.percent} />
        <S.GrayText>{this.props.percent}%</S.GrayText>
      </S.GrayPercentWrapper>
    );
  }
}
