import React from "react";
import * as S from "./styles";

export interface IArqInformationCardProps {
  children?: React.ReactNode;
}

export class ArqInformationCard extends React.Component<
  IArqInformationCardProps,
  {}
> {
  public render(): JSX.Element {
    return <S.StyledCardComponent>{this.props.children}</S.StyledCardComponent>;
  }
}
