import styled from "styled-components";

export const ProgressBarWrapper = styled.div`
  display: flex;
  align-items: center;
  font-family: "Bradesco Regular Sans";
  width: 100%;
`;

export const ProgressBar = styled.div`
  height: 20px;
  width: 100%;
  background-color: #f2f2f2;
  border-radius: 5px;
  position: relative;
`;

export const Progress = styled.div`
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
`;

export const BlueProgress = styled(Progress)<{
  porcentBlue: number;
  rightBorder: boolean;
}>`
  background-color: #007bff;
  width: ${({ porcentBlue }) => porcentBlue + "%"};
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;

  border-top-right-radius: ${({ rightBorder }) => (rightBorder ? "5px" : "")};
  border-bottom-right-radius: ${({ rightBorder }) =>
    rightBorder ? "5px" : ""};
`;

export const GreenProgress = styled(Progress)<{
  porcentGreen?: number;
  porcentBlue?: number;
  rightBorder: boolean;
  leftBorder: boolean;
}>`
  background-color: #28a745;
  width: ${({ porcentGreen }) => porcentGreen + "%"};
  left: ${({ porcentBlue }) => porcentBlue + "%"};

  border-top-left-radius: ${({ leftBorder }) => (leftBorder ? "5px" : "")};
  border-bottom-left-radius: ${({ leftBorder }) => (leftBorder ? "5px" : "")};

  border-top-right-radius: ${({ rightBorder }) => (rightBorder ? "5px" : "")};
  border-bottom-right-radius: ${({ rightBorder }) =>
    rightBorder ? "5px" : ""};
`;

export const YellowProgress = styled(Progress)<{
  porcentYellow?: number;
  lastPorcent?: number;
  rightBorder: boolean;
  leftBorder: boolean;
}>`
  background-color: #ffc107;
  width: ${({ porcentYellow }) => porcentYellow + "%"};
  left: ${({ lastPorcent }) => lastPorcent + "%"};

  border-top-left-radius: ${({ leftBorder }) => (leftBorder ? "5px" : "")};
  border-bottom-left-radius: ${({ leftBorder }) => (leftBorder ? "5px" : "")};

  border-top-right-radius: ${({ rightBorder }) => (rightBorder ? "5px" : "")};
  border-bottom-right-radius: ${({ rightBorder }) =>
    rightBorder ? "5px" : ""};
`;

export const RedProgress = styled(Progress)<{
  porcentRed?: number;
  lastPorcent?: number;
  total: boolean;
}>`
  background-color: #dc3545;
  width: ${({ porcentRed }) => porcentRed + "%"};
  left: ${({ lastPorcent }) => lastPorcent + "%"};

  border-top-right-radius: ${({ total }) => (total ? "5px" : "")};
  border-bottom-right-radius: ${({ total }) => (total ? "5px" : "")};

  border-top-left-radius: ${({ lastPorcent }) =>
    lastPorcent === 0 ? "5px" : ""};
  border-bottom-left-radius: ${({ lastPorcent }) =>
    lastPorcent === 0 ? "5px" : ""};
`;

export const Text = styled.div`
  position: absolute;
  top: 0px;
  left: -20px;
  font-size: 12px;
`;

export const TextPorcent = styled.div<{
  porcent: number;
  fontSize: number;
  down?: boolean;
}>`
  position: absolute;
  top: ${({ down }) => (down ? "" : "-20px")};
  bottom: ${({ down }) => (down ? "-20px" : "")};
  left: ${({ porcent }) => porcent - 1 + "%"};
  font-size: ${({ fontSize }) => (fontSize ? fontSize : 12)}px;
`;

export const Text5 = styled.div<{ bigNumber: boolean }>`
  position: absolute;
  top: 0px;
  right: ${({ bigNumber }) => (bigNumber ? "-55px" : "-35px")};
  font-size: 12px;
`;
