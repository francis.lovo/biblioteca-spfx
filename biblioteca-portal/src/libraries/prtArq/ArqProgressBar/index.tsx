import React from "react";
import * as S from "./styles";

export interface IArqProgressBarProps {
  porcentGreen?: number;
  porcentBlue?: number;
  porcentYellow?: number;
  porcentRed?: number;
  total?: number;
  fontSize?: number;
}

export class ArqProgressBar extends React.Component<IArqProgressBarProps, {}> {
  public render(): JSX.Element {
    const total = this.props.total ? this.props.total : 100;
    const montarMascara = (num: number): string => {
      const numString = num + "";
      if (+numString.split(".")[1] === 0) {
        return num.toFixed(0);
      } else {
        return num.toFixed(2);
      }
    };
    return (
      <S.ProgressBarWrapper>
        <S.ProgressBar>
          <S.Text>0%</S.Text>
          <S.BlueProgress
            porcentBlue={this.props.porcentBlue ? this.props.porcentBlue : 0}
            rightBorder={
              this.props.porcentBlue &&
              this.props.porcentBlue * (total / 100) === total
                ? true
                : false
            }
          />
          {!!this.props.porcentBlue &&
            this.props.porcentBlue * (total / 100) < total && (
              <S.TextPorcent
                porcent={this.props.porcentBlue}
                fontSize={
                  this.props.fontSize && this.props.fontSize > 0
                    ? this.props.fontSize
                    : 12
                }
              >
                {montarMascara(this.props.porcentBlue * (total / 100))}%
              </S.TextPorcent>
            )}
          {!!this.props.porcentGreen && (
            <S.GreenProgress
              porcentGreen={this.props.porcentGreen}
              porcentBlue={this.props.porcentBlue}
              rightBorder={
                ((this.props.porcentBlue || 0) + this.props.porcentGreen) *
                  (total / 100) ===
                total
                  ? true
                  : false
              }
              leftBorder={
                !this.props.porcentBlue || this.props.porcentBlue === 0
                  ? true
                  : false
              }
            />
          )}
          {!!this.props.porcentGreen &&
            ((this.props.porcentBlue || 0) + this.props.porcentGreen) *
              (total / 100) <
              +total &&
            ((this.props.porcentBlue || 0) +
              this.props.porcentGreen -
              this.props.porcentGreen >
              7 ||
              (this.props.porcentYellow &&
                (this.props.porcentGreen || 0) +
                  (this.props.porcentBlue || 0) +
                  this.props.porcentYellow ===
                  100)) && (
              <S.TextPorcent
                porcent={
                  (this.props.porcentBlue || 0) + this.props.porcentGreen
                }
                fontSize={
                  this.props.fontSize && this.props.fontSize > 0
                    ? this.props.fontSize
                    : 12
                }
                down={true}
              >
                {montarMascara(
                  ((this.props.porcentBlue || 0) + this.props.porcentGreen) *
                    (total / 100)
                )}
                %
              </S.TextPorcent>
            )}
          {!!this.props.porcentYellow && (
            <S.YellowProgress
              porcentYellow={this.props.porcentYellow}
              lastPorcent={
                (this.props.porcentGreen || 0) + (this.props.porcentBlue || 0)
              }
              rightBorder={
                +montarMascara(
                  ((this.props.porcentBlue || 0) +
                    (this.props.porcentGreen || 0) +
                    this.props.porcentYellow) *
                    (total / 100)
                ) >=
                (+total.toFixed(2).split(".")[1] === 0
                  ? total
                  : +total.toFixed(2))
              }
              leftBorder={
                (!this.props.porcentGreen || this.props.porcentGreen === 0) &&
                (!this.props.porcentBlue || this.props.porcentBlue === 0)
                  ? true
                  : false
              }
            />
          )}
          {!!this.props.porcentYellow &&
            +montarMascara(
              ((this.props.porcentBlue || 0) +
                (this.props.porcentGreen || 0) +
                this.props.porcentYellow) *
                (total / 100)
            ) <
              (+total.toFixed(2).split(".")[1] === 0
                ? total
                : +total.toFixed(2)) &&
            (this.props.porcentBlue || 0) +
              (this.props.porcentGreen || 0) +
              this.props.porcentYellow -
              ((this.props.porcentBlue || 0) + (this.props.porcentGreen || 0)) >
              9 && (
              <S.TextPorcent
                porcent={
                  (this.props.porcentBlue || 0) +
                  (this.props.porcentGreen || 0) +
                  this.props.porcentYellow
                }
                fontSize={
                  this.props.fontSize && this.props.fontSize > 0
                    ? this.props.fontSize
                    : 12
                }
              >
                {montarMascara(
                  ((this.props.porcentBlue || 0) +
                    (this.props.porcentGreen || 0) +
                    this.props.porcentYellow) *
                    (total / 100)
                )}
                %
              </S.TextPorcent>
            )}
          {!!this.props.porcentRed && (
            <S.RedProgress
              porcentRed={this.props.porcentRed}
              lastPorcent={
                (this.props.porcentYellow || 0) +
                (this.props.porcentBlue || 0) +
                (this.props.porcentGreen || 0)
              }
              total={this.props.porcentRed === 100}
            />
          )}
          {!!this.props.porcentRed &&
            this.props.porcentRed > 0 &&
            (this.props.porcentYellow || 0) +
              (this.props.porcentBlue || 0) +
              (this.props.porcentGreen || 0) +
              this.props.porcentRed <
              100 && (
              <S.TextPorcent
                porcent={
                  (this.props.porcentYellow || 0) +
                  (this.props.porcentBlue || 0) +
                  (this.props.porcentGreen || 0) +
                  this.props.porcentRed
                }
                fontSize={
                  this.props.fontSize && this.props.fontSize > 0
                    ? this.props.fontSize
                    : 12
                }
              >
                {montarMascara(
                  ((this.props.porcentYellow || 0) +
                    (this.props.porcentBlue || 0) +
                    (this.props.porcentGreen || 0) +
                    this.props.porcentRed) *
                    (total / 100)
                )}
                %
              </S.TextPorcent>
            )}
          <S.Text5
            bigNumber={+total.toFixed(2).split(".")[1] === 0 ? false : true}
          >
            {+total.toFixed(2).split(".")[1] === 0
              ? total.toFixed(0)
              : total.toFixed(2)}
            %
          </S.Text5>
        </S.ProgressBar>
      </S.ProgressBarWrapper>
    );
  }
}
