import React from "react";
import * as S from "./styles";

export interface IArqImageSliderProps {
  current?: number;
  images: string[];
}
export interface MyState {
  currentIndex: number;
}

export class ArqImageSlider extends React.Component<
  IArqImageSliderProps,
  MyState
> {
  state: MyState = {
    currentIndex: this.props.current ? this.props.current : 0,
  };
  public render(): JSX.Element {
    const goToNextSlide = (): void => {
      this.setState({
        currentIndex:
          this.state.currentIndex === this.props.images.length - 1
            ? 0
            : this.state.currentIndex + 1,
      });
    };

    const goToPrevSlide = (): void => {
      this.setState({
        currentIndex:
          this.state.currentIndex === 0
            ? this.props.images.length - 1
            : this.state.currentIndex - 1,
      });
    };

    return (
      <S.StyledSlider>
        <S.StyledButton onClick={goToPrevSlide} previous={true}>
          <S.StyledrButtonIcon
            src={require("../../../icons/seta.svg")}
            alt="Icon"
            previous={true}
          />
        </S.StyledButton>
        <S.StyledButton onClick={goToNextSlide}>
          <S.StyledrButtonIcon
            src={require("../../../icons/seta.svg")}
            alt="Icon"
          />
        </S.StyledButton>

        <S.StyledSlideContainer>
          {this.props.images.map((image, index) => (
            <S.StyledImage
              key={index}
              src={image}
              alt={`Slide ${index + 1}`}
              current={this.state.currentIndex}
              index={index}
            />
          ))}
        </S.StyledSlideContainer>
        <S.StyledSliderIndicators>
          {this.props.images.map((_image, index) => (
            <S.StyledSliderIndicator
              key={index}
              onClick={() => this.setState({ currentIndex: index })}
              current={this.state.currentIndex}
              index={index}
            />
          ))}
        </S.StyledSliderIndicators>
      </S.StyledSlider>
    );
  }
}
