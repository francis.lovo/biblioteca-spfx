import React from "react";
import { ArqIconButton } from "../ArqIconButton";
import * as S from "./styles";

export interface IArqPaginationProps {
  totalPages?: string;
  currentPage?: string;
  onClickPageUp?: () => void;
  onClickPageDown?: () => void;
  onClickLastPage?: () => void;
  onClickFirstPage?: () => void;
}
export class ArqPagination extends React.Component<IArqPaginationProps, {}> {
  public render(): JSX.Element {
    return (
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          marginTop: "10px",
        }}
      >
        <div
          style={{
            width: "20%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <S.StyledPageText>
            {this.props.currentPage} de {this.props.totalPages}
          </S.StyledPageText>
          <ArqIconButton
            icon="icon-icon-seta-dupla-b"
            fontSize="16px"
            onClick={this.props.onClickFirstPage}
          />
          <ArqIconButton
            icon="icon-icon-seta-esquerda-b"
            fontSize="16px"
            onClick={this.props.onClickPageUp}
          />
          <ArqIconButton
            icon="icon-icon-seta-direita-b"
            fontSize="16px"
            onClick={this.props.onClickPageDown}
          />
          <ArqIconButton
            icon="icon-icon-seta-dupla-direita-b"
            fontSize="16px"
            onClick={this.props.onClickLastPage}
          />
        </div>
      </div>
    );
  }
}
