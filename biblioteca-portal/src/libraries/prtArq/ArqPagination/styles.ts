import styled from "styled-components";

export const StyledPageText = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #5f5f5f;
  font-size: 12px;
`;
