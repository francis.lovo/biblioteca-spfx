import * as React from "react";
import * as S from "./styles";

export interface IArqBreadcrumbProps {
  items?: string[];
  links?: string[];
}
export class ArqBreadcrumb extends React.Component<IArqBreadcrumbProps, {}> {
  public render(): JSX.Element {
    return (
      <S.BreadcrumbContainer>
        <S.BreadcrumbItem>
          <a
            href={
              this.props.links &&
              this.props.links.length > 0 &&
              this.props.links[0]
                ? this.props.links[0]
                : undefined
            }
            className="link"
          >
            <i className="icon-icon-vert-residencial" />
          </a>
          {this.props.items &&
            this.props.items.length > 0 &&
            this.props.items.map((item: string, index: number) => {
              return (
                <div
                  key={index}
                  className={
                    this.props.items && index === this.props.items.length - 1
                      ? "last"
                      : ""
                  }
                >
                  <i className="icon-icon-seta-direita-a" />
                  <a
                    href={
                      this.props.links &&
                      this.props.links.length > 0 &&
                      this.props.links[index + 1]
                        ? this.props.links[index + 1]
                        : undefined
                    }
                    className="link"
                  >
                    {item}
                  </a>
                </div>
              );
            })}
        </S.BreadcrumbItem>
      </S.BreadcrumbContainer>
    );
  }
}
