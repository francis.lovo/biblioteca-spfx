import styled from 'styled-components';
import { LightBlueBradesco } from '../../../colors';

export const BreadcrumbContainer = styled.div`
  display: flex;
  align-items: center;
  font-family: 'Bradesco regular Sans';
`;

export const BreadcrumbItem = styled.div`
  display: flex;
  align-items: center;
  color: ${LightBlueBradesco};
  margin-right: 10px;

  &:last-child {
    margin-right: 0;
  }

  .icon-icon-vert-residencial {
    color: ${LightBlueBradesco};
    font-size: 24px;
    margin-right: 5px;
    vertical-align: middle;
  }

  .icon-icon-seta-direita-a {
    color: black;
    margin-right: 5px;
    margin-left: 5px;
    font-size: 12px;
    vertical-align: middle;

    &:nth-child(2) {
      margin-left: 5px;
      margin-right: 5px;
    }
  }
  .last {
    font-weight: bold;
  }
  .link {
    color: ${LightBlueBradesco};
    text-decoration: none;
    cursor: pointer;
  }
`;
