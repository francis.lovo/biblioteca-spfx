import React from "react";
import * as S from "./styles";

interface IData {
  itemNames: string[];
  itemTexts: string[];
}

export interface IArqFaqProps {
  buttons?: string[];
  data?: IData[];
}
export interface MyState {
  buttonSelected: number;
  itemSelected: number;
}

export class ArqFaq extends React.Component<IArqFaqProps, MyState> {
  state: MyState = {
    buttonSelected: 0,
    itemSelected: -1,
  };
  public render(): JSX.Element {
    return (
      <S.StyledFaqContainer>
        <S.StyledButtonCantainer>
          {this.props.buttons &&
            this.props.buttons.length > 0 &&
            this.props.buttons.map((buttonString: string, index: number) => {
              return (
                <S.StyledButton
                  buttonSelected={this.state.buttonSelected === index}
                  onClick={() => {
                    this.setState({ buttonSelected: index, itemSelected: -1 });
                  }}
                >
                  {buttonString}
                </S.StyledButton>
              );
            })}
        </S.StyledButtonCantainer>
        <S.StyledItemsContainer>
          {!!this.props.data &&
            this.props.data.length > 0 &&
            this.props.data.map((dataI: IData, index: number) => {
              return (
                <>
                  {dataI.itemNames.map((itemName: string, i: number) => {
                    return (
                      <>
                        {index === this.state.buttonSelected && (
                          <S.StyledItem>
                            <S.StyledTitleItem>
                              {itemName}
                              {dataI.itemTexts &&
                                dataI.itemTexts.length > 0 &&
                                dataI.itemTexts[i] && (
                                  <S.StyledIcon
                                    className={
                                      this.state.itemSelected === i
                                        ? "icon-icon-seta-cima-a"
                                        : "icon-icon-seta-baixo-a"
                                    }
                                    onClick={() => {
                                      if (this.state.itemSelected !== i) {
                                        this.setState({ itemSelected: i });
                                      } else {
                                        this.setState({ itemSelected: -1 });
                                      }
                                    }}
                                  />
                                )}
                            </S.StyledTitleItem>
                            {this.state.itemSelected === i &&
                              dataI.itemTexts &&
                              dataI.itemTexts.length > 0 &&
                              dataI.itemTexts[i] && (
                                <S.StyledTextItem>
                                  {dataI.itemTexts[i]}
                                </S.StyledTextItem>
                              )}
                          </S.StyledItem>
                        )}
                      </>
                    );
                  })}
                </>
              );
            })}
        </S.StyledItemsContainer>
      </S.StyledFaqContainer>
    );
  }
}
