import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const StyledButton = styled.div<{ buttonSelected: boolean }>`
  cursor: pointer;
  color: ${({ buttonSelected }) => (buttonSelected ? "#fff" : "#4f4f4f")};
  background: ${({ buttonSelected }) =>
    buttonSelected ? RedBradesco : "#fff"};
  padding: 10px;
  border: 1px solid #cacaca;
  border-radius: 4px;
`;
export const StyledButtonCantainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;
export const StyledFaqContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  font-family: "Bradesco Regular Sans";
`;
export const StyledItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 25px;
`;
export const StyledItem = styled.div`
  display: flex;
  flex-direction: column;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  margin-bottom: 15px;
  padding: 15px;
  border-radius: 5px;
  color: #4f4f4f;
`;
export const StyledTitleItem = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  font-size: 16px;
  font-weight: 600;
`;
export const StyledIcon = styled.i`
  font-weight: 900;
`;
export const StyledTextItem = styled.div`
  margin-top: 10px;
  font-size: 12px;
`;
