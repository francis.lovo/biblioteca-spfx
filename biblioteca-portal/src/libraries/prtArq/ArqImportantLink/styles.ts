import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const StyledImportantLink = styled.a`
  text-decoration: none;
  color: ${RedBradesco};
  font-weight: bold;
  font-family: Bradesco Regular Sans;
  cursor: pointer;
`;
export const StyledLinkContainer = styled.div`
  display: flex;
  flex-direction: row;
  font-size: 14px;
`;
export const StyledLinkIcon = styled.img`
  width: 18px;
  height: 18px;
  margin-left: 5px;
`;
