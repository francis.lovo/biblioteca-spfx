import React from "react";
import * as S from "./styles";

export interface IArqImportantLinkProps {
  href?: string;
  text?: string;
}

export class ArqImportantLink extends React.Component<
  IArqImportantLinkProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <S.StyledImportantLink href={this.props.href}>
        <S.StyledLinkContainer>
          {this.props.text}
          <S.StyledLinkIcon
            src={require("../../../assets/link-externo.png")}
            alt="Icon"
          />
        </S.StyledLinkContainer>
      </S.StyledImportantLink>
    );
  }
}
