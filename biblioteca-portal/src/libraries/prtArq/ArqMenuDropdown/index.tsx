import React, { Component } from "react";
import * as S from "./styles";

interface IItems {
  name: string;
  dropdownItems: string[];
  link?: string;
}

export interface IArqMenuDropdownProps {
  items?: IItems[];
  title?: string;
  link?: string;
}

export interface MyState {
  showDrop: number | undefined;
  containerHeight: string; // Adicione uma propriedade para controlar a altura do contêiner pai
}

export class ArqMenuDropdown extends Component<IArqMenuDropdownProps, MyState> {
  state: MyState = {
    showDrop: undefined,
    containerHeight: "auto", // Inicialmente, definimos a altura para 'auto'
  };

  public render(): JSX.Element {
    return (
      <S.StyledContainer>
        <S.StyledTitle>{this.props.title}</S.StyledTitle>
        {this.props.items &&
          this.props.items.length > 0 &&
          this.props.items.map((item: IItems, index: number) => {
            return (
              <S.StyledItemsContainer
                key={index}
                style={{ height: this.state.containerHeight }} // Aplicamos a altura ao contêiner pai
              >
                <S.StyledItemName onClick={() => this.handleClick(index)}>
                  {item.link ? (
                    <S.StyledLink href={item.link}>{item.name}</S.StyledLink>
                  ) : (
                    <span>{item.name}</span>
                  )}
                  {item.dropdownItems && item.dropdownItems.length > 0 && (
                    <S.StyledItemIcon
                      className={
                        this.state.showDrop === index
                          ? "icon-icon-seta-cima-a"
                          : "icon-icon-seta-baixo-a"
                      }
                    />
                  )}
                </S.StyledItemName>
                {!!item.dropdownItems && item.dropdownItems.length > 0 && (
                  <S.StyledDropdownContainer
                    show={this.state.showDrop === index}
                    onTransitionEnd={this.handleTransitionEnd}
                  >
                    {item.dropdownItems.map(
                      (drop: string, dropIndex: number) => (
                        <S.StyledDropdownItem
                          key={dropIndex}
                          show={this.state.showDrop === index}
                        >
                          {drop}
                        </S.StyledDropdownItem>
                      )
                    )}
                  </S.StyledDropdownContainer>
                )}
              </S.StyledItemsContainer>
            );
          })}
      </S.StyledContainer>
    );
  }

  private handleClick(index: number): void {
    if (this.state.showDrop === index) {
      this.setState({ showDrop: undefined, containerHeight: "auto" }); // Redefinir a altura para 'auto'
    } else {
      this.setState({ showDrop: index, containerHeight: "auto" });
    }
  }

  private handleTransitionEnd = (): void => {
    if (this.state.showDrop === null) {
      this.setState({ containerHeight: "auto" });
    } else {
      this.setState({ containerHeight: "auto" }); // Ajuste a altura conforme necessário
    }
  };
}
