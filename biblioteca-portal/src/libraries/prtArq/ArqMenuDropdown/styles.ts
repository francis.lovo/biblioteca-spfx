import styled from 'styled-components';

export const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
  font-family: 'Bradesco Regular Sans';
  color: #4f4f4f;
  padding: 15px;
  padding-left: 60px;
  text-decoration: none;
`;
export const StyledTitle = styled.div`
  width: 100%;
  font-size: 18px;
  font-weight: bold;
  padding-top: 10px;
`;
export const StyledItems = styled.div`
  font-size: 14px;
  font-weight: bold;
`;

export const StyledItemName = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
`;
export const StyledItemIcon = styled.i`
  font-weight: 900;
  font-size: 14px;
  margin-left: 5px;
`;
export const StyledDropdownItem = styled.div<{ show: boolean }>`
  font-size: 14px;
  margin-left: 10px;
  margin-top: 10px;
  color: ${({ show }) => (show ? '#4f4f4f' : 'transparent')};
`;

export const StyledLink = styled.a`
  text-decoration: none;
  color: #4f4f4f;
  
  
  &:hover {
    
    text-decoration: none;
    color: #4f4f4f;
    
  }
`;

export const StyledItemsContainer = styled.div`
  width: 25%;
  margin-top: 25px;
  display: flex;
  flex-direction: column;
  height: auto;
`;

export const StyledDropdownContainer = styled.div<{ show: boolean }>`
  max-height: ${({ show }) => (show ? '200px' : '0')};
  overflow: hidden;
  transition: max-height 0.3s ease-in-out;
`;
