import * as React from "react";
import styled from "styled-components";
import { ArqCardBirthday } from "../ArqCardBirthday";

export interface IArqCardAniversariantesProps {
  names?: string[];
  dates?: string[];
  infos?: string[];
  month: string;
}

const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: left;
  font-family: Bradesco Regular Sans;
  width: 100%;
  height: 400px;
  border-radius: 5px;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  padding: 15px;
`;

const Title = styled.p`
  font-weight: bold;
  text-align: left;
  margin-bottom: 0px;
`;

const Month = styled.p`
  color: red;
  text-align: left;
  font-size: 10px;
`;

const ScrollableContainer = styled.div`
  max-height: 400px;
  overflow-y: auto;
  margin-top: 8px;

  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #555;
    border-radius: 4px;
  }

  ::-webkit-scrollbar-track {
    background-color: #f5f5f5;
    border-radius: 4px;
  }
`;

export class ArqCardAniversariantes extends React.Component<
  IArqCardAniversariantesProps,
  {}
> {
  initializeScrollbar = (): void => {
    // Create custom scrollbar functionality
    const container = document.getElementById("scrollableContainer");
    if (container) {
      const scrollbar = document.createElement("div");
      scrollbar.className = "custom-scrollbar";

      const thumb = document.createElement("div");
      thumb.className = "custom-scrollbar-thumb";
      scrollbar.appendChild(thumb);

      container.appendChild(scrollbar);
    }
  };

  componentDidMount(): () => void {
    // Initialize custom scrollbar when component mounts
    const container = document.getElementById("scrollableContainer");
    if (container) {
      container.addEventListener("mouseenter", this.initializeScrollbar);
    }

    return () => {
      // Clean up event listener when component unmounts
      if (container) {
        container.removeEventListener("mouseenter", this.initializeScrollbar);
      }
    };
  }
  public render(): JSX.Element {
    return (
      <CardWrapper>
        <Title>Aniversariantes do Mês</Title>
        <Month>{this.props.month}</Month>
        <ScrollableContainer id="scrollableContainer">
          {this.props.names &&
            this.props.names.map((name, i) => {
              return (
                <ArqCardBirthday
                  name={name}
                  date={
                    this.props.dates && this.props.dates[i]
                      ? this.props.dates[i]
                      : undefined
                  }
                  info={
                    !!this.props.infos && this.props.infos[i]
                      ? this.props.infos[i]
                      : undefined
                  }
                />
              );
            })}
        </ScrollableContainer>
      </CardWrapper>
    );
  }
}
