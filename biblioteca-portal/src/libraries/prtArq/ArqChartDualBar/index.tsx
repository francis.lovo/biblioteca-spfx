/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import * as S from "./styles";

interface IChartData {
  label: string;
  value1: number;
  value2: number;
}
export interface IArqChartDualBarsProps {
  chartData?: IChartData[];
  chartYData?: number[];
  firstColor?: string;
  secColor?: string;
}
export interface MyState {
  maior: number;
}

export class ArqChartDualBars extends React.Component<
  IArqChartDualBarsProps,
  MyState
> {
  state: MyState = {
    maior: 0,
  };
  procurarMaior = (): void => {
    let maiorAux = 0;
    if (this.props.chartYData) {
      this.props.chartYData.forEach((item) => {
        if (item > maiorAux) {
          maiorAux = item;
        }
      });
    }
    this.setState({ maior: maiorAux });
  };
  componentDidMount(): void {
    this.procurarMaior();
  }
  componentDidUpdate(prevProps: any): void {
    if (this.props.chartData !== prevProps.chartData) {
      this.procurarMaior();
    }
  }
  public render(): JSX.Element {
    return (
      <S.StyledContainerDual
        maior={this.state.maior < 10 ? this.state.maior * 5 : this.state.maior}
      >
        {this.props.chartYData &&
          this.props.chartYData.map((data, index) => {
            return (
              <S.StyledSpanYDual
                y={this.state.maior < 10 ? data * 5 : data}
                key={index}
              >
                {data}
              </S.StyledSpanYDual>
            );
          })}
        <S.StyledAxisYDual />
        <S.StyledRowContainerDual>
          {this.props.chartData &&
            this.props.chartData.map((item, index) => (
              <S.StyledDualBar key={index}>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "50%",
                    alignItems: "center",
                  }}
                >
                  <S.StyledNumberBarLabel>{item.value1}</S.StyledNumberBarLabel>
                  <S.StyledBarSegment
                    blue={true}
                    style={{
                      height: `${
                        this.state.maior < 10
                          ? item.value1 * 25
                          : item.value1 * 5
                      }px`,
                      backgroundColor: this.props.firstColor,
                    }}
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    width: "50%",
                    alignItems: "center",
                  }}
                >
                  <S.StyledNumberBarLabel>{item.value2}</S.StyledNumberBarLabel>
                  <S.StyledBarSegment
                    style={{
                      height: `${
                        this.state.maior < 10
                          ? item.value2 * 25
                          : item.value1 * 5
                      }px`,
                      backgroundColor: this.props.secColor,
                    }}
                  />
                </div>
              </S.StyledDualBar>
            ))}
        </S.StyledRowContainerDual>
        <S.StyledlabelXContainerDual>
          {this.props.chartData &&
            this.props.chartData.map((data, index) => {
              return (
                <S.StyledSpanXDual key={index}>{data.label}</S.StyledSpanXDual>
              );
            })}
        </S.StyledlabelXContainerDual>
      </S.StyledContainerDual>
    );
  }
}
