import * as React from "react";
import * as S from "./styles";

export interface IArqChartBarsProps {
  chartData?: number[];
  chartYData?: number[];
  chartXData?: string[];
  color?: string;
}
export interface MyState {
  maior: number;
}

export class ArqChartBars extends React.Component<IArqChartBarsProps, MyState> {
  state: MyState = {
    maior: 0,
  };
  public render(): JSX.Element {
    const buscaMaior = (): void => {
      let maiorAux = 0;
      if (this.props.chartYData) {
        this.props.chartYData.forEach((data) => {
          if (data > maiorAux) {
            maiorAux = data;
          }
        });
      }
      this.setState({ maior: maiorAux });
    };
    React.useEffect(() => {
      buscaMaior();
    }, [this.props.chartYData]);

    return (
      <S.StyledContainer
        maior={this.state.maior < 10 ? this.state.maior * 5 : this.state.maior}
      >
        {this.props.chartYData &&
          this.props.chartYData.map((data, index) => {
            return (
              <S.StyledSpanY
                y={this.state.maior < 10 ? data * 5 : data}
                key={index}
              >
                {data}
              </S.StyledSpanY>
            );
          })}
        <S.StyledAxisY />
        <S.StyledAxisX>
          {this.props.chartData &&
            this.props.chartData.map((data, index) => {
              return (
                <S.StyledBar
                  key={index}
                  data={this.state.maior < 10 ? data * 5 : data}
                  color={this.props.color}
                />
              );
            })}
        </S.StyledAxisX>
        <S.StyledlabelXContainer>
          {this.props.chartXData &&
            this.props.chartXData.map((data, index) => {
              return <S.StyledSpanX key={index}>{data}</S.StyledSpanX>;
            })}
        </S.StyledlabelXContainer>
      </S.StyledContainer>
    );
  }
}
