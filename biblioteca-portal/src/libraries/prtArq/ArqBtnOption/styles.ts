import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const Container = styled.div`
  text-align: left;
  margin-bottom: 20px;
  font-family: "Bradesco Regular Sans";
`;

export const Question = styled.p`
  font-weight: bold;
  color: #4f4f4f;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: left;
  gap: 30px;
`;

export const Button = styled.button<{ selected: boolean }>`
  border: none;
  width: 120px;
  border: 1px solid #f1f1f1;
  border-radius: 20px;
  padding: 10px 20px;
  background-color: ${(props) => (props.selected ? RedBradesco : "white")};
  color: ${(props) => (props.selected ? "#ffffff" : "#4f4f4f")};
  cursor: pointer;
`;
