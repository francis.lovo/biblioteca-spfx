import * as React from "react";
import * as S from "./styles";

export interface IArqBtnOptionProps {
  question?: string;
  selected?: "sim" | "nao";
  onChange?: (value: boolean) => void;
}
export interface MyState {
  buttonSelected: "sim" | "nao" | undefined;
}

export class ArqBtnOption extends React.Component<IArqBtnOptionProps, MyState> {
  state: MyState = {
    buttonSelected: this.props.selected,
  };
  componentDidMount(): void {
    this.setState({ buttonSelected: this.props.selected }); //quando inicia
  }
  /*componentDidUpdate(): void {
    this.setState({ buttonSelected: this.props.selected }); //quando o estado seta
  }*/
  public render(): JSX.Element {
    const handleButtonClick = (option: "sim" | "nao"): void => {
      this.setState({ buttonSelected: option });
      if (this.props.onChange) {
        this.props.onChange(option === "sim");
      }
    };
    return (
      <div>
        <S.Container>
          {this.props.question && (
            <S.Question>{this.props.question}</S.Question>
          )}
        </S.Container>
        <S.ButtonContainer>
          <S.Button
            selected={this.state.buttonSelected === "sim"}
            onClick={() => handleButtonClick("sim")}
          >
            Sim
          </S.Button>
          <S.Button
            selected={this.state.buttonSelected === "nao"}
            onClick={() => handleButtonClick("nao")}
          >
            Não
          </S.Button>
        </S.ButtonContainer>
      </div>
    );
  }
}
