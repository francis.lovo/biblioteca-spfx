import React from "react";
import { LineChart, Line, XAxis, YAxis } from "recharts";
import * as S from "./styles";

interface IDotsChart {
  x: number;
  y: number;
}
export interface IArqLineChartProps {
  dots?: IDotsChart[];
  chartWidth: number;
  chartHeight: number;
}

export class ArqLineChart extends React.Component<IArqLineChartProps, {}> {
  public render(): JSX.Element {
    return (
      <S.ChartContainer>
        <LineChart
          width={this.props.chartWidth}
          height={this.props.chartHeight}
          data={this.props.dots}
        >
          <XAxis dataKey="x" />
          <YAxis dataKey="y" />
          <Line dataKey="y" stroke="#8884d8" dot={{ fill: "blue", r: 4 }} />
        </LineChart>
      </S.ChartContainer>
    );
  }
}
