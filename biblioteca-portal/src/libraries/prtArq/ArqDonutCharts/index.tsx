import React from "react";
import * as S from "./styles";

export interface DonutChartProps {
  data: {
    label: string;
    value: number;
    color: string;
  }[];
  radius: number;
  strokeWidth: number;
}
export class ArqDonutCharts extends React.Component<DonutChartProps, {}> {
  public render(): JSX.Element {
    const total = this.props.data.reduce((sum, item) => sum + item.value, 0);
    const centerX = this.props.radius;
    const centerY = this.props.radius;

    let currentAngle = -145;

    return (
      <S.Container>
        <S.SVG
          width={this.props.radius * 2}
          height={this.props.radius * 2}
          radius={this.props.radius}
        >
          <circle
            cx={centerX}
            cy={centerY}
            r={this.props.radius - this.props.strokeWidth / 2}
            fill="transparent"
            strokeWidth={this.props.strokeWidth}
          />
          {this.props.data.map((item) => {
            const { label, value, color } = item;
            const percentage = (value / total) * 100;

            const startAngle = currentAngle;
            const endAngle = currentAngle + (360 * percentage) / 100;

            const startRadians = (startAngle * Math.PI) / 180;
            const endRadians = (endAngle * Math.PI) / 180;

            const startX =
              centerX +
              Math.cos(startRadians) *
                (this.props.radius - this.props.strokeWidth / 2);
            const startY =
              centerY +
              Math.sin(startRadians) *
                (this.props.radius - this.props.strokeWidth / 2);

            const endX =
              centerX +
              Math.cos(endRadians) *
                (this.props.radius - this.props.strokeWidth / 2);
            const endY =
              centerY +
              Math.sin(endRadians) *
                (this.props.radius - this.props.strokeWidth / 2);

            const largeArcFlag = percentage <= 50 ? "0" : "1";

            const pathData = `M ${startX} ${startY} A ${
              this.props.radius - this.props.strokeWidth / 2
            } ${
              this.props.radius - this.props.strokeWidth / 2
            } 0 ${largeArcFlag} 1 ${endX} ${endY}`;

            currentAngle = endAngle;

            return (
              <path
                key={label}
                d={pathData}
                fill="transparent"
                stroke={color}
                strokeWidth={this.props.strokeWidth}
              />
            );
          })}
        </S.SVG>
        <S.LegendContainer>
          {this.props.data.map((item) => (
            <S.LegendItem key={item.label}>
              <S.LegendColor color={item.color} />
              <span>{item.label}</span>
            </S.LegendItem>
          ))}
        </S.LegendContainer>
      </S.Container>
    );
  }
}
