import React from "react";
import { ArqButton } from "../ArqButton";
import { ArqModal } from "../ArqModal";
import * as S from "./styles";

export interface IArqSuccessModalProps {
  isOpen?: boolean;
  onClose?: () => void;
  onClick?: () => void;
}

export class ArqSuccessModal extends React.Component<
  IArqSuccessModalProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <ArqModal
        topLine={true}
        modalWidth="300px"
        isOpen={this.props.isOpen}
        onClose={this.props.onClose}
        noCloseButton={true}
        padding={0}
      >
        <S.StyledModalContainer>
          <S.StyledIcon className="icon-icon-alerta-sucesso" />
          <S.MessageContainer>
            <S.WarningMessage>Enviado com Sucesso</S.WarningMessage>
            <ArqButton text="Ok" onClick={this.props.onClick} styleType="filed" color="#e60935"/>
          </S.MessageContainer>
        </S.StyledModalContainer>
      </ArqModal>
    );
  }
}
