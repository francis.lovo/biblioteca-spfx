import styled from "styled-components";
import { RedBradesco } from "../../../colors";

export const StyledLetterButton = styled.a<{ selected: boolean }>`
  width: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #cacaca;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  padding-bottom: 2px;
  cursor: pointer;
  color: ${({ selected }) => (selected ? "#fff" : "#4f4f4f")};
  background: ${({ selected }) => (selected ? RedBradesco : "#fff")};
  font-weight: bold;
  font-size: 16px;
  z-index: 100;
  text-decoration: none;
`;
export const StyledLettersContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-around;
`;
export const StyledGlossaryContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-family: Bradesco Regular Sans;
  width: 70%;
`;
export const StyledDropDownGlossaryContainer = styled.div`
  padding: 5px;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  width: 100%;
  height: 410px;
  overflow-y: hidden;
`;
export const StyledDropDownLetter = styled.div<{ first: boolean }>`
  color: ${RedBradesco};
  font-weight: bold;
  border-top: ${({ first }) => (first ? "" : "1px solid #cacaca")};
  border-bottom: 1px solid #cacaca;
  font-size: 16px;
  margin-bottom: 10px;
`;
export const ScrollableContainer = styled.div`
  max-height: 400px;
  overflow-y: auto;
  margin-top: 8px;

  /* Custom Scrollbar Styles */
  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-thumb {
    background-color: #555;
    border-radius: 4px;
  }

  ::-webkit-scrollbar-track {
    background-color: #f5f5f5;
    border-radius: 4px;
  }
`;
export const StyledItem = styled.div`
  color: ${RedBradesco};
  font-weight: bold;
  font-size: 14px;
  margin-bottom: 10px;
  cursor: pointer;
`;
export const StyledNewTermContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 15px;
  width: max-content;
  cursor: pointer;
`;
export const StyledNewTermText = styled.div`
  color: ${RedBradesco};
  font-weight: bold;
  font-size: 14px;
`;
export const StyledNewTermIcon = styled.div`
  color: ${RedBradesco};
  font-weight: 900;
  font-size: 14px;
  margin-right: 5px;
`;
