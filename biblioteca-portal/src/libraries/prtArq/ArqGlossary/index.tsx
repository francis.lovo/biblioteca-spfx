import React from "react";
import * as S from "./styles";

export interface IArqGlossaryProps {
  items?: string[];
  onChange?: (items: string[]) => void;
  onClick?: (itemClicked: string) => void;
}
export interface MyState {
  selected: string;
  realItems: string[];
}
export class ArqGlossary extends React.Component<IArqGlossaryProps, MyState> {
  state: MyState = {
    selected: "a",
    realItems: this.props.items ? this.props.items : [],
  };
  initializeScrollbar = (): void => {
    // Create custom scrollbar functionality
    const container = document.getElementById("scrollableContainer");
    if (container) {
      const scrollbar = document.createElement("div");
      scrollbar.className = "custom-scrollbar";

      const thumb = document.createElement("div");
      thumb.className = "custom-scrollbar-thumb";
      scrollbar.appendChild(thumb);

      container.appendChild(scrollbar);
    }
  };

  componentDidMount(): () => void {
    // Initialize custom scrollbar when component mounts
    const container = document.getElementById("scrollableContainer");
    if (container) {
      container.addEventListener("mouseenter", this.initializeScrollbar);
    }

    return () => {
      // Clean up event listener when component unmounts
      if (container) {
        container.removeEventListener("mouseenter", this.initializeScrollbar);
      }
    };
  }
  public render(): JSX.Element {
    const letters = [
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z",
    ];
    const changeSelected = (value: string): void => {
      this.setState({ selected: value });
    };
    //const addNewterm = (): void => {
    // if (this.props.onChange && this.state.realItems) {
    //this.props.onChange(this.state.realItems);
    // } else if (this.props.onChange && !this.state.realItems) {
    // this.props.onChange([]);
    // }
    //};
    return (
      <S.StyledGlossaryContainer>
        <S.StyledLettersContainer>
          {letters.map((letter: string) => {
            return (
              <S.StyledLetterButton
                onClick={() => changeSelected(letter)}
                selected={this.state.selected === letter}
                href={`#${letter}`}
              >
                {letter.toLocaleUpperCase()}
              </S.StyledLetterButton>
            );
          })}
        </S.StyledLettersContainer>
        <S.StyledDropDownGlossaryContainer>
          <S.ScrollableContainer id="scrollableContainer">
            {letters.map((letter: string, index: number) => {
              return (
                <>
                  <S.StyledDropDownLetter first={index === 0} id={letter}>
                    {letter.toLocaleUpperCase()}
                  </S.StyledDropDownLetter>
                  {this.state.realItems &&
                    this.state.realItems.length > 0 &&
                    this.state.realItems
                      .sort(Intl.Collator().compare)
                      .map((item: string) => {
                        if (
                          item.charAt(0) === letter ||
                          item.charAt(0) === letter.toLocaleUpperCase()
                        ) {
                          return (
                            <S.StyledItem
                              onClick={() => {
                                if (this.props.onClick)
                                  this.props.onClick(item);
                              }}
                            >
                              {item}
                            </S.StyledItem>
                          );
                        } else {
                          return "";
                        }
                      })}
                </>
              );
            })}
          </S.ScrollableContainer>
        </S.StyledDropDownGlossaryContainer>
        {/*<S.StyledNewTermContainer onClick={addNewterm}>
          <S.StyledNewTermIcon className="icon-icon-nav-adicionar" />
          <S.StyledNewTermText>Novo termo</S.StyledNewTermText>
          </S.StyledNewTermContainer>*/}
      </S.StyledGlossaryContainer>
    );
  }
}
