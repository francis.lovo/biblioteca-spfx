import React from "react";
import * as S from "./styles";
import { ApprovedIcons } from "../../../types";

export interface IArqIconCardProps {
  icon?: ApprovedIcons;
  text?: string;
  isSelected?: boolean;
  onChange?: (value: boolean) => void;
}
export interface MyState {
  selected: boolean;
}

export class ArqIconCard extends React.Component<IArqIconCardProps, MyState> {
  state: MyState = {
    selected: this.props.isSelected ? this.props.isSelected : false,
  };
  public render(): JSX.Element {
    const handleClick = (): void => {
      if (this.props.onChange) {
        this.props.onChange(!this.state.selected);
      }
      this.setState({ selected: !this.state.selected });
    };
    return (
      <S.Card isSelected={this.state.selected} onClick={handleClick}>
        {!!this.props.icon && (
          <S.CardIcon
            className={this.props.icon}
            isSelected={this.state.selected}
          />
        )}
        <S.CardText isSelected={this.state.selected}>
          {this.props.text}
        </S.CardText>
      </S.Card>
    );
  }
}
