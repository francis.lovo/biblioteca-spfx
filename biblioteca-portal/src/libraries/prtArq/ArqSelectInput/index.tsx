/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import * as S from "./styles";

export interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

export interface IArqSelectInputProps {
  options?: ISelect[];
  noSelectedLabel?: string;
  label?: string;
  onChange?: (value: ISelect) => void;
  emptyField?: boolean;
  selectedOption?: ISelect;
}
export interface MyState {
  isOpen: boolean;
  selectedOptionLabel: string | undefined;
  selectedOption: ISelect | undefined;
}
export class ArqSelectInput extends React.Component<
  IArqSelectInputProps,
  MyState
> {
  state: MyState = {
    isOpen: false,
    selectedOptionLabel: this.props.selectedOption
      ? this.props.selectedOption.label
      : undefined,
    selectedOption: this.props.selectedOption
      ? this.props.selectedOption
      : undefined,
  };
  componentDidUpdate(
    prevProps: Readonly<IArqSelectInputProps>,
    prevState: Readonly<MyState>,
    snapshot?: any
  ): void {
    if (prevProps.selectedOption !== this.props.selectedOption) {
      this.setState({
        selectedOption: this.props.selectedOption,
        selectedOptionLabel: this.props.selectedOption?.label,
      });
    }
  }
  public render(): JSX.Element {
    const chooseOp = (option: ISelect): void => {
      if (this.props.onChange) {
        this.props.onChange(option);
      }
      this.setState({
        selectedOptionLabel: option.label,
        selectedOption: option,
        isOpen: false,
      });
    };

    return (
      <div>
        {!!this.props.label && (
          <S.StyledLabel>{this.props.label}</S.StyledLabel>
        )}
        <S.DropdownContainer>
          <S.DropdownButton
            onClick={() => {
              this.setState({ isOpen: !this.state.isOpen });
            }}
            emptyField={this.props.emptyField || false}
          >
            <S.DropdownButtonText>
              {this.state.selectedOptionLabel ||
                this.props.noSelectedLabel ||
                "Selecione"}
            </S.DropdownButtonText>
            <S.ArrowIcon className="icon-icon-seta-baixo-b" />
          </S.DropdownButton>
          <S.DropdownList isOpen={this.state.isOpen}>
            {!!this.props.options &&
              this.props.options?.map((option: ISelect) => {
                return (
                  <S.DropdownItem
                    onClick={() => {
                      // eslint-disable-next-line no-unused-expressions
                      !option.disabled ? chooseOp(option) : {};
                    }}
                    selected={this.state.selectedOption === option}
                    isDisabled={option.disabled ? option.disabled : false}
                  >
                    {option.label}
                  </S.DropdownItem>
                );
              })}
          </S.DropdownList>
        </S.DropdownContainer>
      </div>
    );
  }
}
