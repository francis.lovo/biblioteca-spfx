import styled, { css } from "styled-components";

interface IDropdownButtonProps {
  emptyField: boolean;
}

export const StyledLabel = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  margin-bottom: 10px;
`;

export const DropdownContainer = styled.div`
  width: 100%;
  position: relative;
  margin-top: 5px;
`;
export const DropdownButton = styled.button<IDropdownButtonProps>`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  text-align: left;
  cursor: pointer;
  padding: 10px;
  border: ${(props) => (props.emptyField ? '1px solid red' : '1px solid #b8b8b8')};
  border-radius: 4px;
`;
export const DropdownButtonText = styled.span`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: #4f4f4f;
  font-family: Bradesco Regular Sans;
`;
export const ArrowIcon = styled.i`
  margin-left: 4px;
  font-size: 16px;
  color: #4f4f4f;
`;
export const DropdownItem = styled.li<{
  selected: boolean;
  isDisabled: boolean;
}>`
  ${({ isDisabled }) =>
    isDisabled
      ? ""
      : css`
          &:hover {
            background-color: #efefef;
          }
        `}

  list-style: none;
  padding: 8px;
  cursor: ${({ isDisabled }) => (isDisabled ? "" : "pointer")};
  background-color: #fff;
  color: ${({ isDisabled }) => (isDisabled ? "#b8b8b8" : "#4f4f4f")};
  font-weight: ${({ selected }) => (selected ? "bold" : "")};
  font-family: Bradesco Regular Sans;
`;
export const DropdownList = styled.ul<{ isOpen: boolean }>`
  flex-direction: column;
  margin: 0;
  padding: 0;
  position: absolute;
  width: 100%;
  z-index: 100;
  max-height: 216px;
  overflow-y: auto;
  background-color: #fff;
  list-style: none;
  border-radius: 4px;
  box-shadow: 0 2px 4px rgba(72, 91, 98, 0.12), 0 2px 2px rgba(72, 91, 98, 0.2);
  display: ${({ isOpen }) => (isOpen ? "flex" : "none")};
`;
