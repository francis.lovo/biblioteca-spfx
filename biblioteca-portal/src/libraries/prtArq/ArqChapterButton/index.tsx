import * as React from "react";
import * as S from "./styles";

export interface IArqChapterButtonProps
  extends Partial<S.IArqChapterButtonProps> {
  titles?: string[];
  subtitles?: string[];
  links?: string[];
}

export class ArqChapterButton extends React.Component<
  IArqChapterButtonProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <div style={{ width: "100%" }}>
        {this.props.titles &&
          this.props.titles.map((title: string, index: number) => {
            if (this.props.open || (!this.props.open && index === 0)) {
              return (
                <S.StyledChapterButton
                  color={this.props.color}
                  open={this.props.open}
                  index={index}
                  last={this.props.titles && this.props.titles.length - 1}
                  key={index}
                  href={
                    !!this.props.links &&
                    this.props.links.length > 0 &&
                    !!this.props.links[index]
                      ? this.props.links[index]
                      : undefined
                  }
                >
                  <S.StyledChapterButtonTexts>
                    <S.StyledChapterButtonTitle index={index}>
                      {title}
                    </S.StyledChapterButtonTitle>
                    {!!this.props.subtitles &&
                      this.props.subtitles.length > 0 &&
                      !!this.props.subtitles[index] && (
                        <S.StyledChapterButtonSubtitle>
                          {this.props.subtitles[index]}
                        </S.StyledChapterButtonSubtitle>
                      )}
                  </S.StyledChapterButtonTexts>
                  <S.StyledChapterButtonIcon className="icon-icon-seta-direita-b" />
                </S.StyledChapterButton>
              );
            } else {
              return "";
            }
          })}
      </div>
    );
  }
}
