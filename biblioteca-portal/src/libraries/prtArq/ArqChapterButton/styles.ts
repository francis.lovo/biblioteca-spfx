import styled, { css } from "styled-components";
import { LightBlueBradesco } from "../../../colors";

export interface IArqChapterButtonProps {
  color?: string;
  open?: boolean;
}
export interface IArqChapterButtonOpenProps
  extends Partial<IArqChapterButtonProps> {
  index?: number;
  last?: number;
}

export const StyledChapterButton = styled.a<IArqChapterButtonOpenProps>`
  display: flex;
  background-color: rgb(251, 249, 249);
  color: rgb(57, 57, 57);
  padding: 10px 20px;
  border: none;
  border-left: 6px solid ${({ color }) => color};
  align-items: center;
  justify-content: space-between;
  height: 70px;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  ${({ index, last, open }) =>
    index === 0 && open
      ? css`
          border-top-left-radius: 4px;
          border-top-right-radius: 4px;
          border-bottom-right-radius: 0px;
          border-bottom-left-radius: 0px;
        `
      : index === last && open
      ? css`
          border-top-left-radius: 0px;
          border-top-right-radius: 0px;
          border-bottom-right-radius: 4px;
          border-bottom-left-radius: 4px;
        `
      : !open
      ? css`
          border-radius: 4px;
        `
      : ""};
  cursor: ${({ href }) => (!!href ? "pointer" : "")};
  text-decoration: none;
  font-family: Bradesco Regular Sans;
`;
export const StyledChapterButtonTitle = styled.div<IArqChapterButtonOpenProps>`
  display: flex;
  align-items: left;
  justify-content: flex-start;
  text-align: left;
  font-weight: ${({ index }) => (index === 0 ? "bold" : "normal")};
`;
export const StyledChapterButtonSubtitle = styled.div`
  margin-top: 4px;
  color: rgb(167, 167, 167);
  display: flex;
  align-items: center;
  justify-content: flex-start;
  text-align: left;
  font-size: 12px;
  font-weight: light;
`;

export const StyledChapterButtonTexts = styled.div`
  display: flex;
  flex-direction: column;
  align-items: left;
  text-align: left;
`;

export const StyledChapterButtonIcon = styled.i`
  color: ${LightBlueBradesco};
  font-size: 20px;
`;
