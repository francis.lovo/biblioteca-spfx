import styled, { css } from 'styled-components';

export interface IArqImportantLinkListProps {
  first?: boolean;
}

export const StyledImportantLinkList = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.1), 4px 0 6px rgba(0, 0, 0, 0.1);
  padding-left: 10px;
  padding-right: 10px;
  justify-content: space-between;
`;

export const StyledLink = styled.div<IArqImportantLinkListProps>`
  width: 25%;
  margin-bottom: 25px;
  ${({ first }) =>
    first
      ? css`
          margin-top: 20px;
        `
      : ''}
`;

export const StyledTitle = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  margin-bottom: 10px;
  color: #5f5f5f;
`;

export const StyledPageText = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #5f5f5f;
  font-size: 12px;
`;
