import React from "react";
import { ArqImportantLink } from "../ArqImportantLink";
import * as S from "./styles";
import { ArqIconButton } from "../ArqIconButton";

export interface IArqImportantLinkListProps {
  links?: string[];
  texts?: string[];
  totalPages?: string;
  currentPage?: string;
  onClickPageUp?: () => void;
  onClickPageDown?: () => void;
  onClickLastPage?: () => void;
  onClickFirstPage?: () => void;
}

export class ArqImportantLinkList extends React.Component<
  IArqImportantLinkListProps,
  {}
> {
  public render(): JSX.Element {
    return (
      <div>
        <S.StyledTitle>Links Importantes</S.StyledTitle>
        <S.StyledImportantLinkList>
          {this.props.texts &&
            this.props.texts.map((text, i) => {
              return (
                <S.StyledLink first={i < 4}>
                  <ArqImportantLink
                    text={text}
                    href={
                      this.props.links && this.props.links[i]
                        ? this.props.links[i]
                        : undefined
                    }
                  />
                </S.StyledLink>
              );
            })}
        </S.StyledImportantLinkList>
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "flex-end",
            marginTop: "10px",
          }}
        >
          <div
            style={{
              width: "20%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}
          >
            <S.StyledPageText>
              {this.props.currentPage} de {this.props.totalPages}
            </S.StyledPageText>
            <ArqIconButton
              icon="icon-icon-seta-dupla-b"
              fontSize="16px"
              onClick={this.props.onClickFirstPage}
            />
            <ArqIconButton
              icon="icon-icon-seta-esquerda-b"
              fontSize="16px"
              onClick={this.props.onClickPageUp}
            />
            <ArqIconButton
              icon="icon-icon-seta-direita-b"
              fontSize="16px"
              onClick={this.props.onClickPageDown}
            />
            <ArqIconButton
              icon="icon-icon-seta-dupla-direita-b"
              fontSize="16px"
              onClick={this.props.onClickLastPage}
            />
          </div>
        </div>
      </div>
    );
  }
}
