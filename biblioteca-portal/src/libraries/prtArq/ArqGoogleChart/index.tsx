/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import Chart from "react-google-charts";

export interface IArqFaqProps {
  graphicType: string;
  chartData: any;
  chartTitle: string;
  joinData: any;
  textSizeX: any;
  textSizeY: any;
  textLegendSize: any;
}

export class ArqGoogleChart extends React.Component<IArqFaqProps, {}> {
  public render(): JSX.Element {
    return (
      <Chart
        width={"100%"}
        height={"500px"}
        chartType={
          this.props.graphicType === "Coluna"
            ? "ColumnChart"
            : this.props.graphicType === "Barras"
            ? "BarChart"
            : this.props.graphicType === "Dispersão"
            ? "ScatterChart"
            : this.props.graphicType === "Linhas"
            ? "LineChart"
            : "PieChart"
        }
        data={this.props.chartData}
        options={{
          title: this.props.chartTitle,
          isStacked: this.props.joinData,
          hAxis: {
            textStyle: {
              fontSize: this.props.textSizeX ? this.props.textSizeX : 13, // or the number you want
            },
          },
          vAxis: {
            textStyle: {
              fontSize: this.props.textSizeY ? this.props.textSizeY : 13, // or the number you want
            },
          },
          legend: {
            textStyle: {
              fontSize: this.props.textLegendSize
                ? this.props.textLegendSize
                : 13,
            },
          },
        }}
      />
    );
  }
}
