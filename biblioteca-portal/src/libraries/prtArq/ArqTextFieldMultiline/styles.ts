import styled from 'styled-components';

export const StyledMultiline = styled.textarea<{ emptyField: boolean }>`
  border: 1px solid ${props => props.emptyField ? 'red' : '#b8b8b8'};
  padding: 10px;
  width: 100%;
  border-radius: 4px;
  font-family: Bradesco Regular Sans;
  :focus-visible {
    outline: none;
  }
`;
export const StyledText = styled.div`
  font-family: Bradesco Regular Sans;
  font-weight: bold;
  color: #4f4f4f;
  margin-bottom: 10px;
`;
