/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import * as S from "./styles";

export interface IArqTextFieldMultilineProps {
  label?: string;
  rows?: number;
  placeholder?: string;
  value?: string;
  emptyField?: boolean;
  onChange?: (text: string) => void;
}

export class ArqTextFieldMultiline extends React.Component<
  IArqTextFieldMultilineProps,
  {}
> {
  public render(): JSX.Element {
    const onChangeMultiline = (event: any): void => {
      if (this.props.onChange) {
        this.props.onChange(event.target.value);
        console.log("");
      }
    };
    return (
      <>
        {!!this.props.label && <S.StyledText>{this.props.label}</S.StyledText>}
        <S.StyledMultiline
          rows={this.props.rows}
          placeholder={this.props.placeholder}
          value={this.props.value}
          emptyField={this.props.emptyField || false}
          onChange={onChangeMultiline}
        />
      </>
    );
  }
}
