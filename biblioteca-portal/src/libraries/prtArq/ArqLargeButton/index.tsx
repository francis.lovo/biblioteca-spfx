import React from "react";
import styled from "styled-components";

const Button = styled.button`
  font-family: "Bradesco Regular Sans";
  font-weight: bold;
  font-size: 16px;
  width: 400px;
  height: 70px;
  background-color: white;
  text-align: left;
  display: flex;
  align-items: center;
  border: 1px gray;
  border-radius: 5px;
  box-shadow: 0px 0px 2px 2px rgba(0, 0, 0, 0.1);
`;

const Text = styled.span`
  text-align: left;
  margin-left: 15px;
`;
export interface IArqLageButtonProps {
  text: string;
}

export class ArqLargeButton extends React.Component<IArqLageButtonProps, {}> {
  public render(): JSX.Element {
    return (
      <Button>
        <Text>{this.props.text}</Text>
      </Button>
    );
  }
}
