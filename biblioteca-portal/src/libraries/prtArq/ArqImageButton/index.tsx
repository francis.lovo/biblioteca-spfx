import React from "react";
import * as S from "./styles";
import { ArqChapterButton } from "../ArqChapterButton";

export interface IArqImageButtonProps {
  title?: string;
  subtitle?: string;
  link?: string;
  src?: string;
}

export class ArqImageButton extends React.Component<IArqImageButtonProps, {}> {
  public render(): JSX.Element {
    return (
      <S.StyledImageButton>
        {!!this.props.src && (
          <S.StyledImage src={this.props.src} alt="ImageButton" />
        )}
        <ArqChapterButton
          titles={this.props.title ? [this.props.title] : []}
          subtitles={this.props.subtitle ? [this.props.subtitle] : undefined}
          color="rgb(251, 249, 249)"
          links={this.props.link ? [this.props.link] : undefined}
        />
      </S.StyledImageButton>
    );
  }
}
