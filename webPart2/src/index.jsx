import React from "react";
import { Button } from "prt-spfx";

export const HelloWorld = () => {
  return (
    <div>
      <Button text="esse é o texto que eu coloquei no webpart" />
    </div>
  );
};
