import React from "react";
import { IQualquerCoisaProps } from "./IQualquerCoisaProps";
import { Notification } from "./Notification";
import { ArqMenuDropdown } from "biblioteca-portal";

export default class QualquerCoisa extends React.Component<
  IQualquerCoisaProps,
  { isDropdownOpen: boolean }
> {
  state = {
    isDropdownOpen: false,
  };

  handleDropdownClick = (): void => {
    this.setState((prevState) => ({
      isDropdownOpen: !prevState.isDropdownOpen,
    }));
  };

  render(): React.ReactElement<IQualquerCoisaProps> {
    return (
      <div>
        <div
          style={{
            width: "100% !important",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              fontSize: "18px",
              paddingLeft: "10px",
            }}
          >
            <Notification />
            {/* Botão hamburguer */}
            <div
              onClick={this.handleDropdownClick}
              style={{
                marginLeft: "10px",
                cursor: "pointer",
                userSelect: "none",
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="white"
                viewBox="0 0 16 16"
              >
                <path
                  d="M1 3h14v1H1zm0 5h14v1H1zm0 5h14v1H1z"
                  fillRule="evenodd"
                />
              </svg>
            </div>
            {/* Renderizar o componente ArqMenuDropdown */}
            {this.state.isDropdownOpen && (
              <div
                style={{
                  position: "absolute",
                  left: "60px",
                  top: "60px",
                  zIndex: "2000",
                  backgroundColor: "white",
                  width: "80%",
                }}
              >
                <ArqMenuDropdown
                  items={[
                    {
                      name: "Página Inicial",
                      dropdownItems: [],
                      link: "https://645fyx.sharepoint.com/SitePages/Links-importantes.aspx",
                    },
                    {
                      name: "Arquitetura Delivery",
                      dropdownItems: ["Chapter", "Chapter Leader"],
                    },
                    {
                      name: "Arquitetura Corporativa",
                      dropdownItems: ["Chapter", "Chapter Leader"],
                    },
                    {
                      name: "Arquitetura da Informação",
                      dropdownItems: ["Chapter", "Chapter Leader"],
                    },
                    {
                      name: "Comunicados",
                      dropdownItems: [],
                    },
                    {
                      name: "Trilhas do Conhecimento",
                      dropdownItems: [],
                    },
                    {
                      name: "Social",
                      dropdownItems: [],
                    },
                    {
                      name: "Eventos",
                      dropdownItems: [],
                    },
                    {
                      name: "Sou Novo Por Aqui",
                      dropdownItems: [],
                    },
                  ]}
                  title="Menu"
                />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
