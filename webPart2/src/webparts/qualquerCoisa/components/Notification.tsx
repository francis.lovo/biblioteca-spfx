/* eslint-disable require-atomic-updates */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState, useEffect } from "react";
import { ArqModal, ArqNotificationCard, ArqText } from "biblioteca-portal";

export interface NotificationProps {
  onClick?: () => void;
}

interface ListItem {
  Id: number;
  Title: string;
  EditorId: number;
  AuthorName?: string;
  Modified: string;
  isNew: boolean;
  Description?: string;
}

export const Notification = () => {
  const [openModal, setOpenModal] = useState(false);
  const [notifications, setNotifications] = useState<ListItem[]>([]);
  const [hasNewModificationCircle, setHasNewModificationCircle] =
    useState(false);
  const [lastModificationTimestamp, setLastModificationTimestamp] = useState<
    number | null
  >(null);

  async function getNotification() {
    const webUrl =
      "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/Web/Lists(guid'4540801e-3ce8-4828-be22-b96596c766a3')/Items";

    try {
      const response = await fetch(webUrl, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        return data.d.results;
      } else {
        console.error("Falha na requisição");
        return [];
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
      return [];
    }
  }

  const getUserInfoById = async (editorId: number) => {
    const apiUrl = `https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/getuserbyid(${editorId})`;

    try {
      const response = await fetch(apiUrl, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        return data.d.Title;
      } else {
        console.error("Falha na requisição");
        return null;
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
      return null;
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await getNotification();
      const sortedData = data.sort((a: ListItem, b: ListItem) =>
        b.Modified.localeCompare(a.Modified)
      );

      const latestModificationTimestamp = new Date(
        sortedData[0]?.Modified
      ).getTime();

      if (latestModificationTimestamp !== lastModificationTimestamp) {
        setLastModificationTimestamp(latestModificationTimestamp);
        const lastFiveModifications = sortedData.slice(0, 5);
        const modifiedItemsWithAuthors = await Promise.all(
          lastFiveModifications.map(async (modified: ListItem) => {
            const authorName = await getUserInfoById(modified.EditorId);
            modified.AuthorName = authorName;
            modified.isNew = true;
            return modified;
          })
        );

        const removedNotifications = JSON.parse(
          localStorage.getItem("removedNotifications") || "[]"
        );
        console.log("Removed: ", removedNotifications);

        const filteredItems: any[] = [];
        const updatedRemovedNotifications: any[] = [];

        modifiedItemsWithAuthors.map((item) => {
          let isRemoved = false;
          let isModified = false;
          removedNotifications.map((removed: any) => {
            if (removed.Id === item.Id) {
              isRemoved = true;
              if (removed.Modified !== item.Modified) {
                isModified = true;
              }
            }
          });
          if (!isRemoved) {
            filteredItems.push(item);
          } else if (isRemoved && isModified) {
            filteredItems.push(item);
          } else {
            updatedRemovedNotifications.push(item);
          }
        });
        console.log(
          "updatedRemovedNotifications:",
          updatedRemovedNotifications
        );

        console.log("filteredItems:", filteredItems);

        setNotifications(filteredItems);
        setHasNewModificationCircle(true);

        localStorage.setItem(
          "removedNotifications",
          JSON.stringify(updatedRemovedNotifications)
        );
      } else {
        setHasNewModificationCircle(false);
      }
    };

    const interval = setInterval(fetchData, 5000);

    return () => {
      clearInterval(interval);
    };
  }, [lastModificationTimestamp]);

  const handleNotificationClick = (): void => {
    setOpenModal(true);
    setHasNewModificationCircle(false);
  };

  const handleCloseModal = (): void => {
    setOpenModal(false);
  };

  const handleDeleteItem = (itemId: number) => {
    const removedNotifications: any[] = JSON.parse(
      localStorage.getItem("removedNotifications") || "[]"
    );

    const updatedItems = notifications.filter((item) => item.Id !== itemId);
    notifications.map((item) => {
      if (itemId === item.Id) {
        removedNotifications.push(item);
      }
    });

    localStorage.setItem(
      "removedNotifications",
      JSON.stringify(removedNotifications)
    );

    setNotifications(updatedItems);
  };

  return (
    <div
      style={{ fontSize: "14px", paddingRight: "10px", position: "relative" }}
    >
      {hasNewModificationCircle && (
        <div
          style={{
            position: "absolute",
            top: "-5px",
            left: "-5px",
            width: "10px",
            height: "10px",
            backgroundColor: "red",
            borderRadius: "50%",
          }}
        />
      )}
      <a
        href="#"
        style={{ textDecoration: "none", color: "inherit" }}
        onClick={handleNotificationClick}
      >
        <div style={{ display: "inline-block" }}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            fill="white"
            viewBox="0 0 16 16"
          >
            <path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zM8 1.918l-.797.161A4.002 4.002 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4.002 4.002 0 0 0-3.203-3.92L8 1.917zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5.002 5.002 0 0 1 13 6c0 .88.32 4.2 1.22 6z" />
          </svg>
        </div>
      </a>

      {openModal && (
        <ArqModal
          isOpen={openModal}
          onClose={handleCloseModal}
          modalWidth="60%"
          padding={30}
        >
          <div
            style={{
              width: "100%",
            }}
          >
            <div style={{ marginBottom: 20 }}>
              <ArqText text="Notificações" color="#484848" fontSize="20px" />
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                gap: 20,
                maxHeight: "400px",
                overflowY: "auto",
                width: "100%",
              }}
            >
              {notifications.map((item, index) => (
                <div key={index}>
                  <ArqNotificationCard
                    title={item.Title}
                    text={"Autor: " + item.AuthorName || ""}
                    //text={item.Description || ""}
                    onClick={() => handleDeleteItem(item.Id)}
                  />
                </div>
              ))}
            </div>
          </div>
        </ArqModal>
      )}
    </div>
  );
};
