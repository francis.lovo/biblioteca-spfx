import * as React from "react";
import { ArqText, ArqIconButton } from "biblioteca-portal";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function SubMenuBar() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        paddingBottom: 10,
        borderBottom: "2px solid #484848",
      }}
    >
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Visão Geral"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Gestão"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Wiki e Ferramenta"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Chapter Sync"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Vertical"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="POCLAC"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Visão Holística"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
      </div>
    </div>
  );
}
