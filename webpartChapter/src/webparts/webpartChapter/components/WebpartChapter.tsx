import * as React from "react";
import { IWebpartChapterProps } from "./IWebpartChapterProps";
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubMenuBar";

export default class WebpartChapter extends React.Component<
  IWebpartChapterProps,
  {}
> {
  public render(): React.ReactElement<IWebpartChapterProps> {
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb
            items={["Arquitetura da Informação", "Chapter"]}
            links={["/", "/SitePages/Arquitetura-da-informacao.aspx"]}
          />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Chapter"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
        <SubMenuBar />
      </div>
    );
  }
}
