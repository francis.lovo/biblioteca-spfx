/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { IWebpartFormTreinamentoProps } from "./IWebpartFormTreinamentoProps";
import { FormPessoal } from "./FormPessoal";
import { FormCurso } from "./FormCurso";
import { FormOutros } from "./FormOutros";
import { ArqButton, ArqSuccessModal } from "biblioteca-portal";
import { SPHttpClient } from "@microsoft/sp-http";
import jsPDF from "jspdf";

export default class WebpartFormTreinamento extends React.Component<
  IWebpartFormTreinamentoProps,
  {}
> {
  state = {
    files: [],
    nome: undefined,
    tipoDoc: undefined,
    categoria: undefined,
    areaResponsavel: undefined,
    nomeProjeto: undefined,
    ppmc: undefined,
    vertical: undefined,
    descricao: undefined,
    dataCriacao: undefined,
    ultimaAtualizacao: undefined,
    responsavelAtualizacao: undefined,
    versao: undefined,
    observacao: undefined,
    openModal: false,
    error: false,
  };
  componentDidUpdate(): void {
    console.log("NOME: ", this.state.nome);
    console.log("tipoDoc: ", this.state.tipoDoc);
  }
  public render(): React.ReactElement<IWebpartFormTreinamentoProps> {

    const changeNome = (nome: string): void => {
      const MAX_CHARACTER_LENGTH = 56;

      if (nome.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ nome: nome });
      } else {
        this.setState({ nome: nome.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeTipoDoc = (tipoDoc: string): void => {
      const MAX_CHARACTER_LENGTH = 54;

      if (tipoDoc.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ tipoDoc: tipoDoc });
      } else {
        this.setState({ tipoDoc: tipoDoc.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeCategoria = (value: string | number): void => {
      this.setState({ categoria: value });
    };
    const changePPMC = (ppmc: string): void => {
      const MAX_CHARACTER_LENGTH = 10;

      const numericValue = ppmc.replace(/\D/g, "");

      if (numericValue.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ ppmc: numericValue });
      } else {
        this.setState({ ppmc: numericValue.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeAreaResponsavel = (value: string | number): void => {
      this.setState({ areaResponsavel: value });
    };
    const changeNomeProjeto = (nomeProjeto: string): void => {
      const MAX_CHARACTER_LENGTH = 54;

      if (nomeProjeto.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ nomeProjeto: nomeProjeto });
      } else {
        this.setState({ nomeProjeto: nomeProjeto.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeDescricao = (descricao: string): void => {
      const MAX_DESCRICAO_LENGTH = 180;
      if (descricao.length <= MAX_DESCRICAO_LENGTH) {
        this.setState({ descricao: descricao });
      } else {
        this.setState({ descricao: descricao.slice(0, MAX_DESCRICAO_LENGTH) });
      }
    };
    const changeData = (dataCriacao: string): void => {
      const numericValue = dataCriacao.replace(/\D/g, "");

      if (numericValue.length > 0) {
        let dataFormatada = numericValue.slice(0, 2);

        if (numericValue.length > 2) {
          dataFormatada += "/" + numericValue.slice(2, 4);

          if (numericValue.length > 4) {
            dataFormatada += "/" + numericValue.slice(4, 8);
          }
        }

        this.setState({ dataCriacao: dataFormatada });
      } else {
        this.setState({ dataCriacao: numericValue });
      }
    };
    const changeUltimaAtualizacao = (ultimaAtualizacao: string): void => {
      const numericValue = ultimaAtualizacao.replace(/\D/g, "");

      if (numericValue.length > 0) {
        let dataFormatada = numericValue.slice(0, 2);

        if (numericValue.length > 2) {
          dataFormatada += "/" + numericValue.slice(2, 4);

          if (numericValue.length > 4) {
            dataFormatada += "/" + numericValue.slice(4, 8);
          }
        }

        this.setState({ ultimaAtualizacao: dataFormatada });
      } else {
        this.setState({ ultimaAtualizacao: numericValue });
      }
    };
    const changeResponsavelAtualizacao = (
      responsavelAtualizacao: string
    ): void => {
      const MAX_CHARACTER_LENGTH = 60;

      if (responsavelAtualizacao.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ responsavelAtualizacao: responsavelAtualizacao });
      } else {
        this.setState({ responsavelAtualizacao: responsavelAtualizacao.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeVersao = (versao: string): void => {
      const MAX_CHARACTER_LENGTH = 66;

      if (versao.length <= MAX_CHARACTER_LENGTH) {
        this.setState({ versao: versao });
      } else {
        this.setState({ versao: versao.slice(0, MAX_CHARACTER_LENGTH)});
      }
    };
    const changeVertical = (value: string | number): void => {
      this.setState({ vertical: value });
    };
    const changeFiles = (files: any): void => {
      this.setState({ files: files });
    };
    const changeObservacao = (observacao: string): void => {
      this.setState({ observacao: observacao });
    };

    const enviarInfo = () => {
      if (!validateFields()) {
        alert("É necessário o preenchimento dos campos.");
        return;
      }
      const urlListBS =
        "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/lists/getByTitle('Repositorio')/items";

      this.props.spHttpClient
        .post(`${urlListBS}`, SPHttpClient.configurations.v1, {
          headers: {
            Accept: "application/json;odata=verbose",
            "Content-Type": "application/json;odata=verbose",
            "odata-version": "",
          },
          body: JSON.stringify({
            __metadata: {
              type: "SP.Data.TreinamentosListItem",
            },
            Nome: this.state.nome,
            E_x002d_mail: this.state.tipoDoc,
            Chave: this.state.categoria,
            OData__x00c1_rea: this.state.areaResponsavel,
            TipoTreinamento: this.state.vertical,
            QuantidadedeHoras: this.state.ppmc,
            Trilha: this.state.nomeProjeto,
            Outros: this.state.descricao,
            DataInicio: this.state.dataCriacao,
            DataFim: this.state.ultimaAtualizacao,
            NomedoCurso: this.state.responsavelAtualizacao,
            Status: this.state.versao,
            OutrasInforma_x00e7__x00f5_es: this.state.observacao,
          }),
        })
        .then((listResponse) => {
          if (listResponse.ok) {
            return listResponse.json().then((responseData) => {
              const itemId = responseData.d.Id;
              const documents = this.state.files as File[];

              for (const file of documents) {
                const formData = new FormData();
                formData.append("file", file, file.name);

                this.props.spHttpClient
                  .post(
                    `${urlListBS}(${itemId})/AttachmentFiles/add(FileName='${file.name}')`,
                    SPHttpClient.configurations.v1,
                    {
                      body: formData,
                    }
                  )
                  .then((addAttachmentResponse) => {
                    if (addAttachmentResponse.ok) {
                      const attachmentBaseUrl =
                        "https://bancobradesco.sharepoint.com/teams/5800-pa/Lists/Treinamentos/Attachments";
                      const attachmentRelativeUrl = `${itemId}/${encodeURIComponent(
                        file.name
                      )}`;
                      const attachmentFullUrl = `${attachmentBaseUrl}/${attachmentRelativeUrl}`;

                      const updateItemUrl = `${urlListBS}(${itemId})`;
                      const updateItemPayload = {
                        __metadata: {
                          type: "SP.Data.TreinamentosListItem",
                        },
                        Certificado: {
                          Description: file.name,
                          Url: attachmentFullUrl,
                        },
                      };

                      const updateConfig = {
                        method: "PATCH",
                        headers: {
                          Accept: "application/json;odata=verbose",
                          "Content-Type": "application/json;odata=verbose",
                          "X-HTTP-Method": "MERGE",
                          "If-Match": "*",
                          "odata-version": "",
                        },
                        body: JSON.stringify(updateItemPayload),
                      };

                      this.props.spHttpClient
                        .post(
                          updateItemUrl,
                          SPHttpClient.configurations.v1,
                          updateConfig
                        )
                        .then((updateResponse) => {
                          if (!updateResponse.ok) {
                            throw new Error(
                              "Erro ao salvar URL do anexo na coluna Certificado"
                            );
                          }
                          return updateResponse.json();
                        });
                    } else {
                      alert(
                        "Erro ao salvar URL do anexo na coluna Certificado."
                      );
                    }
                  });
              }
              this.setState({ openModal: true });

              generatePDF();
            });
          } else {
            alert("Erro ao solicitar a requisição.");
          }
        });
    };

    const handleCloseModal = (): void => {
      this.setState({ openModal: false });
    };

    const validateFields = () => {
      const requiredFields: (keyof (typeof WebpartFormTreinamento)["prototype"]["state"])[] =
        [
          "nome",
          "tipoDoc",
          "categoria",
          "areaResponsavel",
          "descricao",
          "versao",
          "vertical",
          "dataCriacao",
          "responsavelAtualizacao",
        ];

      const hasEmptyField = requiredFields.every((field) => {
        const value = this.state[field];
        return (
          value !== undefined && value !== null && String(value).trim() !== ""
        );
      });

      this.setState({ error: !hasEmptyField });

      return hasEmptyField;
    };

    const generatePDF = () => {
      const {
        nome,
        tipoDoc,
        categoria,
        areaResponsavel,
        ppmc,
        vertical,
        descricao,
        nomeProjeto,
        dataCriacao,
        ultimaAtualizacao,
        responsavelAtualizacao,
        versao,
        observacao,
      } = this.state;

      const doc = new jsPDF();

      const logoUrl =
        "https://bancobradesco.sharepoint.com/:i:/r/teams/5800-pa/SiteAssets/bs.png?csf=1&web=1&e=rFaYXG";

      const x = 10;
      const y = 10;
      const width = 130;
      const height = 20;

      doc.addImage(logoUrl, "PNG", x, y, width, height);
      doc.addFont("Bradesco Sans.woff", "Bradesco Sans", "normal", "UTF-8");
      doc.setFont("Bradesco Sans");
      doc.setFontSize(18);

      doc.setTextColor("#cc092f");
      doc.text("Repositório da Arquitetura", 10, 40);

      doc.setTextColor(0);

      const addFieldToPDF = (
        label: string,
        value: string | undefined,
        yPos: number
      ) => {
        if (value !== undefined) {
          doc.text(`${label}: ${value}`, 10, yPos);
          return true;
        }
        return false;
      };

      let yPos = 60;
      const verticalSpacing = 10;
      const maxCharsPerLine = 65;
      let descricaoFormatada = (descricao || "").replace(new RegExp(`(.{1,${maxCharsPerLine}}|\\d+)`, 'g'), '$1\n');

      console.log("descricaoFormatada: ", descricaoFormatada)

      if (addFieldToPDF("Tipo Documento", tipoDoc, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Categoria", categoria, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Nome Arquivo", nome, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Área Responsável", areaResponsavel, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Número PPMC", ppmc, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Nome do Projeto", nomeProjeto, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Descrição", descricaoFormatada, yPos)) {     
        const linesInDescricao = (descricaoFormatada || "").split(/\n/).length;
        console.log("linesInDescricao: ", linesInDescricao);
      
        if(linesInDescricao == 2){
          yPos += verticalSpacing * 1
        } else if(linesInDescricao == 3){
          yPos += verticalSpacing * 1.8
        } else if(linesInDescricao == 4){
          yPos += verticalSpacing * 2.5
        } else {
          yPos += verticalSpacing
        }
      }
      if (addFieldToPDF("Versão", versao, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Vertical", vertical, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Data Criação", dataCriacao, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Última Atualização", ultimaAtualizacao, yPos)) {
        yPos += verticalSpacing;
      }
      if (addFieldToPDF("Responsável", responsavelAtualizacao, yPos)) {
        yPos += verticalSpacing;
      }

      if (addFieldToPDF("Observação", observacao, yPos)) {
        yPos += verticalSpacing;
      }

      doc.save("repositorioArquitetura.pdf");
    };

    return (
      <div>
        {this.state.openModal && (
          <ArqSuccessModal
            isOpen={this.state.openModal}
            onClick={handleCloseModal}
          />
        )}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            width: "100%",
            justifyContent: "space-between",
          }}
        >
          <div
            style={{
              width: "29%",
            }}
          >
            <FormPessoal
              nomeState={this.state.nome}
              tipoDocState={this.state.tipoDoc}
              categoriaState={this.state.categoria}
              changeNome={changeNome}
              changeTipoDoc={changeTipoDoc}
              changeCategoria={changeCategoria}
              error={this.state.error}
            />
          </div>
          <div
            style={{
              width: "40%",
            }}
          >
            <FormCurso
              nPPMC={this.state.ppmc}
              changePPMC={changePPMC}
              areaResponsavel={this.state.areaResponsavel}
              changeAreaResponsavel={changeAreaResponsavel}
              changeNomeProjeto={changeNomeProjeto}
              nomeProjeto={this.state.nomeProjeto}
              changeDescricao={changeDescricao}
              descricaoState={this.state.descricao}
              changeUltimaAtualizacao={changeUltimaAtualizacao}
              ultimaAtualizacaoState={this.state.ultimaAtualizacao}
              changeData={changeData}
              dataState={this.state.dataCriacao}
              changeResponsavelAtualizacao={changeResponsavelAtualizacao}
              responsavelAtualizacaoState={this.state.responsavelAtualizacao}
              changeVersao={changeVersao}
              versao={this.state.versao}
              changeVertical={changeVertical}
              vertical={this.state.vertical}
              error={this.state.error}
            />
          </div>
          <div
            style={{
              width: "29%",
            }}
          >
            <FormOutros
              changeFiles={changeFiles}
              changeObservacao={changeObservacao}
              observacao={this.state.observacao}
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 20,
            width: "100%",
          }}
        >
          <div style={{ width: "max-content", marginRight: 5 }}>
            <ArqButton
              color="#0E81ED"
              hoverColor="#0E81ED"
              text="Enviar Informações"
              onClick={enviarInfo}
              styleType="filed"
            />
          </div>
        </div>
      </div>
    );
  }
}
