import { ArqInformationCard, ArqSelectInput, ArqText, ArqTextField } from "biblioteca-portal";
import React, {useState} from "react";

interface IFormPessoal {
  nomeState: string | undefined;
  changeNome: (nome: string) => void;
  tipoDocState: string | undefined;
  changeTipoDoc: (email: string) => void;
  categoriaState: string | undefined;
  changeCategoria: (chave: string) => void;
  error: boolean;
}

interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const FormPessoal = ({
  nomeState,
  changeNome,
  tipoDocState,
  changeTipoDoc,
  changeCategoria,
  error,
}: IFormPessoal) => {
  const [categoria, setCategoria] = useState("");

  const onChangeNome = (text: string): void => {
    changeNome(text);
  };
  const onChangeTipoDoc = (text: string): void => {
    changeTipoDoc(text);
  };
  const onChangeCategoria = (value: ISelect): void => {
    changeCategoria(value.label);
    setCategoria(value.label);
  };
  return (
    <ArqInformationCard>
      <ArqText
        text="Informações Documento"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div style={{ marginBottom: 20, marginTop: 20 }}>
        <ArqTextField
          label="Tipo Documento"
          placeholder="Tipo Documento"
          onChange={onChangeTipoDoc}
          value={tipoDocState}
          emptyField={
            error && (tipoDocState === undefined || tipoDocState === "")
          }
        />
      </div>
      <div style={{ marginBottom: 20 }}>
      <ArqSelectInput
            label="Categoria"
            options={[
              { value: 1, label: "INTERNO" },
              { value: 2, label: "EXTERNO" },
            ]}
            onChange={onChangeCategoria}
            emptyField={categoria === "" && error}
        />
      </div>
      <div style={{ marginBottom: 20 }}>
        <ArqTextField
          label="Nome Arquivo"
          placeholder="Nome Arquivo"
          value={nomeState}
          onChange={onChangeNome}
          emptyField={error && (nomeState === undefined || nomeState === "")}
        />
      </div>
    </ArqInformationCard>
  );
};
