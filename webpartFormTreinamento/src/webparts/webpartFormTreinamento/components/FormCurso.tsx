import { ArqInformationCard, ArqText, ArqTextField, ArqSelectInput } from "biblioteca-portal";
import React, { useEffect, useState } from "react";

interface IFormCurso {
  nPPMC: string | undefined;
  changePPMC: (nPPMC: string) => void;
  areaResponsavel: string | undefined;
  changeAreaResponsavel: (value: string) => void;
  nomeProjeto: string | undefined;
  changeNomeProjeto: (nomeProjeto: string) => void;
  descricaoState: string | undefined;
  changeDescricao: (descricao: string) => void;
  dataState: string | undefined;
  changeData: (data: string) => void;
  ultimaAtualizacaoState: string | undefined;
  changeUltimaAtualizacao: (ultimaAtualizacao: string) => void;
  responsavelAtualizacaoState: string | undefined;
  changeResponsavelAtualizacao: (responsavel: string) => void;
  versao: string | undefined;
  changeVersao: (versao: string) => void;
  vertical: string | undefined;
  changeVertical: (value: string) => void;
  error: boolean;
}

interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

type Option = {
  value: number;
  label: string;
};

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const FormCurso = ({
  nPPMC,
  changePPMC,
  changeAreaResponsavel,
  nomeProjeto,
  changeNomeProjeto,
  descricaoState,
  changeDescricao,
  dataState,
  changeData,
  ultimaAtualizacaoState,
  changeUltimaAtualizacao,
  responsavelAtualizacaoState,
  changeResponsavelAtualizacao,
  changeVersao,
  versao,
  changeVertical,
  error,
}: IFormCurso) => {
  const [areaResponsavel, setAreaResponsavel] = useState("");
  const [vertical, setVertical] = useState("");
  const [optionsArea, setOptionsArea] = useState<Option[]>([]);
  const [optionsVertical, setOptionsVertical] = useState<Option[]>([]);

  const onChangePPMC = (text: string): void => {
    changePPMC(text);
  };
  const onchangeAreaResponsavel = (value: ISelect): void => {
    changeAreaResponsavel(value.label);
    setAreaResponsavel(value.label);
  };

  const onChangeNomeProjeto = (text: string): void => {
    changeNomeProjeto(text);
  };
  const onChangeDesc = (text: string): void => {
    changeDescricao(text);
  };
  const onChangeData = (text: string): void => {
    changeData(text);
  };
  const onChangeUltimaAtualizacao = (text: string): void => {
    changeUltimaAtualizacao(text);
  };
  const onChangeResponsavelAtualizacao = (text: string): void => {
    changeResponsavelAtualizacao(text);
  };
  const onchangeVersao = (text: string): void => {
    changeVersao(text);
  };
  const onchangeVertical = (value: ISelect): void => {
    changeVertical(value.label);
    setVertical(value.label);
  };

  const optionsAreaResponsavel = () => {
    fetch("https://bancobradesco.sharepoint.com/teams/5800-pa/Documentos%20Compartilhados/areaResponsavel.txt")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Falha ao carregar o arquivo.");
        }
        return response.text();
      })
      .then((text) => {
        const lines = text.split("\n");
        const parsedOptions = lines.map((line, index) => ({
          value: index + 1,
          label: line.trim(),
        }));
        setOptionsArea(parsedOptions);
      })
      .catch((error) => {
        console.error("Erro ao carregar as opções:", error);
      });
  };
  
  const optionVertical = () => {
    fetch("https://bancobradesco.sharepoint.com/teams/5800-pa/Documentos%20Compartilhados/vertical.txt")
      .then((response) => {
        if (!response.ok) {
          throw new Error("Falha ao carregar o arquivo.");
        }
        return response.text();
      })
      .then((text) => {
        const lines = text.split("\n");
        const parsedOptions = lines.map((line, index) => ({
          value: index + 1,
          label: line.trim(),
        }));
        setOptionsVertical(parsedOptions);
      })
      .catch((error) => {
        console.error("Erro ao carregar as opções:", error);
      });
  };

  useEffect(() => {
    optionsAreaResponsavel();
    optionVertical()
  }, []);

  return (
    <ArqInformationCard>
      <ArqText
        text="Informações Projeto"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginTop: 20,
          marginBottom: 20,
        }}
      >
       <div style={{ width: "49%" }}>
          <ArqSelectInput
            label="Área Responsável"
            options={optionsArea}
            onChange={onchangeAreaResponsavel}
            emptyField={areaResponsavel === "" && error}
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Número PPMC"
            placeholder="Número PPMC"
            value={nPPMC}
            onChange={onChangePPMC}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Nome do Projeto"
            placeholder="Nome do Projeto"
            value={nomeProjeto}
            onChange={onChangeNomeProjeto}
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Descrição"
            placeholder="Descrição"
            value={descricaoState}
            onChange={onChangeDesc}
            emptyField={error && (descricaoState === undefined || descricaoState === "")}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Versão"
            placeholder="Versão"
            value={versao}
            onChange={onchangeVersao}
            emptyField={error && (versao === undefined || versao === "")}
          />
        </div>
        <div style={{ width: "49%" }}>
        <ArqSelectInput
            label="Vertical"
            options={optionsVertical}
            onChange={onchangeVertical}
            emptyField={vertical === "" && error}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Data Criação"
            placeholder="XX/XX/XXXX"
            value={dataState}
            onChange={onChangeData}
            emptyField={error && (dataState === undefined || dataState === "")}
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Última Atualização"
            placeholder="XX/XX/XXXX"
            value={ultimaAtualizacaoState}
            onChange={onChangeUltimaAtualizacao}
          />
        </div>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginBottom: 20,
        }}
      >
        <div style={{ width: "52%" }}>
          <ArqTextField
            label="Responsável"
            placeholder="Responsável pela última atualização"
            value={responsavelAtualizacaoState}
            onChange={onChangeResponsavelAtualizacao}
            emptyField={
              error &&
              (responsavelAtualizacaoState === undefined ||
                responsavelAtualizacaoState === "")
            }
          />
        </div>
      </div>
    </ArqInformationCard>
  );
};
