/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  ArqInformationCard,
  ArqText,
  ArqTextFieldMultiline,
  ArqUploadFolder,
} from "biblioteca-portal";
import * as React from "react";

interface IFormOutros {
  changeFiles: (files: any) => void;
  observacao: string | undefined;
  changeObservacao: (observacao: string) => void;
}
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const FormOutros = ({
  changeFiles,
  observacao,
  changeObservacao,
}: IFormOutros) => {
  const [files, setFiles] = React.useState<any>();

  const onchangeObservacao = (text: string): void => {
    changeObservacao(text);
  };

  React.useEffect(() => {
    if (files && files.length > 0) {
      console.log("FILES: ", files);

      changeFiles(files);
    }
  }, [files]);
  return (
    <ArqInformationCard>
      <ArqText
        text="Adicionais"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div style={{ width: "90%", marginTop: 20, marginBottom: 20 }}>
        <ArqTextFieldMultiline
          label="Observação"
          value={observacao}
          onChange={onchangeObservacao}
        />
      </div>
      <div style={{ width: "49%" }}>
        <ArqUploadFolder
          files={files}
          setFiles={setFiles}
          plusIcon={true}
          color={"#cacaca"}
          hoverColor="#cacaca"
          label="Anexo"
        />
      </div>
    </ArqInformationCard>
  );
};
