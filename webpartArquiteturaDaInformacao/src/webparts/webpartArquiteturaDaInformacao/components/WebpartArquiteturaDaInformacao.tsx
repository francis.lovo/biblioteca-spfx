import * as React from 'react';
import { IWebpartArquiteturaDaInformacaoProps } from './IWebpartArquiteturaDaInformacaoProps';
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubMenuBar";

export default class WebpartArquiteturaDaInformacao extends React.Component<IWebpartArquiteturaDaInformacaoProps, {}> {
  public render(): React.ReactElement<IWebpartArquiteturaDaInformacaoProps> {
    

    return (
     <div>
      <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb items={["Arquitetura da Informação"]} links={["/"]} />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Arquitetura da Informação"
            fontSize="20px"
            color="#484848"
            fontWeight="bold"
          />
        </div>
        <div>
        <SubMenuBar />
        </div>
     </div>
    );
  }
}
