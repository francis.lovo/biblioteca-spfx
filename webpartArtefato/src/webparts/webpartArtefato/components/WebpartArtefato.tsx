import * as React from "react";
import { IWebpartArtefatoProps } from "./IWebpartArtefatoProps";
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubMenuBar";

export default class WebpartArtefato extends React.Component<
  IWebpartArtefatoProps,
  {}
> {
  public render(): React.ReactElement<IWebpartArtefatoProps> {
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb
            items={["Arquitetura da Informação", "Artefato", "Artefato XXX"]}
            links={[
              "/",
              "/SitePages/Arquitetura-da-informacao.aspx",
              "/SitePages/Processos-e-Indicadores.aspx",
            ]}
          />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Artefato"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
        <SubMenuBar />
      </div>
    );
  }
}
