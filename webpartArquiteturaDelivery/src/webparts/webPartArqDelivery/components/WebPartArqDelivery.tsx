import * as React from "react";
import { IWebPartArqDeliveryProps } from "./IWebPartArqDeliveryProps";
import { ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubMenuBar";

export default class WebPartArqDelivery extends React.Component<
  IWebPartArqDeliveryProps,
  {}
> {
  public render(): React.ReactElement<IWebPartArqDeliveryProps> {
    return (
      <div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Arquitetura Delivery"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
        <SubMenuBar />
      </div>
    );
  }
}
