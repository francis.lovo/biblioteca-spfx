import * as React from "react";
import { IWebpartEventosProps } from "./IWebpartEventosProps";
import { ArqText } from "biblioteca-portal";

export default class WebpartEventos extends React.Component<
  IWebpartEventosProps,
  {}
> {
  public render(): React.ReactElement<IWebpartEventosProps> {
    return (
      <div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
          }}
        >
          <ArqText
            text="Eventos"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
      </div>
    );
  }
}
