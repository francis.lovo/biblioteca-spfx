import * as React from 'react';
import { IWebpartSouNovoProps } from './IWebpartSouNovoProps';
import {
  ArqBreadcrumb,
  ArqText,
} from "biblioteca-portal";

export default class WebpartSouNovo extends React.Component<IWebpartSouNovoProps, {}> {
  public render(): React.ReactElement<IWebpartSouNovoProps> {
    

    return (
      <div>
        
      <div
      style={{
        display: "flex",
        flexDirection: "column",          
      }}>
        <ArqBreadcrumb
        items={["Home","Sou Novo Aqui"]}
        links={["#","#"]}/>

        <div style={{
        marginTop:"15px",          
      }}>
        <ArqText
        fontSize="20px"
        color="#000000"
        fontWeight="bold"
        text="Sou Novo Aqui"/>
        </div>
        <div 
        style={{
        width: "100% !important",
        }}>     
   
        <div
        style={{
          background: "#CC092f",
          padding: "10px",
          textAlign: "center",
          color: "white",
          display: "flex",
          flexDirection: "row",
          justifyContent: "flex-start",
          alignItems: "center",
          height:"40px",
          marginTop:"15px",
          }}>
            <ArqText
              fontSize="24px"
              color="#ffffff"
              fontWeight="bold"
              text="Informações Iniciais"/>
          </div>         
          </div>     
      </div>  
  </div>
    );
  }
}
