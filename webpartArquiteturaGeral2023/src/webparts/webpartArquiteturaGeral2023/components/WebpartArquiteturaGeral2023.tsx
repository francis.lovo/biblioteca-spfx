import * as React from 'react';
import { IWebpartArquiteturaGeral2023Props } from './IWebpartArquiteturaGeral2023Props';
import { SubMenuBar } from "./SubMenuBar";
import { Tabela } from "./Tabela";
import { ArqSelectInput,} from "biblioteca-portal";
import { 
  ArqBreadcrumb, 
  ArqText, 
  ArqInformationCard, 
  ArqProgressBar, 
  ArqGrayPercent,
  ArqTextField,
  ArqTable,
} from "biblioteca-portal";


export default class WebpartArquiteturaGeral2023 extends React.Component<IWebpartArquiteturaGeral2023Props, {}> {
  public render(): React.ReactElement<IWebpartArquiteturaGeral2023Props> {
    
   const topLabels = ["CDC", "Descrição CDC", "GT","Orçado", "Fatura","Comprometido", "Disponível","Porcentagem Comprometido", "Porcentagem Disponível",];
   const tableItems = [
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
     ["45645654", "Arquitetura Corporativa - Arquitetura 2021", "Sávio Talamoni","R$ 0,00","R$ 0,00","R$ 0,00","R$ 0,00","0,00%","0,00%",],
   ];
   

    return (
      <div>
      
      <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb items={["Gestão","Orçamentos e Contratos","Arquitetura Geral 2023"]} links={["/"]} />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Arquitetura Geral 2023"
            fontSize="20px"
            color="#484848"
            fontWeight="bold"
          />
        </div>
        <div>
        <SubMenuBar />
        </div>

        <div style={{
        display:"flex",
        flexDirection:"row",
        width:"100%",
        justifyContent: "space-between",
        }}>
        <div style={{
        display:"flex",
        flexDirection:"column",
        width:"50%",
        justifyContent: "space-between",
        paddingRight:"15px",
        }}>
        <ArqSelectInput
        options={[
        { value: "1", label: "Opção 1" },
        { value: "2", label: "Opção 2" },
        { value: "3", label: "Opção 3", disabled: true },
        ]}
        noSelectedLabel="Ronald Davidson"
        label="GT"
        />
        <div style={{marginTop:"20px"}}>
        <ArqText
            text="CDC"
            fontSize="12px"
            color="#484848"
            fontWeight="bold"
          />
         </div>
        <ArqTextField/>
        </div>
        <ArqInformationCard>
          <div style={{ 
            width: "850px",
            marginTop: "15px",
            marginLeft: "15px",
          }}>
          <div style={{ 
            width: "790px",
            marginTop: "15px",
            marginLeft: "15px",
          }}>
          <ArqProgressBar
          porcentGreen={25}
          porcentBlue={25}
          porcentYellow={25}
          porcentRed={25}/>
          </div>
          </div>
          <div style={{ 
            marginTop: "15px",
            marginLeft:"20px",
          }}>
          <ArqGrayPercent 
          percent={25}/>
          </div>
        </ArqInformationCard>
        
        </div>
          <div style = {{
            marginTop:"20px",
          }}>
          <ArqTable
            topLabels={topLabels}
            tableItems={tableItems}
          />
          </div>
          <div style = {{
            marginTop:"30px",
          }}>     
          <Tabela/>
          </div>
     </div>
    );
  }
}
