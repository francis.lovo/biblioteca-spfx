import React from "react";

const tableHeaderStyle = {
  backgroundColor: "#CC092f",
  color: "white",
  fontWeight: "bold",
  padding: "8px",
};

const tableData = [
  {
    orcado: "1000,00",
    fatura: "800,00",
    comprometido: "600,00",
    disponivel: "400,00",
    porcentagemComprometido: "60%",
    porcentagemDisponivel: "40%",
  },
  // Adicione mais objetos com os dados das linhas conforme necessário
];

export function Tabela() {
  return (
    <div style={{
      display:"flex",
      flexDirection:"row",
    }}>
      <div style={{
        width:"100px",
        height:"30px",
        backgroundColor:"#cc092f",
        color:"white",
        textAlign:"center",
        alignItems:"center",
        fontWeight:"bold",
        marginTop:"35px",
        display:"flex",
        justifyContent:"center",
        borderTopLeftRadius:"5px",
        borderBottomLeftRadius:"5px",
      }}>
      <span>Total Despesa</span>
      </div>
    <div>
      {/* Tabela */}
      <table style={{ borderCollapse: "collapse", width: "100%" }}>
        <thead>
          <tr>
            {/* Títulos das colunas */}
            <th style={tableHeaderStyle}>Orçado</th>
            <th style={tableHeaderStyle}>Fatura</th>
            <th style={tableHeaderStyle}>Comprometido</th>
            <th style={tableHeaderStyle}>Disponível</th>
            <th style={tableHeaderStyle}>Porcentagem Comprometido</th>
            <th style={tableHeaderStyle}>Porcentagem Disponível</th>
          </tr>
        </thead>
        <tbody>
          
          {/* Iterar sobre os dados para renderizar as linhas */}
          {tableData.map((rowData, index) => (
            <tr key={index}>
              {/* Valores para cada coluna */}
              <td style={{ textAlign: "center" }}>{rowData.orcado}</td>
              <td style={{ textAlign: "center" }}>{rowData.fatura}</td>
              <td style={{ textAlign: "center" }}>{rowData.comprometido}</td>
              <td style={{ textAlign: "center" }}>{rowData.disponivel}</td>
              <td style={{ textAlign: "center" }}>{rowData.porcentagemComprometido}</td>
              <td style={{ textAlign: "center" }}>{rowData.porcentagemDisponivel}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    </div>
  );
}
