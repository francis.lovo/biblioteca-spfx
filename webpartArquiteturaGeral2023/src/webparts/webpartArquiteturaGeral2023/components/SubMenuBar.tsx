import * as React from "react";
import { ArqText, ArqIconButton } from "biblioteca-portal";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function SubMenuBar() {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        paddingBottom: 10,
        borderBottom: "2px solid #cc092f",
        marginBottom: 30,
      }}
    >
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Visão Geral"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Gestão de funcionários"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Orçamentos e Contratos"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="MART"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Governança da Arquitetura"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Recente"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        <div style={{ marginLeft: 5 }}>
          <ArqIconButton
            icon="icon-icon-seta-baixo-b"
            color="#484848"
            fontSize="12px"
            hoverColor="#484848"
            fontWeight="bold"
          />
        </div>
      </div>
      <div
        style={{
          marginRight: 20,
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          cursor: "pointer",
        }}
      >
        <ArqText
          text="Lixeira"
          fontSize="12px"
          color="#484848"
          fontWeight={"bold"}
        />
        
      </div>     
    </div>
  );
}
