import { SPHttpClient } from "@microsoft/sp-http";

export interface IWebpartComunicadosAnaliseProps {
  description: string;
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
  spHttpClient: SPHttpClient;
  type: string;
}
