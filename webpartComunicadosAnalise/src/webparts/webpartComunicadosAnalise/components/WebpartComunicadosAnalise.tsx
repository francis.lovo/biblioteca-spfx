/* eslint-disable no-void */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import * as React from "react";
import { IWebpartComunicadosAnaliseProps } from "./IWebpartComunicadosAnaliseProps";
import {
  ArqButton,
  ArqTable,
  ArqText,
  ArqTextFieldMultiline,
  formatarData,
} from "biblioteca-portal";
import { SPHttpClient } from "@microsoft/sp-http";

interface MyState {
  itemSelected: any[];
  indexSelected: number;
  justification: string;
  tableItems: any[];
  itemsIndexes: any[];
  allListItems: any[];
  dataResponse: any;
}

export default class WebpartComunicadosAnalise extends React.Component<
  IWebpartComunicadosAnaliseProps,
  {}
> {
  state: MyState = {
    itemSelected: [],
    indexSelected: 0,
    justification: "",
    tableItems: [],
    itemsIndexes: [],
    allListItems: [],
    dataResponse: {},
  };
  mountTableItems = (data: any): void => {
    const auxTable: any[] = [];
    const indexesAux: number[] = [];
    data.forEach((item: any, index: number) => {
      const auxRow: string[] = [];
      if (
        (this.props.type === "Primeira Análise" &&
          item.Status === "Em análise") ||
        (this.props.type === "Segunda Análise" && item.Status === "Revisado")
      ) {
        auxRow.push(item.Title);
        auxRow.push(item.TipodeComunicado);
        auxRow.push(formatarData(item.DataSolicita_x00e7__x00e3_o));
        auxRow.push(item.Solicitante_x0020_Nome);
        auxRow.push(item.Status);
        auxTable.push(auxRow);
        indexesAux.push(index);
      }
    });
    console.log("DATA: ", data);

    this.setState({
      tableItems: auxTable,
      itemsIndexes: indexesAux,
      allListItems: data,
    });
  };

  editList = async (id: any, updatedData: any, justificativa: string) => {
    const webUrl = `https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/lists/GetByTitle('Comunicado')/items(${id})`;

    const updateItemPayload = {
      __metadata: {
        type: "SP.Data.ComunicadoListItem",
      },
      Status: updatedData,
      Justificativa: justificativa,
    };

    const requestOptions = {
      method: "PATCH",
      headers: {
        Accept: "application/json;odata=verbose",
        "Content-Type": "application/json;odata=verbose",
        "X-HTTP-Method": "MERGE",
        "If-Match": "*",
        "odata-version": "",
      },
      body: JSON.stringify(updateItemPayload),
    };

    try {
      const response = await this.props.spHttpClient.post(
        webUrl,
        SPHttpClient.configurations.v1,
        requestOptions
      );

      if (response.ok) {
        console.log("Item atualizado com sucesso.");
        this.setState({
          itemSelected: [],
          indexSelected: 0,
          justification: "",
        });
        await this.loadList();
      } else {
        console.error("Falha na requisição:", response);
      }
    } catch (error) {
      console.error("Erro na requisição:", error);
    }
  };

  loadList = async () => {
    const webUrl =
      "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('Comunicado')/items";
    //"https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('FAQ')/items";

    try {
      const response = await fetch(webUrl, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        this.setState({ dataResponse: data });
        this.mountTableItems(data.d.results);
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  };

  componentDidMount(): void {
    void this.loadList();
  }
  componentDidUpdate(
    prevProps: Readonly<IWebpartComunicadosAnaliseProps>,
    prevState: Readonly<{}>,
    snapshot?: any
  ): void {
    if (prevProps.type !== this.props.type) {
      this.setState({
        itemSelected: [],
        indexSelected: 0,
        justification: "",
        tableItems: [],
        itemsIndexes: [],
        allListItems: [],
        dataResponse: {},
      });
      void this.loadList();
    }
  }

  public render(): React.ReactElement<IWebpartComunicadosAnaliseProps> {
    const { type } = this.props;

    const tableHeader = [
      "Titulo do Comunicado",
      "Tipo",
      "Data Solicitada",
      "Solicitante",
      "Status",
    ];
    const clickTableRow = (item: number): void => {
      this.setState({
        itemSelected: this.state.tableItems[item],
        indexSelected: this.state.itemsIndexes[item],
        justification: "",
      });
    };

    const changeJustific = (just: string): void => {
      this.setState({ justification: just });
    };

    const changeItemList = (confirm: boolean): void => {
      //index selected
      const itemSelectedAux: any =
        this.state.allListItems[this.state.indexSelected];

      const id = itemSelectedAux.ID;
      const status = (itemSelectedAux.Status = confirm
        ? type === "Primeira Análise"
          ? "Revisado"
          : "Autorizado"
        : "Recusado");
      const justificativa = (itemSelectedAux.Status = confirm
        ? ""
        : this.state.justification);

      void this.editList(id, status, justificativa);
    };

    return (
      <div>
        <div style={{ marginBottom: 20 }}>
          <ArqText
            text="Comunicados Pendentes"
            fontSize="20px"
            fontWeight="bold"
            color="black"
          />
        </div>
        {this.state.tableItems && this.state.tableItems.length > 0 && (
          <ArqTable
            tableItems={this.state.tableItems}
            topLabels={tableHeader}
            hoverItems={true}
            onClickRow={clickTableRow}
          />
        )}
        {(!this.state.tableItems || this.state.tableItems.length === 0) && (
          <div>
            <ArqText text="Nenhuma análise pendente" />
          </div>
        )}
        {this.state.itemSelected &&
          this.state.itemSelected.length > 0 &&
          this.state.allListItems &&
          this.state.allListItems.length > 0 && (
            <div style={{ marginTop: 30 }}>
              <ArqText
                text={"Comunicado: " + this.state.itemSelected[0]}
                fontSize="16px"
                fontWeight="bold"
                color="#4d4e53"
              />
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  gap: 20,
                  marginTop: 20,
                }}
              >
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Solicitante:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .Solicitante_x0020_Nome
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Data Solicitação:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={formatarData(
                      this.state.allListItems[this.state.indexSelected]
                        .DataSolicita_x00e7__x00e3_o
                    )}
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="E-mail:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected].Email
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Departamento:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .Departamento
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Área:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .OData__x00c1_rea
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Público Alvo:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .P_x00fa_blicoAlvo
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Outros:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected].Outros
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Tipo de Comunicado:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .TipodeComunicado
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Título do Comunicado:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected].Title
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Texto do Comunicado:"
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .TextodoComunicado
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Área Responsável: "
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .OData__x00c1_reaRespons_x00e1_vel
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Contato: "
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected].Contato
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="GD para Aprovação: "
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected]
                        .GDparaAprova_x00e7__x00e3_o
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    gap: 5,
                  }}
                >
                  <ArqText
                    text="Status "
                    fontSize="12px"
                    color="#4d4e53"
                    fontWeight="bold"
                  />
                  <ArqText
                    text={
                      this.state.allListItems[this.state.indexSelected].Status
                    }
                    fontSize="14px"
                    color="#4d4e53"
                  />
                </div>

                {this.state.allListItems[this.state.indexSelected].Anexo &&
                  this.state.allListItems[this.state.indexSelected].Anexo
                    .Description && (
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        gap: 5,
                      }}
                    >
                      <ArqText
                        text="Status "
                        fontSize="12px"
                        color="#4d4e53"
                        fontWeight="bold"
                      />
                      <a
                        href={
                          this.state.allListItems[this.state.indexSelected]
                            .Anexo.Url
                        }
                        target="_blank"
                        rel="noreferrer"
                      >
                        {
                          this.state.allListItems[this.state.indexSelected]
                            .Anexo.Description
                        }
                      </a>
                    </div>
                  )}
              </div>
              <div style={{ marginTop: 25 }}>
                <ArqText
                  text="Justificativa: "
                  fontSize="12px"
                  color="#4d4e53"
                />
                <div style={{ marginTop: 5, width: "60%" }}>
                  <ArqTextFieldMultiline
                    value={this.state.justification}
                    onChange={changeJustific}
                  />
                </div>
                <div
                  style={{
                    marginTop: 15,
                    display: "flex",
                    width: "100%",
                    justifyContent: "center",
                    gap: 30,
                  }}
                >
                  <ArqButton
                    text="Recusar"
                    color="#cc092f"
                    hoverColor="#cc092f"
                    styleType="filed"
                    onClick={() => changeItemList(false)}
                  />
                  <ArqButton
                    text="Autorizar"
                    color="#cc092f"
                    hoverColor="#cc092f"
                    styleType="filed"
                    onClick={() => changeItemList(true)}
                  />
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}
