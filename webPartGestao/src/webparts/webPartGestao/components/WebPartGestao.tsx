import * as React from "react";
import { IWebPartGestaoProps } from "./IWebPartGestaoProps";
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubmenuBar";

export default class WebPartGestao extends React.Component<
  IWebPartGestaoProps,
  {}
> {
  public render(): React.ReactElement<IWebPartGestaoProps> {
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb items={["Gestão"]} links={["/"]} />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Gestão"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
        <SubMenuBar />
      </div>
    );
  }
}
