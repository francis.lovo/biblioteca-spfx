import * as React from 'react';
import { IWebpartTrilhasDoConhecimentoProps } from './IWebpartTrilhasDoConhecimentoProps';
import {
  ArqTable,
  ArqBreadcrumb,
  ArqText,
} from "biblioteca-portal";

export default class WebpartTrilhasDoConhecimento extends React.Component<IWebpartTrilhasDoConhecimentoProps, {}> {
  public render(): React.ReactElement<IWebpartTrilhasDoConhecimentoProps> {

   // Dados para a tabela
   const topLabels = ["Nome", "Chave", "Área","Tipo de Conhecimento", "Quantidade de horas","Trilha", "Data de Início","Data de Término", "Nome", "Status",];
   const tableItems = [
     ["Bruno Santana", "m000000", "Governança Arquitetura","Tipo 1","60","Dados","01/01/2022","01/01/2023","Curso Teste 1","Concluído",],
     ["Bruno Santana", "m000000", "Governança Arquitetura","Tipo 1","60","Dados","01/01/2022","01/01/2023","Curso Teste 1","Concluído",],
     ["Bruno Santana", "m000000", "Governança Arquitetura","Tipo 1","60","Dados","01/01/2022","01/01/2023","Curso Teste 1","Concluído",],
   ];
    return (
      <div>
        <ArqBreadcrumb
          items={["Home","Trilhas do Conhecimento"]}
          links={["#","#"]}/>
        <ArqText
          fontSize="20px"
          color="#000000"
          fontWeight="bold"
          text="Trilhas do Conhecimento"/>
          <div 
          style={{
          marginTop: "20px",
          }}> 
          <ArqTable
            topLabels={topLabels}
            tableItems={tableItems}
          />
          </div>
      </div>
    );
  }
}
