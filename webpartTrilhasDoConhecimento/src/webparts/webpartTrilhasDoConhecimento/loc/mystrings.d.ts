declare interface IWebpartTrilhasDoConhecimentoWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AppLocalEnvironmentSharePoint: string;
  AppLocalEnvironmentTeams: string;
  AppLocalEnvironmentOffice: string;
  AppLocalEnvironmentOutlook: string;
  AppSharePointEnvironment: string;
  AppTeamsTabEnvironment: string;
  AppOfficeEnvironment: string;
  AppOutlookEnvironment: string;
}

declare module 'WebpartTrilhasDoConhecimentoWebPartStrings' {
  const strings: IWebpartTrilhasDoConhecimentoWebPartStrings;
  export = strings;
}
