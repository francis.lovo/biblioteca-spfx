/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import * as ReactDom from "react-dom";
import { Version } from "@microsoft/sp-core-library";
import {
  IPropertyPaneConfiguration,
  IPropertyPaneDropdownOption,
  PropertyPaneButton,
  PropertyPaneButtonType,
  PropertyPaneCheckbox,
  PropertyPaneDropdown,
  PropertyPaneLabel,
  PropertyPaneTextField,
  PropertyPaneToggle,
} from "@microsoft/sp-property-pane";
import { BaseClientSideWebPart } from "@microsoft/sp-webpart-base";
import { IReadonlyTheme } from "@microsoft/sp-component-base";
import WebpartGraficoPadrao from "./components/WebpartGraficoPadrao";
import { IWebpartGraficoPadraoProps } from "./components/IWebpartGraficoPadraoProps";
import { PropertyFieldFilePicker } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";
import * as XLSX from "xlsx";

export interface IWebpartGraficoPadraoWebPartProps {
  filePickerResult: any;
  graphicType: string;
  name: string;
  join: boolean;
  labelsChartCb: string;
  filter1: string;
  filter2: string;
  filterY1: string;
  removeDuos: boolean;
  labelChartTf: string;
  filter1Tf: string;
  filter2Tf: string;
  count: boolean;
  chartData: any[];
  fileData: any;
  textSizeX: number;
  textSizeY: number;
  textLegendSize: number;
  toggleTable: boolean;
  paginationTable: number;
  tableFields: any;
}

export default class WebpartGraficoPadraoWebPart extends BaseClientSideWebPart<IWebpartGraficoPadraoWebPartProps> {
  private _isDarkTheme: boolean = false;
  private _environmentMessage: string = "";
  private lastModifiedTime: Date | null = null;
  private fileData: any;
  private columnNames: string[] = [];
  private propertyFilter: any[] = [];
  private propertyFilterY: any[] = [];
  private propertyFilterX: any[] = [];
  private propertyFilterGlobal: any[] = [];
  private chartData: string[][] = [];
  private tableFields: any = PropertyPaneLabel("labelTable", { text: "" });
  private labelChartTfText = "";
  private filter1TfText = "";
  private filter2TfText = "";
  private tableDataText = "";

  private salvarConfiguracoes(): void {
    this.properties.chartData = this.chartData;
    this.properties.fileData = this.fileData;
    this.properties.tableFields = this.tableFields;
    this.context.propertyPane.refresh();
  }

  private passaFiltro(filtro: string, filtros: string[]): boolean {
    let passou = false;
    filtros.map((filt: string) => {
      if (filtro + "" === filt) {
        passou = true;
      }
    });
    return passou;
  }
  private mountThisGraphic(): void {
    //montar grafico
    let consegueMontarGrafico = true;
    let auxLabels: string[] | undefined;
    let auxSecondX: string[] | undefined;
    let filter: string[] | undefined;
    if (this.properties.labelChartTf) {
      auxLabels = this.properties.labelChartTf.split(", ");
    } else {
      consegueMontarGrafico = false;
    }
    if (this.properties.filter1Tf) {
      auxSecondX = this.properties.filter1Tf.split(", ");
    }
    if (this.properties.filter2Tf) {
      filter = this.properties.filter2Tf.split(", ");
    }
    //primeira linha do grafico
    const auxFirstLineChart: string[] = [];
    const auxChartData: string[][] = [];
    if (this.properties.labelsChartCb && this.properties.filterY1) {
      if (this.properties.labelsChartCb === this.properties.filterY1) {
        consegueMontarGrafico = false;
      }
    } else {
      consegueMontarGrafico = false;
    }
    if (consegueMontarGrafico) {
      if (auxSecondX && auxSecondX.length > 0) {
        auxFirstLineChart.push(this.properties.labelsChartCb);
        auxSecondX.map((item: string) => {
          auxFirstLineChart.push(item);
        });
      } else {
        auxFirstLineChart.push(this.properties.labelsChartCb);
        auxFirstLineChart.push(this.properties.filterY1);
      }
      auxChartData.push(auxFirstLineChart);

      //dados do grafico
      if (auxLabels) {
        auxLabels.map((rotulo: string) => {
          const auxLine: any[] = [];
          let count = 0;
          auxLine.push(rotulo);
          const auxDuos: any[] = [];
          if (auxSecondX && auxSecondX.length > 0) {
            //varios x
            auxSecondX.map((barraX: string) => {
              count = 0;
              this.fileData.map((fileLine: any) => {
                if (
                  (filter &&
                    this.passaFiltro(
                      fileLine[this.properties.filter2],
                      filter
                    )) ||
                  !filter ||
                  filter.length === 0
                ) {
                  if (this.properties.count) {
                    if (this.properties.removeDuos) {
                      //retirar duplicado
                      if (
                        rotulo + "" ===
                          fileLine[this.properties.labelsChartCb] + "" &&
                        barraX + "" === fileLine[this.properties.filter1] + ""
                      ) {
                        let achouDuplicado = false;
                        if (auxDuos.length === 0) {
                          auxDuos.push(fileLine[this.properties.filterY1]);
                        } else {
                          auxDuos.map((itemDuo: any) => {
                            if (
                              fileLine[this.properties.filterY1] === itemDuo
                            ) {
                              achouDuplicado = true;
                            }
                          });
                        }
                        if (!achouDuplicado) {
                          auxDuos.push(fileLine[this.properties.filterY1]);
                          if (
                            rotulo + "" ===
                              fileLine[this.properties.labelsChartCb] + "" &&
                            barraX + "" ===
                              fileLine[this.properties.filter1] + ""
                          ) {
                            count = count + 1;
                          }
                        }
                      }
                    } else {
                      //contagem
                      if (
                        rotulo + "" ===
                          fileLine[this.properties.labelsChartCb] + "" &&
                        barraX + "" ===
                          fileLine[this.properties.filter1] + "" &&
                        fileLine[this.properties.filterY1]
                      ) {
                        count = count + 1;
                      }
                    }
                  } else {
                    //sem contagem
                    if (
                      rotulo + "" ===
                        fileLine[this.properties.labelsChartCb] + "" &&
                      barraX + "" === fileLine[this.properties.filter1] + "" &&
                      fileLine[this.properties.filterY1]
                    ) {
                      count = count + +fileLine[this.properties.filterY1];
                    }
                  }
                }
              });

              auxLine.push(count);
            });
            auxChartData.push(auxLine);
          } else {
            //um x
            const auxDuos: any[] = [];
            this.fileData.map((fileLine: any) => {
              if (
                (filter &&
                  this.passaFiltro(
                    fileLine[this.properties.filter2],
                    filter
                  )) ||
                !filter ||
                filter.length === 0
              ) {
                if (this.properties.count) {
                  if (this.properties.removeDuos) {
                    //remover duplicados

                    if (
                      rotulo + "" ===
                      fileLine[this.properties.labelsChartCb] + ""
                    ) {
                      let achouDuplicado = false;
                      if (auxDuos.length === 0) {
                        auxDuos.push(fileLine[this.properties.filterY1]);
                      } else {
                        auxDuos.map((itemDuo: any) => {
                          if (fileLine[this.properties.filterY1] === itemDuo) {
                            achouDuplicado = true;
                          }
                        });
                      }
                      if (!achouDuplicado) {
                        auxDuos.push(fileLine[this.properties.filterY1]);
                        if (
                          rotulo + "" ===
                            fileLine[this.properties.labelsChartCb] + "" &&
                          fileLine[this.properties.filterY1]
                        ) {
                          count = count + 1;
                        }
                      }
                    }
                  } else {
                    //contagem normal
                    if (
                      rotulo + "" ===
                        fileLine[this.properties.labelsChartCb] + "" &&
                      fileLine[this.properties.filterY1]
                    ) {
                      count = count + 1;
                    }
                  }
                } else {
                  //sem contagem
                  if (
                    rotulo + "" ===
                      fileLine[this.properties.labelsChartCb] + "" &&
                    fileLine[this.properties.filterY1]
                  ) {
                    count = count + +fileLine[this.properties.filterY1];
                  }
                }
              }
            });
            auxLine.push(count);
            auxChartData.push(auxLine);
          }
        });
        console.log(auxChartData);
      }
    }
    this.chartData = auxChartData;
  }
  private async readThisFile(): Promise<void> {
    try {
      this.fileData = [];

      await this.onInit();
    } catch (error) {
      console.error("Erro ao ler o arquivo: ", error);
    }
  }
  private loadTemplates(): void {
    this.propertyFilter = [];
    this.propertyFilterY = [];
    this.propertyFilterX = [];
    this.propertyFilterGlobal = [];
    const cbChartOptions: IPropertyPaneDropdownOption[] = [];
    cbChartOptions.push({
      key: "Nenhum",
      text: "Nenhum",
    });
    this.columnNames.map(function (item) {
      cbChartOptions.push({
        key: item,
        text: item,
      });
    });
    this.propertyFilterX.push(
      PropertyPaneDropdown("labelsChartCb", {
        label: "Eixo X (Rótulos dos Gráficos)*:",
        options: cbChartOptions,
      })
    );
    this.propertyFilterX.push(
      PropertyPaneTextField("labelChartTf", {
        label: "Rótulos Selecionados: ",
        value: this.labelChartTfText,
      })
    );
    this.propertyFilter.push(
      PropertyPaneDropdown("filter1", {
        label: "Segundo Eixo X (Recomendado para Barras Duplas, Triplas, etc):",
        options: cbChartOptions,
      })
    );
    this.propertyFilter.push(
      PropertyPaneTextField("filter1Tf", {
        label: "Segundo Eixo X Selecionado: ",
        value: this.filter1TfText,
      })
    );
    this.propertyFilterGlobal.push(
      PropertyPaneDropdown("filter2", {
        label: "Filtro:",
        options: cbChartOptions,
      })
    );
    this.propertyFilterGlobal.push(
      PropertyPaneTextField("filter2Tf", {
        label: "Filtro Selecionado: ",
        value: this.filter2TfText,
      })
    );
    this.propertyFilterY.push(
      PropertyPaneDropdown("filterY1", {
        label: "Eixo Y* (Recomendado campo numérico):",
        options: cbChartOptions,
      })
    );
  }
  async carregarArquivo(): Promise<void> {
    try {
      const response = await fetch(
        this.properties.filePickerResult.fileAbsoluteUrl,
        {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        }
      );

      if (response.ok) {
        const arrayBuffer = await response.arrayBuffer(); // tem que fazer isso pra processar o arquivo XLSX
        const data = new Uint8Array(arrayBuffer);
        const workbook = XLSX.read(data, { type: "array" }); // pra ler e pegar as planilhas do arquivo

        const planilhaX = workbook.Sheets[workbook.SheetNames[0]]; // pra acessar a planilha desejada do arquivo

        //const jsonData: any[] = XLSX.utils.sheet_to_json(planilhaX); // converter dados da planilha em json

        const jsonData: any[] = XLSX.utils.sheet_to_json(planilhaX, {
          raw: false, // isso garante que as datas sejam interpretadas corretamente
        });

        //this.setState({ fileData: jsonData });
        this.fileData = jsonData;
        console.log("DADOS EM JSON: ", jsonData);
        if (jsonData && jsonData.length > 0) {
          this.columnNames = Object.keys(jsonData[0]);
          this.loadTemplates();
        }
        this.salvarConfiguracoes();
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  }
  protected onPropertyPaneFieldChanged(
    propertyPath: string,
    oldValue: any,
    newValue: any
  ): void {
    if (propertyPath === "readFile") {
      this.loadTemplates();
    }
    if (propertyPath === "toggleTable" && newValue !== oldValue) {
      if (newValue) {
        this.tableFields = PropertyPaneTextField("paginationTable", {
          label: "Número de linhas por página:",
          description: "Campo numérico",
          value: this.tableDataText,
        });
      } else {
        this.tableFields = PropertyPaneLabel("labelTable", { text: "" });
      }
    }
    if (propertyPath === "labelsChartCb" && newValue !== oldValue) {
      const auxStr: string[] = [];
      if (this.properties.labelsChartCb !== "Nenhum") {
        this.labelChartTfText = "";
        this.fileData.map((item: any) => {
          if (auxStr.length === 0) {
            auxStr.push(item[this.properties.labelsChartCb]);
          } else {
            let achou = false;
            auxStr.map((rotulo: string) => {
              if (rotulo === item[this.properties.labelsChartCb]) {
                achou = true;
              }
            });
            if (!achou) {
              auxStr.push(item[this.properties.labelsChartCb]);
            }
          }
        });
      }
      if (auxStr.length > 0) {
        this.properties.labelChartTf = auxStr.sort().join(", ");
        this.labelChartTfText = auxStr.sort().join(", ");
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      } else {
        this.properties.labelChartTf = "";
        this.labelChartTfText = "";
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      }
    }
    if (propertyPath === "filter1" && newValue !== oldValue) {
      const auxStr1: string[] = [];
      if (this.properties.filter1 !== "Nenhum") {
        this.filter1TfText = "";
        this.fileData.map((item: any) => {
          if (auxStr1.length === 0) {
            auxStr1.push(item[this.properties.filter1]);
          } else {
            let achou = false;
            auxStr1.map((rotulo: string) => {
              if (rotulo === item[this.properties.filter1]) {
                achou = true;
              }
            });
            if (!achou) {
              auxStr1.push(item[this.properties.filter1]);
            }
          }
        });
      }
      if (auxStr1.length > 0) {
        this.properties.filter1Tf = auxStr1.sort().join(", ");
        this.filter1TfText = auxStr1.sort().join(", ");
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      } else {
        this.properties.filter1Tf = "";
        this.filter1TfText = "";
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      }
    }
    if (propertyPath === "filter2" && newValue !== oldValue) {
      const auxStr2: string[] = [];
      if (this.properties.filter2 !== "Nenhum") {
        this.filter2TfText = "";
        this.fileData.map((item: any) => {
          if (auxStr2.length === 0) {
            auxStr2.push(item[this.properties.filter2]);
          } else {
            let achou = false;
            auxStr2.map((rotulo: string) => {
              if (rotulo === item[this.properties.filter2]) {
                achou = true;
              }
            });
            if (!achou) {
              auxStr2.push(item[this.properties.filter2]);
            }
          }
        });
      }
      if (auxStr2.length > 0) {
        this.properties.filter2Tf = auxStr2.sort().join(", ");
        this.filter2TfText = auxStr2.sort().join(", ");
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      } else {
        this.properties.filter2Tf = "";
        this.filter2TfText = "";
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      }
    }

    super.onPropertyPaneFieldChanged(propertyPath, oldValue, newValue);
  }

  public render(): void {
    const element: React.ReactElement<IWebpartGraficoPadraoProps> =
      React.createElement(WebpartGraficoPadrao, {
        isDarkTheme: this._isDarkTheme,
        environmentMessage: this._environmentMessage,
        hasTeamsContext: !!this.context.sdks.microsoftTeams,
        userDisplayName: this.context.pageContext.user.displayName,
        filePickerResult: this.properties.filePickerResult,
        graphicType: this.properties.graphicType,
        chartTitle: this.properties.name,
        chartData: this.chartData,
        fileData: this.fileData,
        joinData: this.properties.join,
        textSizeX: this.properties.textSizeX,
        textSizeY: this.properties.textSizeY,
        textLegendSize: this.properties.textLegendSize,
        toggleTable: this.properties.toggleTable,
        paginationTable: this.properties.paginationTable,
      });

    ReactDom.render(element, this.domElement);
  }
  private carregarConfiguracoes(): void {
    if (this.properties.chartData) {
      this.chartData = this.properties.chartData;
    }
    if (this.properties.fileData) {
      this.fileData = this.properties.fileData;
      this.columnNames = Object.keys(this.properties.fileData[0]);
      this.loadTemplates();
    }
    if (this.properties.tableFields) {
      this.tableFields = this.properties.tableFields;
    }
  }

  protected async onInit(): Promise<void> {
    this.carregarConfiguracoes();
    if (
      this.properties.filePickerResult &&
      this.properties.filePickerResult.fileAbsoluteUrl
    ) {
      await this.carregarArquivo(); // Aguarde a conclusão da leitura do arquivo
      this.carregarConfiguracoes();
      this.loadTemplates();
      this.onPropertyPaneFieldChanged("readFile", null, true);
      this.getPropertyPaneConfiguration();

      if(this.properties.labelsChartCb){
        this.onPropertyPaneFieldChanged("labelsChartCb", null, this.properties.labelsChartCb)
      }

      this.salvarConfiguracoes();
      this.mountThisGraphic();
    }
    return super.onInit();
  }

  protected onThemeChanged(currentTheme: IReadonlyTheme | undefined): void {
    if (!currentTheme) {
      return;
    }

    this._isDarkTheme = !!currentTheme.isInverted;
    const { semanticColors } = currentTheme;

    if (semanticColors) {
      this.domElement.style.setProperty(
        "--bodyText",
        semanticColors.bodyText || null
      );
      this.domElement.style.setProperty("--link", semanticColors.link || null);
      this.domElement.style.setProperty(
        "--linkHovered",
        semanticColors.linkHovered || null
      );
    }
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse("1.0");
  }
  protected onPropertyPaneConfigurationStart(): void {
    this.carregarConfiguracoes();
    super.onPropertyPaneConfigurationStart();
  }

  public getUrlModification(): void {
    const url = this.properties.filePickerResult.fileAbsoluteUrl;
    const parts = url.split("/");
    const lastPart = parts[parts.length - 1];

    const regex =
      /https:\/\/bancobradesco\.sharepoint\.com\/teams\/5800-pa\/(.*?)(?=\/[^/]+$)/;

    const match = url.match(regex);

    if (match) {
      const listName = match[1];
      const fullUrl = `https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/GetFolderByServerRelativeUrl('${listName}')/files?$filter=Name eq '${lastPart}'`;

      this.getModification(fullUrl);
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  async getModification(url: string) {
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        const results = data.d.results;
        results.forEach((item: any) => {
          const timeLastModified = item.TimeLastModified;

          if (
            !this.lastModifiedTime ||
            timeLastModified > this.lastModifiedTime
          ) {
            this.lastModifiedTime = timeLastModified;
            this.readThisFile();
          }
        });
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: "Montagem Gráfico",
          },
          groups: [
            {
              groupName: "Arquivo:",
              groupFields: [
                PropertyFieldFilePicker("filePicker", {
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: any) => {
                    this.properties.filePickerResult = e;

                    this.getUrlModification();
                  },
                  onChanged: (e: any) => {
                    this.properties.filePickerResult = e;
                  },
                  key: "filePickerId",
                  buttonLabel: this.properties.filePickerResult
                    ? "Mudar Arquivo"
                    : "Escolher Arquivo",
                  label: "Arquivo Excel",
                  context: this.context as any,
                  filePickerResult: this.properties.filePickerResult,
                }),
                PropertyPaneButton("readFile", {
                  text: "Ler Arquivo Excel",
                  buttonType: PropertyPaneButtonType.Primary,
                  onClick: this.readThisFile.bind(this),
                  disabled: this.properties.filePickerResult ? false : true,
                }),
                PropertyPaneTextField("name", {
                  label: "Título do Gráfico:",
                }),
                PropertyPaneDropdown("graphicType", {
                  label: "Tipo Gráfico:",
                  options: [
                    { key: "Barras", text: "Barras" },
                    { key: "Coluna", text: "Coluna" },
                    { key: "Dispersão", text: "Dispersão" },
                    { key: "Linhas", text: "Linhas" },
                    { key: "Pizza", text: "Pizza" },
                  ],
                }),
              ],
            },
            {
              groupName: "Eixo Y",
              groupFields: this.propertyFilterY,
            },
            {
              groupName: "Filtro Eixo Y (Opcionais)",
              groupFields: [
                PropertyPaneCheckbox("count", {
                  checked: false,
                  disabled: false,
                  text: "Contagem de linhas",
                }),
                PropertyPaneCheckbox("removeDuos", {
                  checked: false,
                  disabled: this.properties.count ? false : true,
                  text: "Remover Duplicados",
                }),
                PropertyPaneCheckbox("join", {
                  checked: false,
                  disabled: false,
                  text: "Agrupar Colunas",
                }),
              ],
            },
            {
              groupName: "Eixo X",
              groupFields: this.propertyFilterX,
            },
            {
              groupName: "Tamanho dos Rótulos",
              groupFields: [
                PropertyPaneTextField("textSizeY", {
                  label: "Tamanho da Fonte no Eixo Y:",
                }),
                PropertyPaneTextField("textSizeX", {
                  label: "Tamanho da Fonte no Eixo X:",
                }),
                PropertyPaneTextField("textLegendSize", {
                  label: "Tamanho da Fonte da Legenda:",
                }),
              ],
            },
            {
              groupName: "Filtro do eixo X (Opcionais)",
              groupFields: this.propertyFilter,
            },
            {
              groupName: "Filtro Geral",
              groupFields: this.propertyFilterGlobal,
            },
            {
              groupName: "Tabela de Dados",
              groupFields: [
                PropertyPaneToggle("toggleTable", { label: "Mostrar Tabela" }),
                this.tableFields,
              ],
            },
            {
              groupName: "",
              groupFields: [
                PropertyPaneLabel("warningLabel", {
                  text:
                    this.properties.labelsChartCb === this.properties.filterY1
                      ? "Gráficos com o mesmo conteúdo no eixo x e no eixo y não podem ser montados"
                      : "",
                }),
                PropertyPaneButton("mountGraphic", {
                  text: "Montar Gráfico",
                  onClick: this.mountThisGraphic.bind(this),
                  disabled: this.properties.filePickerResult ? false : true,
                }),
                PropertyPaneButton("salvarConfiguracoes", {
                  text: "Salvar Configurações",
                  buttonType: PropertyPaneButtonType.Primary,
                  onClick: this.salvarConfiguracoes.bind(this),
                }),
              ],
            },
          ],
        },
      ],
    };
  }
}
