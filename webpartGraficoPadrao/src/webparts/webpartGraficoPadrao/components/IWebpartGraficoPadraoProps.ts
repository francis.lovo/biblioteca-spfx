/* eslint-disable @typescript-eslint/no-explicit-any */
export interface IWebpartGraficoPadraoProps {
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
  filePickerResult: any;
  graphicType: string;
  chartTitle: string;
  chartData: any[];
  joinData: boolean;
  fileData: any[];
  textSizeX: number;
  textSizeY: number;
  textLegendSize: number;
  toggleTable: boolean;
  paginationTable: number;
}
