/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import styles from "./WebpartGraficoPadrao.module.scss";
import { IWebpartGraficoPadraoProps } from "./IWebpartGraficoPadraoProps";
import { escape } from "@microsoft/sp-lodash-subset";
import { ArqGoogleChart, ArqPagination, ArqTable } from "biblioteca-portal";

interface MyState {
  currentPagePagination: string;
  currentTableData: any[];
}

export default class WebpartGraficoPadrao extends React.Component<
  IWebpartGraficoPadraoProps,
  MyState
> {
  state: MyState = {
    currentPagePagination: "1",
    currentTableData: this.props.chartData,
  };
  componentDidMount(): void {
    if (
      this.props.paginationTable > 0 &&
      this.props.chartData &&
      this.props.chartData.length > 0
    ) {
      //logica para separar
      const startIndex =
        (+this.state.currentPagePagination - 1) * this.props.paginationTable;
      const endIndex = startIndex + 1 + +this.props.paginationTable;
      // Dados para a página atual
      const aux = this.props.chartData.slice(startIndex + 1, endIndex);
      aux.unshift(this.props.chartData[0]);

      this.setState({
        currentTableData: aux,
      });
    }
  }
  componentDidUpdate(
    prevProps: Readonly<IWebpartGraficoPadraoProps>,
    prevState: Readonly<MyState>,
    snapshot?: any
  ): void {
    if (
      this.props.paginationTable > 0 &&
      this.props.chartData &&
      this.props.chartData.length > 0 &&
      prevProps !== this.props
    ) {
      //logica para separar
      const startIndex =
        (+this.state.currentPagePagination - 1) * this.props.paginationTable;
      const endIndex = startIndex + 1 + +this.props.paginationTable;
      // Dados para a página atual
      const aux = this.props.chartData.slice(startIndex + 1, endIndex);
      aux.unshift(this.props.chartData[0]);

      this.setState({
        currentTableData: aux,
        currentPagePagination: "1",
      });
    }
    if (
      prevProps !== this.props &&
      (!this.props.paginationTable ||
        this.props.paginationTable + "" === "" ||
        +this.props.paginationTable === 0)
    ) {
      this.setState({
        currentTableData: this.props.chartData,
        currentPagePagination: "1",
      });
    }
  }
  public render(): React.ReactElement<IWebpartGraficoPadraoProps> {
    const {
      hasTeamsContext,
      graphicType,
      chartTitle,
      chartData,
      joinData,
      textSizeX,
      textSizeY,
      textLegendSize,
      toggleTable,
      paginationTable,
    } = this.props;
    const totalPages = (chartData.length - 1) / paginationTable + "";
    const changePage = (currentPage: number): void => {
      //logica para separar
      const startIndex = (+currentPage - 1) * this.props.paginationTable;
      const endIndex = startIndex + 1 + +this.props.paginationTable;
      // Dados para a página atual
      const aux = this.props.chartData.slice(startIndex + 1, endIndex);
      aux.unshift(this.props.chartData[0]);

      this.setState({
        currentTableData: aux,
      });
    };
    const onClickPageUp = (): void => {
      if (+this.state.currentPagePagination - 1 >= 1) {
        this.setState({
          currentPagePagination: +this.state.currentPagePagination - 1 + "",
        });
        changePage(+this.state.currentPagePagination - 1);
      }
    };
    const onClickPageDown = (): void => {
      const totalPagesAux =
        +totalPages < 1
          ? chartData.length - 1 + ""
          : totalPages.split(".").length > 1
          ? +totalPages.split(".")[0] + 1 + ""
          : totalPages;

      if (+this.state.currentPagePagination + 1 <= +totalPagesAux) {
        this.setState({
          currentPagePagination: +this.state.currentPagePagination + 1 + "",
        });
        changePage(+this.state.currentPagePagination + 1);
      }
    };
    const onClickFirstPage = (): void => {
      this.setState({
        currentPagePagination: "1",
      });
      changePage(1);
    };
    const onClickLastPage = (): void => {
      const totalPagesAux =
        +totalPages < 1
          ? chartData.length - 1 + ""
          : totalPages.split(".").length > 1
          ? +totalPages.split(".")[0] + 1 + ""
          : totalPages;
      this.setState({
        currentPagePagination: totalPagesAux,
      });
      changePage(+totalPagesAux);
    };
    return (
      <section
        className={`${styles.webpartGraficoPadrao} ${
          hasTeamsContext ? styles.teams : ""
        }`}
      >
        <div className={styles.welcome}>
          <ArqGoogleChart
            chartTitle={escape(chartTitle)}
            joinData={joinData}
            graphicType={graphicType}
            chartData={chartData}
            textSizeX={textSizeX}
            textSizeY={textSizeY}
            textLegendSize={textLegendSize}
          />
        </div>
        {chartData &&
          chartData.length > 0 &&
          toggleTable &&
          this.state.currentTableData &&
          this.state.currentTableData.length > 0 && (
            <div style={{ overflowX: "auto" }}>
              <ArqTable
                tableItems={this.state.currentTableData}
                topLabels={[chartTitle]}
              />
              {paginationTable &&
                +paginationTable > 0 &&
                !isNaN(+totalPages) &&
                paginationTable < chartData.length - 1 && (
                  <ArqPagination
                    currentPage={this.state.currentPagePagination}
                    totalPages={
                      +totalPages < 1
                        ? chartData.length - 1 + ""
                        : totalPages.split(".").length > 1
                        ? +totalPages.split(".")[0] + 1 + ""
                        : totalPages
                    }
                    onClickPageUp={onClickPageUp}
                    onClickPageDown={onClickPageDown}
                    onClickLastPage={onClickLastPage}
                    onClickFirstPage={onClickFirstPage}
                  />
                )}
            </div>
          )}
      </section>
    );
  }
}
