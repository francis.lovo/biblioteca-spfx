/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-use-before-define */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { IWebpartComunicadosFormProps } from "./IWebpartComunicadosFormProps";
import { FormSolicitante } from "./FormSolicitante";
import { FormComunicado } from "./FormComunicado";
import {
  ArqButton,
  ArqSuccessModal,
  ArqTable,
  ArqText,
  formatarData,
} from "biblioteca-portal";
import { FormContato } from "./FormContato";
import { SPHttpClient } from "@microsoft/sp-http";

interface UserProfileProperty {
  Key: string;
  Value: string;
}

interface MyState {
  justification: string;
  files: any[];
  area: any;
  areaResponsavel: any;
  tipoComunicado: string | undefined;
  contato: any;
  publicoAlvo: any;
  outros: any;
  tituloComunicado: any;
  textoComunicado: any;
  anexoComunicado: any;
  gdAprovacao: any;
  openModal: boolean;
  error: boolean;
  dataResponse: any;
  tableItems: any[];
  itemsIndexes: any[];
  allListItems: any;
  itemSelected: any;
  indexSelected: number;
}

export default class WebpartComunicadosForm extends React.Component<
  IWebpartComunicadosFormProps,
  MyState
> {
  state: MyState = {
    justification: "",
    files: [],
    area: undefined,
    areaResponsavel: undefined,
    tipoComunicado: undefined,
    contato: undefined,
    publicoAlvo: undefined,
    outros: undefined,
    tituloComunicado: undefined,
    textoComunicado: undefined,
    anexoComunicado: undefined,
    gdAprovacao: undefined,
    openModal: false,
    error: false,
    dataResponse: undefined,
    tableItems: [],
    itemsIndexes: [],
    allListItems: undefined,
    itemSelected: undefined,
    indexSelected: -1,
  };
  public mountTableItems = (data: any) => {
    const auxTable: any[] = [];
    const indexesAux: number[] = [];
    data.forEach((item: any, index: number) => {
      const auxRow: string[] = [];
      if (item.Status === "Recusado" &&
        item.Solicitante_x0020_Nome === this.props.userDisplayName
      ) {
        auxRow.push(item.Title);
        auxRow.push(formatarData(item.DataSolicita_x00e7__x00e3_o));
        auxRow.push(item.Solicitante_x0020_Nome);
        auxRow.push(item.Status);
        auxRow.push(item.Justificativa);
        auxTable.push(auxRow);
        indexesAux.push(index);
      }
    });
    console.log("DATA: ", data);

    this.setState({
      tableItems: auxTable,
      itemsIndexes: indexesAux,
      allListItems: data,
    });
  };
  public loadListData = async (): Promise<void> => {
    const webUrl =
      "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('Comunicado')/items";
    //"https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('FAQ')/items";

    try {
      const response = await fetch(webUrl, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        this.setState({ dataResponse: data });
        this.mountTableItems(data.d.results);
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  };
  componentDidMount(): void {
    this.loadListData();
  }
  componentDidUpdate(
    prevProps: Readonly<IWebpartComunicadosFormProps>,
    prevState: Readonly<MyState>,
    snapshot?: any
  ): void {
    if (
      prevState.itemSelected !== this.state.itemSelected &&
      this.state.itemSelected
    ) {
      const itemSelected = this.state.allListItems[this.state.indexSelected];
      this.setState({
        publicoAlvo: itemSelected.P_x00fa_blicoAlvo,
        outros: itemSelected.Outros,
        area: itemSelected.OData__x00c1_rea,
        tituloComunicado: itemSelected.Title,
        tipoComunicado: itemSelected.TipodeComunicado,
        textoComunicado: itemSelected.TextodoComunicado,
        areaResponsavel: itemSelected.OData__x00c1_reaRespons_x00e1_vel,
        contato: itemSelected.Contato,
        gdAprovacao: itemSelected.GDparaAprova_x00e7__x00e3_o,
      });
    }
  }

  public render(): React.ReactElement<IWebpartComunicadosFormProps> {
    const tableHeader = [
      "Titulo do Comunicado",
      "Data Solicitada",
      "Solicitante",
      "Status",
      "Justificativa",
    ];
    const changePublicoAlvo = (publicoAlvo: string): void => {
      this.setState({ publicoAlvo: publicoAlvo });
    };
    const changeOutros = (outros: string): void => {
      this.setState({ outros: outros });
    };
    const changeArea = (value: string | number): void => {
      this.setState({ area: value });
    };
    const changeTituloComunicado = (tituloComunicado: string): void => {
      this.setState({ tituloComunicado: tituloComunicado });
    };
    const changeTextoComunicado = (textoComunicado: string): void => {
      this.setState({ textoComunicado: textoComunicado });
    };
    const changeTipoComunicado = (value: string): void => {
      this.setState({ tipoComunicado: value });
    };
    const changeAreaResponsavel = (value: string | number): void => {
      this.setState({ areaResponsavel: value });
    };
    const changeContato = (value: string | number): void => {
      this.setState({ contato: value });
    };
    const changeGDAprovacao = (gdAprovacao: string): void => {
      this.setState({ gdAprovacao: gdAprovacao });
    };
    const changeFiles = (files: any): void => {
      this.setState({ files: files });
    };
    const URLBase = "https://bancobradesco.sharepoint.com/teams/5800-pa";

    const validateFields = (): boolean => {
      const requiredFields: (keyof (typeof WebpartComunicadosForm)["prototype"]["state"])[] =
        [
          "area",
          "publicoAlvo",
          "tipoComunicado",
          "tituloComunicado",
          "textoComunicado",
          "areaResponsavel",
          "contato",
          "gdAprovacao",
        ];

      const hasEmptyField = requiredFields.every((field) => {
        const value = this.state[field];
        return (
          value !== undefined && value !== null && String(value).trim() !== ""
        );
      });

      this.setState({ error: !hasEmptyField });

      return hasEmptyField;
    };
    const enviarInfo = (): void => {
      if (!validateFields()) {
        alert("É necessário o preenchimento dos campos.");
        return;
      }
      if (this.state.indexSelected === -1) {
        getCurrentUser().then((userData) => {
          getCurrentUserID().then((userID) => {
            if (userID && userData) {
              const { loggedUser, email, department } = userData;

              const urlListBS = `${URLBase}/_api/web/lists/getByTitle('Comunicado')/items`;

              this.props.spHttpClient
                .post(`${urlListBS}`, SPHttpClient.configurations.v1, {
                  headers: {
                    Accept: "application/json;odata=verbose",
                    "Content-Type": "application/json;odata=verbose",
                    "odata-version": "",
                  },
                  body: JSON.stringify({
                    __metadata: {
                      type: "SP.Data.ComunicadoListItem",
                    },
                    SolicitanteId: userID,
                    SolicitanteStringId: userID.toString(),
                    Solicitante_x0020_Nome: loggedUser,
                    Email: email,
                    Departamento: department,
                    OData__x00c1_rea: this.state.area,
                    P_x00fa_blicoAlvo: this.state.publicoAlvo,
                    Outros: this.state.outros,
                    TipodeComunicado: this.state.tipoComunicado,
                    Title: this.state.tituloComunicado,
                    TextodoComunicado: this.state.textoComunicado,
                    OData__x00c1_reaRespons_x00e1_vel:
                      this.state.areaResponsavel,
                    Contato: this.state.contato,
                    GDparaAprova_x00e7__x00e3_o: this.state.gdAprovacao,
                  }),
                })
                .then((listResponse) => {
                  if (listResponse.ok) {
                    return listResponse.json().then((responseData) => {
                      const itemId = responseData.d.Id;
                      const documents = this.state.files as File[];

                      for (const file of documents) {
                        const formData = new FormData();
                        formData.append("file", file, file.name);

                        this.props.spHttpClient
                          .post(
                            `${urlListBS}(${itemId})/AttachmentFiles/add(FileName='${file.name}')`,
                            SPHttpClient.configurations.v1,
                            {
                              body: formData,
                            }
                          )
                          .then((addAttachmentResponse) => {
                            if (addAttachmentResponse.ok) {
                              const attachmentBaseUrl = `${URLBase}/Lists/Comunicado/Attachments`;
                              const attachmentRelativeUrl = `${itemId}/${encodeURIComponent(
                                file.name
                              )}`;
                              const attachmentFullUrl = `${attachmentBaseUrl}/${attachmentRelativeUrl}`;

                              const updateItemUrl = `${urlListBS}(${itemId})`;
                              const updateItemPayload = {
                                __metadata: {
                                  type: "SP.Data.ComunicadoListItem",
                                },
                                Anexo: {
                                  Description: file.name,
                                  Url: attachmentFullUrl,
                                },
                              };

                              const updateConfig = {
                                method: "PATCH",
                                headers: {
                                  Accept: "application/json;odata=verbose",
                                  "Content-Type":
                                    "application/json;odata=verbose",
                                  "X-HTTP-Method": "MERGE",
                                  "If-Match": "*",
                                  "odata-version": "",
                                },
                                body: JSON.stringify(updateItemPayload),
                              };

                              this.props.spHttpClient
                                .post(
                                  updateItemUrl,
                                  SPHttpClient.configurations.v1,
                                  updateConfig
                                )
                                .then((updateResponse) => {
                                  if (!updateResponse.ok) {
                                    throw new Error(
                                      "Erro ao salvar URL do anexo na coluna Anexo"
                                    );
                                  }
                                  return updateResponse.json();
                                });
                            } else {
                              alert(
                                "Erro ao salvar URL do anexo na coluna Anexo."
                              );
                            }
                          });
                      }
                      this.setState({ openModal: true });
                    });
                  } else {
                    alert("Erro ao solicitar a requisição.");
                  }
                });
            } else {
              alert("Erro ao obter informações do usuário.");
            }
          });
        });
      } else {
        //editar dados
        const itemSelectedAux: any =
          this.state.allListItems[this.state.indexSelected];
        const id = itemSelectedAux.ID;

        editList(id);
        this.setState({
          itemSelected: undefined,
          indexSelected: -1,
          publicoAlvo: "",
          outros: "",
          area: "",
          tituloComunicado: "",
          tipoComunicado: undefined,
          textoComunicado: "",
          areaResponsavel: "",
          contato: "",
          gdAprovacao: "",
        });
      }
      this.loadListData();
    };

    const editList = async (id: any) => {
      const webUrl = `${URLBase}/_api/web/lists/GetByTitle('Comunicado')/items(${id})`;

      const updateItemPayload = {
        __metadata: {
          type: "SP.Data.ComunicadoListItem",
        },
        OData__x00c1_rea: this.state.area,
        P_x00fa_blicoAlvo: this.state.publicoAlvo,
        Outros: this.state.outros,
        TipodeComunicado: this.state.tipoComunicado,
        Title: this.state.tituloComunicado,
        TextodoComunicado: this.state.textoComunicado,
        OData__x00c1_reaRespons_x00e1_vel: this.state.areaResponsavel,
        Contato: this.state.contato,
        GDparaAprova_x00e7__x00e3_o: this.state.gdAprovacao,
        Status: "Em análise",
        Justificativa: "",
      };

      const requestOptions = {
        method: "PATCH",
        headers: {
          Accept: "application/json;odata=verbose",
          "Content-Type": "application/json;odata=verbose",
          "X-HTTP-Method": "MERGE",
          "If-Match": "*",
          "odata-version": "",
        },
        body: JSON.stringify(updateItemPayload),
      };

      try {
        const response = await this.props.spHttpClient.post(
          webUrl,
          SPHttpClient.configurations.v1,
          requestOptions
        );

        if (response.ok) {
          console.log("Item atualizado com sucesso.");
          this.setState({
            itemSelected: undefined,
            indexSelected: 0,
            justification: "",
          });
          await this.loadListData();
        } else {
          console.error("Falha na requisição:", response);
        }
      } catch (error) {
        console.error("Erro na requisição:", error);
      }
    };

    const getCurrentUser = async () => {
      try {
        const currentUserUrl = `${URLBase}/_api/SP.UserProfiles.PeopleManager/GetMyProperties`;

        const response = await fetch(currentUserUrl, {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        });

        if (response.ok) {
          const data = await response.json();

          if (data && data.d && data.d.UserProfileProperties) {
            const userProfileProperties = data.d.UserProfileProperties.results;
            const departmentProperty: UserProfileProperty | undefined =
              userProfileProperties.find(
                (prop: UserProfileProperty) => prop.Key === "Office"
              );

            const loggedUser = data.d.DisplayName || "Not Available";
            const email = data.d.Email || "Not Available";

            if (departmentProperty) {
              const department = departmentProperty.Value;

              return {
                department,
                loggedUser,
                email,
              };
            } else {
              console.error("Departamento do usuário não encontrado.");
              return null;
            }
          }
        } else {
          console.error(
            "Erro ao obter informações do usuário:",
            response.status
          );
          return null;
        }
      } catch (error) {
        console.error("Erro geral:", error);
        return null;
      }
    };

    const getCurrentUserID = async () => {
      try {
        const currentUserUrl = `${URLBase}/_api/web/currentuser`;

        const response = await fetch(currentUserUrl, {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        });

        if (response.ok) {
          const data = await response.json();

          if (data && data.d) {
            const userId = data.d.Id;
            return userId;
          } else {
            console.error("Objeto 'data' ou 'data.d' é indefinido.");
            return null;
          }
        } else {
          console.error(
            "Erro ao obter informações do usuário:",
            response.statusText
          );
          return null;
        }
      } catch (error) {
        console.error("Erro geral:", error);
        return null;
      }
    };

    const handleCloseModal = (): void => {
      this.setState({ openModal: false });
      this.loadListData();
    };

    const clickTabelRow = (item: number): void => {
      this.setState({
        itemSelected: this.state.tableItems[item],
        indexSelected: this.state.itemsIndexes[item],
      });
    };

    return (
      <div>
        {this.state.openModal && (
          <ArqSuccessModal
            isOpen={this.state.openModal}
            onClick={handleCloseModal}
          />
        )}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            width: "100%",
            justifyContent: "space-between",
          }}
        >
          <div
            style={{
              width: "29%",
            }}
          >
            <FormSolicitante
              publicoAlvoState={this.state.publicoAlvo}
              outrosState={this.state.outros}
              changePublicoAlvo={changePublicoAlvo}
              changeOutros={changeOutros}
              areaState={this.state.area}
              changeArea={changeArea}
              error={this.state.error}
            />
          </div>
          <div
            style={{
              width: "40%",
            }}
          >
            <FormComunicado
              tipoComunicadoState={this.state.tipoComunicado}
              changeTipoComunicado={changeTipoComunicado}
              tituloComunicadoState={this.state.tituloComunicado}
              textoComunicadoState={this.state.textoComunicado}
              anexoState={this.state.anexoComunicado}
              changeTituloComunicado={changeTituloComunicado}
              changeTextoComunicado={changeTextoComunicado}
              error={this.state.error}
            />
          </div>
          <div
            style={{
              width: "29%",
            }}
          >
            <FormContato
              gdAprovacaoState={this.state.gdAprovacao}
              changeGD={changeGDAprovacao}
              areaResponsavelState={this.state.areaResponsavel}
              changeAreaResponsavel={changeAreaResponsavel}
              contatoState={this.state.contato}
              changeContato={changeContato}
              changeFiles={changeFiles}
              error={this.state.error}
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            marginTop: 20,
            width: "100%",
          }}
        >
          <div style={{ width: "max-content", marginRight: 5 }}>
            <ArqButton
              color="#0E81ED"
              hoverColor="#0E81ED"
              text="Enviar para Análise"
              onClick={enviarInfo}
              styleType="filed"
            />
          </div>
        </div>
        <div style={{ marginTop: 25 }}>
          <div style={{ marginBottom: 10 }}>
            <ArqText
              text="Meus Comunicados"
              fontSize="20px"
              fontWeight="bold"
              color="black"
            />
          </div>
          {this.state.tableItems && this.state.tableItems.length > 0 && (
            <ArqTable
              tableItems={this.state.tableItems}
              topLabels={tableHeader}
              hoverItems={true}
              onClickRow={clickTabelRow}
            />
          )}
          {(!this.state.tableItems || this.state.tableItems.length === 0) && (
            <div>
              <ArqText text="Nenhum comunicado pendente" />
            </div>
          )}
        </div>
      </div>
    );
  }
}
