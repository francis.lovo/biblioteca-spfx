import {
  ArqInformationCard,
  ArqSelectInput,
  ArqText,
  ArqTextField,
} from "biblioteca-portal";
import React, { useState } from "react";

interface IFormSolicitante {
  areaState: string | undefined;
  publicoAlvoState: string | undefined;
  changePublicoAlvo: (publico: string) => void;
  outrosState: string | undefined;
  changeOutros: (outros: string) => void;
  changeArea: (area: string) => void;
  error: boolean;
}

interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const FormSolicitante = ({
  publicoAlvoState,
  changePublicoAlvo,
  outrosState,
  changeOutros,
  changeArea,
  error,
  areaState,
}: IFormSolicitante) => {
  const [area, setArea] = useState("");
  const [selectedArea, setSelectedArea] = useState<ISelect>();

  const onchangePublicoAlvo = (text: string): void => {
    changePublicoAlvo(text);
  };
  const onchangeOutros = (text: string): void => {
    changeOutros(text);
  };
  const onChangeArea = (value: ISelect): void => {
    changeArea(value.label);
    setArea(value.label);
  };

  React.useEffect(() => {
    if (areaState) {
      setSelectedArea({
        value: areaState,
        label: areaState,
      });
    } else {
      setSelectedArea(undefined);
    }
  }, [areaState]);
  return (
    <ArqInformationCard>
      <ArqText
        text="Informações Solicitante/Público"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginTop: 20,
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqSelectInput
            label="Área"
            options={[
              { value: 1, label: "Arquitetura Delivery" },
              { value: 2, label: "Arquitetura Da Informação" },
              { value: 2, label: "Arquitetura Corporativa" },
            ]}
            onChange={onChangeArea}
            emptyField={area === "" && error}
            selectedOption={selectedArea}
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="Público Alvo"
            placeholder="Nome Público Alvo"
            value={publicoAlvoState}
            onChange={onchangePublicoAlvo}
            emptyField={
              error &&
              (publicoAlvoState === undefined || publicoAlvoState === "")
            }
          />
        </div>
      </div>

      <div style={{ marginBottom: 20, marginTop: 20 }}>
        <ArqTextField
          label="Outros"
          placeholder="Outros"
          value={outrosState}
          onChange={onchangeOutros}
        />
      </div>
    </ArqInformationCard>
  );
};
