import {
  ArqInformationCard,
  ArqSelectInput,
  ArqText,
  ArqTextField,
  ArqTextFieldMultiline,
} from "biblioteca-portal";
import React, { useState } from "react";

interface IFormComunicado {
  tipoComunicadoState: string | undefined;
  tituloComunicadoState: string | undefined;
  changeTituloComunicado: (nome: string) => void;
  textoComunicadoState: string | undefined;
  changeTextoComunicado: (outros: string) => void;
  anexoState: string | undefined;
  changeTipoComunicado: (tipo: string) => void;
  error: boolean;
}

interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const FormComunicado = ({
  tituloComunicadoState,
  changeTituloComunicado,
  textoComunicadoState,
  changeTextoComunicado,
  changeTipoComunicado,
  error,
  tipoComunicadoState,
}: IFormComunicado) => {
  const [tipo, setTipo] = useState("");
  const [selectedArea, setSelectedArea] = useState<ISelect>();

  const onchangeTituloComunicado = (text: string): void => {
    changeTituloComunicado(text);
  };
  const onchangeTextoComunicado = (text: string): void => {
    changeTextoComunicado(text);
  };
  const onchangeTipoComunicado = (value: ISelect): void => {
    changeTipoComunicado(value.label);
    setTipo(value.label);
  };

  React.useEffect(() => {
    if (tipoComunicadoState) {
      setSelectedArea({
        value: tipoComunicadoState,
        label: tipoComunicadoState,
      });
    } else {
      setSelectedArea(undefined);
    }
  }, [tipoComunicadoState]);
  return (
    <ArqInformationCard>
      <ArqText
        text="Informações Comunicado"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div style={{ marginBottom: 20, marginTop: 20 }}>
        <ArqSelectInput
          label="Tipo de Comunicado"
          options={[
            { value: "Comunicado", label: "Comunicado" },
            { value: "Arquitetura Quinzenal", label: "Arquitetura Quinzenal" },
            { value: "Arquitetura Em Foco", label: "Arquitetura Em Foco" },
          ]}
          onChange={onchangeTipoComunicado}
          emptyField={tipo === "" && error}
          selectedOption={selectedArea}
        />
      </div>
      <div style={{ marginBottom: 20 }}>
        <ArqTextField
          label="Título"
          placeholder="Título do Comunicado"
          onChange={onchangeTituloComunicado}
          value={tituloComunicadoState}
          emptyField={
            error &&
            (tituloComunicadoState === undefined ||
              tituloComunicadoState === "")
          }
        />
      </div>
      <div style={{ marginBottom: 20, width: "95%" }}>
        <ArqTextFieldMultiline
          label="Texto"
          placeholder="Texto do Comunicado"
          value={textoComunicadoState}
          onChange={onchangeTextoComunicado}
          emptyField={
            error &&
            (textoComunicadoState === undefined || textoComunicadoState === "")
          }
        />
      </div>
    </ArqInformationCard>
  );
};
