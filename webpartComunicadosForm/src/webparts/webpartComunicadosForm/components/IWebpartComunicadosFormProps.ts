import { SPHttpClient } from "@microsoft/sp-http";

export interface IWebpartComunicadosFormProps {
  description: string;
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
  spHttpClient: SPHttpClient
}
