/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import {
  ArqInformationCard,
  ArqSelectInput,
  ArqText,
  ArqTextField,
  ArqUploadFolder,
} from "biblioteca-portal";
import React, { useState } from "react";

interface IFormContato {
  contatoState: string | undefined;
  areaResponsavelState: string | undefined;
  changeFiles: (files: any) => void;
  gdAprovacaoState: string | undefined;
  changeGD: (nome: string) => void;
  changeAreaResponsavel: (area: string) => void;
  changeContato: (contato: string) => void;
  error: boolean;
}

interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

export const FormContato = ({
  changeFiles,
  gdAprovacaoState,
  changeGD,
  changeAreaResponsavel,
  changeContato,
  error,
  contatoState,
  areaResponsavelState,
}: IFormContato) => {
  const [area, setArea] = useState("");
  const [contato, setContato] = useState("");
  const [files, setFiles] = React.useState<any>();
  const [selectedContato, setSelectedContato] = React.useState<ISelect>();
  const [selectedArea, setSelectedArea] = React.useState<ISelect>();

  React.useEffect(() => {
    if (files && files.length > 0) {
      console.log("FILES: ", files);

      changeFiles(files);
    }
  }, [files]);

  const onchangeAreaResponsavel = (value: ISelect): void => {
    changeAreaResponsavel(value.label);
    setArea(value.label);
  };
  const onchangeContato = (value: ISelect): void => {
    changeContato(value.label);
    setContato(value.label);
  };
  const onchangeGD = (text: string): void => {
    changeGD(text);
  };

  React.useEffect(() => {
    if (contatoState) {
      setSelectedContato({ value: contatoState, label: contatoState });
    } else {
      setSelectedContato(undefined);
    }
  }, [contatoState]);
  React.useEffect(() => {
    if (areaResponsavelState) {
      setSelectedArea({
        value: areaResponsavelState,
        label: areaResponsavelState,
      });
    } else {
      setSelectedArea(undefined);
    }
  }, [areaResponsavelState]);

  return (
    <ArqInformationCard>
      <ArqText
        text="Informações Contato"
        fontSize="18px"
        fontWeight="bold"
        color="#484848"
      />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginTop: 20,
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqSelectInput
            label="Área Responsável"
            options={[
              { value: 1, label: "Arquitetura Delivery" },
              { value: 2, label: "Arquitetura Da Informação" },
              { value: 2, label: "Arquitetura Corporativa" },
            ]}
            onChange={onchangeAreaResponsavel}
            emptyField={area === "" && error}
            selectedOption={selectedArea}
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqSelectInput
            label="Contato"
            options={[
              { value: 1, label: "E-mail 1" },
              { value: 2, label: "E-mail 2" },
              { value: 2, label: "E-mail 3" },
              { value: 2, label: "E-mail 4" },
              { value: 2, label: "E-mail 5" },
            ]}
            onChange={onchangeContato}
            emptyField={contato === "" && error}
            selectedOption={selectedContato}
          />
        </div>
      </div>

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          marginTop: 20,
          marginBottom: 20,
        }}
      >
        <div style={{ width: "49%" }}>
          <ArqTextField
            label="GD"
            placeholder="GD para Aprovação"
            value={gdAprovacaoState}
            onChange={onchangeGD}
            emptyField={
              error &&
              (gdAprovacaoState === undefined || gdAprovacaoState === "")
            }
          />
        </div>
        <div style={{ width: "49%" }}>
          <ArqUploadFolder
            files={files}
            setFiles={setFiles}
            plusIcon={true}
            color={"#cacaca"}
            hoverColor="#cacaca"
            label="Anexo"
          />
        </div>
      </div>
    </ArqInformationCard>
  );
};
