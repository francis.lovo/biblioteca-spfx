import * as React from "react";
import { IWebpartOrcamentoProps } from "./IWebpartOrcamentoProps";
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";

export default class WebpartOrcamento extends React.Component<
  IWebpartOrcamentoProps,
  {}
> {
  public render(): React.ReactElement<IWebpartOrcamentoProps> {
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb
            items={["Gestão", "Orçamentos e Contratos"]}
            links={["/", "/SitePages/Gestao.aspx"]}
          />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Orçamentos e Contratos"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
      </div>
    );
  }
}
