declare interface IWebpartArquiteturaDeliveryIndicadoresWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AppLocalEnvironmentSharePoint: string;
  AppLocalEnvironmentTeams: string;
  AppLocalEnvironmentOffice: string;
  AppLocalEnvironmentOutlook: string;
  AppSharePointEnvironment: string;
  AppTeamsTabEnvironment: string;
  AppOfficeEnvironment: string;
  AppOutlookEnvironment: string;
}

declare module 'WebpartArquiteturaDeliveryIndicadoresWebPartStrings' {
  const strings: IWebpartArquiteturaDeliveryIndicadoresWebPartStrings;
  export = strings;
}
