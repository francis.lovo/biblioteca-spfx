/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import moment from "moment";
import { IWebpartArquiteturaDeliveryIndicadoresProps } from "./IWebpartArquiteturaDeliveryIndicadoresProps";
import {
  ArqRadioButton,
  ArqText,
  ArqSwitch,
  ArqDatePicker,
  ArqButton,
  ArqInformationCard,
  ArqSelectInput,
  ArqNewBarChart,
} from "biblioteca-portal";
import "./style.css";
import * as XLSX from "xlsx";

interface MyState {
  button: number;
  status: boolean;
  data: boolean;
  trimestre: boolean;
  arquiteto: boolean;
  frente: boolean;
  isOn: boolean;
  selectedDisabled: boolean;
  selectContratacao: boolean;
  selectAtuacao: boolean;
  selectConcluido: boolean;
  valueDatainicial: string | undefined;
  valueDataFinal: string | undefined;
  tipoGrafico: number;
  arquitetoSelect: number;
  trimestreSelect: number;
  chartData: any[];
  gestores: string[];
  todasFrentes: string[];
  selectedChart: any[];
  tableItems: any[];
  tableHeaders: string[];
  todosFornecedores: string[];
  arquitetoContratado: string[];
  filtros: any;
  tabelaNames: string[];
  tabelaNamesIndex: number;
  tabelaItens: any[];
  tabelaItensIndex: number;
  gestoresNomes: string[];
  todosStatus: string[];
  selectedFrente: ISelect;
  meses: any[];
}

interface IFiltros {
  tipo: "projeto" | "horas";
  status?: number[];
  dataInicio?: string;
  dataFinal?: string;
  frente?: string;
  arquiteto?: "lider" | "contratado";
  trimestre?: "todos" | 1 | 2 | 3 | 4;
}
interface ISelect {
  value: string | number;
  label: string;
  disabled?: boolean;
}

export default class WebpartArquiteturaDeliveryIndicadores extends React.Component<
  IWebpartArquiteturaDeliveryIndicadoresProps,
  MyState
> {
  state: MyState = {
    button: 0,
    status: true,
    data: true,
    trimestre: false,
    arquiteto: false,
    frente: false,
    isOn: true,
    selectedDisabled: false,
    selectContratacao: false,
    selectAtuacao: false,
    selectConcluido: false,
    valueDatainicial: undefined,
    valueDataFinal: undefined,
    tipoGrafico: 0,
    arquitetoSelect: 0,
    trimestreSelect: 0,
    chartData: [],
    gestores: [],
    todasFrentes: [],
    selectedChart: [],
    tableItems: [],
    tableHeaders: [],
    todosFornecedores: [],
    arquitetoContratado: [],
    filtros: {},
    tabelaNames: [],
    tabelaNamesIndex: 8,
    tabelaItens: [],
    tabelaItensIndex: 2,
    gestoresNomes: [],
    todosStatus: [],
    selectedFrente: { value: "Delivery", label: "DELIVERY" },
    meses: [
      ["1° Trimestre", "2° Trimestre", "3° Trimestre", "4° Trimestre"],
      ["Janeiro", "Fevereiro", "Março"],
      ["Abril", "Maio", "Junho"],
      ["Julho", "Agosto", "Setembro"],
      ["Outubro", "Novembro", "Dezembro"],
    ],
  };
  componentDidMount(): void {
    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    const getUserInfoById = async () => {
      const apiUrl =
        "https://bancobradesco.sharepoint.com/teams/5800-pa/Delivery/Indicadores.xlsx";

      try {
        const response = await fetch(apiUrl, {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        });

        if (response.ok) {
          const arrayBuffer = await response.arrayBuffer(); // tem que fazer isso pra processar o arquivo XLSX
          const data = new Uint8Array(arrayBuffer);
          const workbook = XLSX.read(data, { type: "array" }); // pra ler e pegar as planilhas do arquivo

          const planilhaX = workbook.Sheets[workbook.SheetNames[0]]; // pra acessar a planilha desejada do arquivo

          const jsonData: any[] = XLSX.utils.sheet_to_json(planilhaX); // converter dados da planilha em json
          const aux: any[] = [];
          jsonData.forEach((data: any) => {
            console.log(
              "DATA: ",
              data[
                "Arquitetos,Id,FRENTE,Número da PT,Status,Resumo,Fornecedor,Quantidade de Horas,Gestor Solicitante,Arquiteto,Data de Iní­cio do Projeto,Data de Término do Projeto,Possui Termo de Iní­cio?,Data de Criação,Número PPMC,Data de Modificação do Estado,Arquiteto Lí­der,Arquiteto Contratado,Data Atualização de Contr.,Data Sugerida Onboarding,Data Prevista Onboarding,Data de Onboarding,QIA,Data Sugerida,Data Prevista,Data Realizada,VM,Data Sugerida,Data Prevista,Data Realizada,MA,Data Sugerida,Data Prevista,Data Realizada,Observação"
              ].split(",")
            );
            aux.push(
              data[
                "Arquitetos,Id,FRENTE,Número da PT,Status,Resumo,Fornecedor,Quantidade de Horas,Gestor Solicitante,Arquiteto,Data de Iní­cio do Projeto,Data de Término do Projeto,Possui Termo de Iní­cio?,Data de Criação,Número PPMC,Data de Modificação do Estado,Arquiteto Lí­der,Arquiteto Contratado,Data Atualização de Contr.,Data Sugerida Onboarding,Data Prevista Onboarding,Data de Onboarding,QIA,Data Sugerida,Data Prevista,Data Realizada,VM,Data Sugerida,Data Prevista,Data Realizada,MA,Data Sugerida,Data Prevista,Data Realizada,Observação"
              ].split(",")
            );
          });
          this.setState({ chartData: aux });
        } else {
          console.error("Falha na requisição");
          return null;
        }
      } catch (error) {
        console.error("Erro na requisição: ", error);
        return null;
      }
    };
    // eslint-disable-next-line no-void
    void getUserInfoById();
  }

  calcularTotalTabela(tableItems: any): string[] {
    const totalRow: string[] = ["Total"];
    for (let col = 1; col < tableItems[0].length; col++) {
      let sum = 0;
      for (let rowIndex = 0; rowIndex < tableItems.length; rowIndex++) {
        sum += tableItems[rowIndex][col];
      }
      totalRow.push(sum.toString());
    }
    return totalRow;
  }
  montarHeaderTabela(headerItems: string[]): string[] {
    const tableHeaderAux: string[] = [];
    if (this.state.button === 0) {
      tableHeaderAux.push("Projetos");
    } else if (this.state.button === 1) {
      tableHeaderAux.push("Fornecedores");
    } else if (this.state.button === 2) {
      tableHeaderAux.push("Situação");
    } else if (this.state.button === 3) {
      tableHeaderAux.push("Arquiteto");
    } else if (this.state.button === 4) {
      tableHeaderAux.push("Contratações");
    }

    headerItems.forEach((header: string) => {
      tableHeaderAux.push(header);
    });
    tableHeaderAux.push("Total");

    return tableHeaderAux;
  }
  passaFiltro(data: any[]): boolean {
    if (
      this.state.filtros &&
      this.state.filtros.dataInicio &&
      this.state.filtros.dataInicio !== ""
    ) {
      if (
        moment(data[11].split(" ")[0], "MM/DD/YYYY").isBefore(
          moment(this.state.filtros.dataInicio, "YYYY-MM-DD").format(
            "MM/DD/YYYY"
          )
        )
      ) {
        return false;
      }
    }
    if (
      this.state.filtros &&
      this.state.filtros.dataFinal &&
      this.state.filtros.dataFinal !== ""
    ) {
      if (
        moment(data[11].split(" ")[0], "MM/DD/YYYY").isAfter(
          moment(this.state.filtros.dataFinal, "YYYY-MM-DD").format(
            "MM/DD/YYYY"
          )
        )
      ) {
        return false;
      }
    }
    if (
      this.state.filtros &&
      this.state.filtros.frente &&
      this.state.button === 2 &&
      this.state.filtros.frente === "DELIVERY"
    ) {
      if (data[2] !== "DELIVERY") {
        return false;
      }
    }
    if (
      this.state.filtros &&
      this.state.filtros.frente &&
      this.state.button === 2 &&
      this.state.filtros.frente === "INFORMAÇÃO"
    ) {
      if (data[2] !== "INFORMAÇÃO") {
        return false;
      }
    }
    if (
      this.state.button !== 2 &&
      this.state.filtros &&
      this.state.filtros.status &&
      this.state.filtros.status.length > 0
    ) {
      if (
        !this.state.filtros.status.includes(0) &&
        data[4].split(" - ")[1] === "Contratação"
      ) {
        return false;
      }
      if (
        !this.state.filtros.status.includes(1) &&
        data[4].split(" - ")[1] === "Formalizado"
      ) {
        return false;
      }
      if (
        !this.state.filtros.status.includes(2) &&
        data[4].split(" - ")[1] === "Concluído"
      ) {
        return false;
      }
    }
    return true;
  }
  montarTabela(
    lineNames: string[],
    lineItems: string[],
    nameIndex: number,
    itemsIndex: number
  ): any[] {
    const tableAux: any[] = [];

    lineNames.forEach((gestor: string) => {
      const objetoTable: any[] = [];
      objetoTable.push(gestor);
      let countTotal = 0;
      lineItems.forEach((frente: string) => {
        let count = 0;
        this.state.chartData.forEach((data: any[]) => {
          if (itemsIndex >= 0) {
            if (
              data[nameIndex] === gestor &&
              data[itemsIndex] === frente &&
              this.passaFiltro(data)
            ) {
              if (this.state.filtros && this.state.filtros.tipo === "horas") {
                count += +data[7];
                countTotal += +data[7];
              } else {
                count++;
                countTotal++;
              }
            }
          } else {
            if (lineItems === this.state.meses[0] && this.passaFiltro(data)) {
              //todos
              //primeiro trimestre
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][0]
              ) {
                if (
                  data[11].split("/")[0] === "1" ||
                  data[11].split("/")[0] === "2" ||
                  data[11].split("/")[0] === "3"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }

              //segundo trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][1]
              ) {
                if (
                  data[11].split("/")[0] === "4" ||
                  data[11].split("/")[0] === "5" ||
                  data[11].split("/")[0] === "6"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //terceiro trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][2]
              ) {
                if (
                  data[11].split("/")[0] === "7" ||
                  data[11].split("/")[0] === "8" ||
                  data[11].split("/")[0] === "9"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //quarto trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][3]
              ) {
                if (
                  data[11].split("/")[0] === "10" ||
                  data[11].split("/")[0] === "11" ||
                  data[11].split("/")[0] === "12"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              lineItems === this.state.meses[1] &&
              this.passaFiltro(data)
            ) {
              //1° primeiro trimestre
              //janeiro
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][0]
              ) {
                if (data[11].split("/")[0] === "1") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //fevereiro
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][1]
              ) {
                if (data[11].split("/")[0] === "2") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //março
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][2]
              ) {
                if (data[11].split("/")[0] === "3") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              lineItems === this.state.meses[2] &&
              this.passaFiltro(data)
            ) {
              //2° primeiro trimestre
              //abril
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][0]
              ) {
                if (data[11].split("/")[0] === "4") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //maio
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][1]
              ) {
                if (data[11].split("/")[0] === "5") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //junho
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][2]
              ) {
                if (data[11].split("/")[0] === "6") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              lineItems === this.state.meses[3] &&
              this.passaFiltro(data)
            ) {
              //3° primeiro trimestre
              //julho
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][0]
              ) {
                if (data[11].split("/")[0] === "7") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //agosto
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][1]
              ) {
                if (data[11].split("/")[0] === "8") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //setembro
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][2]
              ) {
                if (data[11].split("/")[0] === "9") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else {
              if (this.passaFiltro(data)) {
                //4° trimestre
                //outubro
                if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][0]
                ) {
                  if (data[11].split("/")[0] === "10") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                }
                //novembro
                else if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][1]
                ) {
                  if (data[11].split("/")[0] === "11") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                } //dezembro
                else if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][2]
                ) {
                  if (data[11].split("/")[0] === "12") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                }
              }
            }
          }
        });
        objetoTable.push(count);
      });
      if (countTotal > 0) {
        objetoTable.push(countTotal);
        tableAux.push(objetoTable);
      }
    });
    if (tableAux.length > 0) {
      const totalRow = this.calcularTotalTabela(tableAux);
      tableAux.push(totalRow);
    }

    return tableAux;
  }
  guardarColunaExcel(index: number): any[] {
    const aux: string[] = [];
    this.state.chartData.forEach((data: any[]) => {
      if (aux.length === 0) {
        aux.push(data[index]);
      } else {
        let achou = false;
        aux.forEach((jaGuardado: string) => {
          if (jaGuardado === data[index]) {
            achou = true;
          }
        });
        if (!achou) {
          aux.push(data[index]);
        }
      }
    });
    return aux;
  }
  montarGrafico(
    nameLine: string[],
    itemLine: string[],
    nameIndex: number,
    itemIndex: number
  ): any[] {
    const chartAux: any[] = [];
    nameLine.forEach((gestor: string) => {
      //montar objeto
      let countTotal = 0;
      const objetoAux: any = { name: gestor };
      itemLine.forEach((frente: string) => {
        let count = 0;
        this.state.chartData.forEach((data: any[]) => {
          if (itemIndex >= 0) {
            if (
              data[nameIndex] === gestor &&
              data[itemIndex] === frente &&
              this.passaFiltro(data)
            ) {
              if (this.state.filtros && this.state.filtros.tipo === "horas") {
                count += +data[7];
                countTotal += data[7];
              } else {
                count++;
                countTotal++;
              }
            }
          } else {
            if (itemLine === this.state.meses[0] && this.passaFiltro(data)) {
              //todos
              //primeiro trimestre
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][0]
              ) {
                if (
                  data[11].split("/")[0] === "1" ||
                  data[11].split("/")[0] === "2" ||
                  data[11].split("/")[0] === "3"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }

              //segundo trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][1]
              ) {
                if (
                  data[11].split("/")[0] === "4" ||
                  data[11].split("/")[0] === "5" ||
                  data[11].split("/")[0] === "6"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //terceiro trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][2]
              ) {
                if (
                  data[11].split("/")[0] === "7" ||
                  data[11].split("/")[0] === "8" ||
                  data[11].split("/")[0] === "9"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //quarto trimestre
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[0][3]
              ) {
                if (
                  data[11].split("/")[0] === "10" ||
                  data[11].split("/")[0] === "11" ||
                  data[11].split("/")[0] === "12"
                ) {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              itemLine === this.state.meses[1] &&
              this.passaFiltro(data)
            ) {
              //1° primeiro trimestre
              //janeiro
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][0]
              ) {
                if (data[11].split("/")[0] === "1") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //fevereiro
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][1]
              ) {
                if (data[11].split("/")[0] === "2") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //março
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[1][2]
              ) {
                if (data[11].split("/")[0] === "3") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              itemLine === this.state.meses[2] &&
              this.passaFiltro(data)
            ) {
              //2° primeiro trimestre
              //abril
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][0]
              ) {
                if (data[11].split("/")[0] === "4") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //maio
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][1]
              ) {
                if (data[11].split("/")[0] === "5") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //junho
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[2][2]
              ) {
                if (data[11].split("/")[0] === "6") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else if (
              itemLine === this.state.meses[3] &&
              this.passaFiltro(data)
            ) {
              //3° primeiro trimestre
              //julho
              if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][0]
              ) {
                if (data[11].split("/")[0] === "7") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
              //agosto
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][1]
              ) {
                if (data[11].split("/")[0] === "8") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              } //setembro
              else if (
                data[nameIndex] === gestor &&
                frente === this.state.meses[3][2]
              ) {
                if (data[11].split("/")[0] === "9") {
                  if (
                    this.state.filtros &&
                    this.state.filtros.tipo === "horas"
                  ) {
                    count += +data[7];
                    countTotal += +data[7];
                  } else {
                    count++;
                    countTotal++;
                  }
                }
              }
            } else {
              if (this.passaFiltro(data)) {
                //4° trimestre
                //outubro
                if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][0]
                ) {
                  if (data[11].split("/")[0] === "10") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                }
                //novembro
                else if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][1]
                ) {
                  if (data[11].split("/")[0] === "11") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                } //dezembro
                else if (
                  data[nameIndex] === gestor &&
                  frente === this.state.meses[4][2]
                ) {
                  if (data[11].split("/")[0] === "12") {
                    if (
                      this.state.filtros &&
                      this.state.filtros.tipo === "horas"
                    ) {
                      count += +data[7];
                      countTotal += +data[7];
                    } else {
                      count++;
                      countTotal++;
                    }
                  }
                }
              }
            }
          }
        });
        objetoAux[frente] = count;
      });
      if (countTotal > 0) {
        chartAux.push(objetoAux);
      }
    });
    return chartAux;
  }
  armazenarTodosNecessariosExcel(): void {
    const gestoresAux: string[] = this.guardarColunaExcel(8);
    const frenteAux: any[] = this.guardarColunaExcel(2);
    const fornecedoresAux: string[] = this.guardarColunaExcel(6);
    const arquitetoContratadoAux: string[] = this.guardarColunaExcel(17);
    const gestoresNomesAux: string[] = this.guardarColunaExcel(0);
    const todosStatusAux: string[] = this.guardarColunaExcel(4);
    this.setState({
      gestores: gestoresAux,
      todasFrentes: frenteAux,
      todosFornecedores: fornecedoresAux,
      arquitetoContratado: arquitetoContratadoAux,
      tabelaItens: frenteAux,
      tabelaNames: gestoresAux,
      gestoresNomes: gestoresNomesAux,
      todosStatus: todosStatusAux,
    });
  }
  montarTabelaPadrao(): void {
    //montar tabela
    const tableHeaderAux = this.montarHeaderTabela(this.state.tabelaItens);
    const tableAux: any[] = this.montarTabela(
      this.state.tabelaNames,
      this.state.tabelaItens,
      this.state.tabelaNamesIndex,
      this.state.tabelaItensIndex
    );
    this.setState({
      tableHeaders: tableHeaderAux,
      tableItems: tableAux,
    });

    //montar grafico
    const chartAux: any[] = this.montarGrafico(
      this.state.tabelaNames,
      this.state.tabelaItens,
      this.state.tabelaNamesIndex,
      this.state.tabelaItensIndex
    );

    this.setState({
      selectedChart: chartAux,
    });
  }

  iniciarFiltros(): void {
    const filtroAtual: IFiltros = {
      tipo: "projeto",
      status: [0, 1, 2],
      dataInicio: "",
      dataFinal: "",
    };
    this.setState({ filtros: filtroAtual });
  }

  componentDidUpdate(
    prevProps: Readonly<IWebpartArquiteturaDeliveryIndicadoresProps>,
    prevState: Readonly<MyState>,
    snapshot?: any
  ): void {
    if (
      prevState.chartData.length === 0 &&
      prevState.chartData !== this.state.chartData
    ) {
      //armazenar dados do excel
      this.armazenarTodosNecessariosExcel();
      this.iniciarFiltros();
    }
    if (
      prevState.tabelaItens !== this.state.tabelaItens ||
      prevState.tabelaNames !== this.state.tabelaNames ||
      prevState.tabelaItensIndex !== this.state.tabelaItensIndex ||
      prevState.tabelaNamesIndex !== this.state.tabelaNamesIndex
    ) {
      this.montarTabelaPadrao();
    }
  }
  public render(): React.ReactElement<IWebpartArquiteturaDeliveryIndicadoresProps> {
    const chartData = [
      {
        name: "Projetos",
      },
      {
        name: "Fornecedores",
      },
      {
        name: "Situação",
      },
      {
        name: "Arquiteto",
      },
      {
        name: "Contratações",
      },
    ];

    const topLabel = [
      "Projetos X Vertical",
      "Projetos X Fornecedor",
      "Projetos X Situação",
      "Projetos X Arquiteto Líder",
      "Contratações X Trimestre",
      "Contratações X Mês",
      "Projetos X Arquiteto Contratado",
    ];
    const topLabelsQuant = [
      "Horas de Arquitetura X Vertical",
      "Horas de Arquitetura X Fornecedor",
      "Horas de Arquitetura X Situação",
      "Horas de Arquitetura X Arquiteto Líder",
      "Horas de Arquitetura X Trimestre",
      "Horas de Arquitetura X Mês",
      "Horas de Arquitetura X Arquiteto Contratado",
    ];
    const mudarAbaParaFornecedores = (): void => {
      this.setState({
        tabelaItens: this.state.todasFrentes,
        tabelaNames: this.state.todosFornecedores,
        tabelaNamesIndex: 6,
        tabelaItensIndex: 2,
      });
    };
    const mudarParaAbaProjetos = (): void => {
      this.setState({
        tabelaItens: this.state.todasFrentes,
        tabelaNames: this.state.gestores,
        tabelaNamesIndex: 8,
        tabelaItensIndex: 2,
      });
    };
    const mudarAbaParaArquiteto = (): void => {
      let names = 0;
      if (this.state.filtros && this.state.filtros.arquiteto === "contratado") {
        names = 17;
      }
      this.setState({
        tabelaItens: this.state.todasFrentes,
        tabelaNames:
          names === 0
            ? this.state.gestoresNomes
            : this.state.arquitetoContratado,
        tabelaNamesIndex: names,
        tabelaItensIndex: 2,
      });
    };
    const mudarAbaParaSituacao = (): void => {
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.frente = this.state.selectedFrente.label;
      this.setState({
        tabelaItens: this.state.todosStatus,
        tabelaNames: this.state.gestores,
        tabelaNamesIndex: 8,
        tabelaItensIndex: 4,
        filtros: filtroAux,
      });
    };
    const setarSelectFrente = (value: ISelect): void => {
      this.setState({ selectedFrente: value });
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.frente = value.label;
      this.setState({
        filtros: filtroAux,
      });
      this.montarTabelaPadrao();
    };
    const mudarAbaParaContratacoes = (): void => {
      this.setState({
        tabelaItens: this.state.meses[this.state.trimestreSelect],
        tabelaNames: this.state.gestores,
        tabelaNamesIndex: 8,
        tabelaItensIndex: -1,
      });
    };
    const clickButton = (index: number): void => {
      if (index === 0) {
        mudarParaAbaProjetos();
      }

      if (index === 1) {
        mudarAbaParaFornecedores();
      }

      if (index === 2) {
        this.setState({ status: false, frente: true });
        mudarAbaParaSituacao();
      } else if (index !== 2 && !this.state.status) {
        this.setState({ status: true, frente: false });
      }

      if (index === 3) {
        this.setState({ arquiteto: true });
        mudarAbaParaArquiteto();
      } else if (index !== 3 && this.state.arquiteto) {
        this.setState({ arquiteto: false });
      }

      if (index === 4) {
        this.setState({ data: false, trimestre: true });
        mudarAbaParaContratacoes();
      } else if (index !== 4 && !this.state.data) {
        this.setState({ data: true, trimestre: false });
      }

      this.setState({ button: index });
    };
    const resetarCampos = (): void => {
      this.setState({
        isOn: !this.state.isOn,
        valueDatainicial: undefined,
        valueDataFinal: undefined,
      });
      const filtrosAux: IFiltros = this.state.filtros;
      filtrosAux.dataFinal = "";
      filtrosAux.dataInicio = "";
      filtrosAux.status = [0, 1, 2];
      this.setState({ filtros: filtrosAux });
      this.montarTabelaPadrao();
    };

    const carregarFiltrosStatus = (
      contratacao: boolean,
      atuacao: boolean,
      concluido: boolean
    ): void => {
      const aux: number[] = [];

      if (!contratacao) {
        aux.push(0);
      }
      if (!atuacao) {
        aux.push(1);
      }
      if (!concluido) {
        aux.push(2);
      }
      const fitrosAux: IFiltros = this.state.filtros;
      fitrosAux.status = aux;
      this.setState({ filtros: fitrosAux });
      this.montarTabelaPadrao();
    };
    const mudarSwitchContratacao = (value: boolean): void => {
      this.setState({ selectContratacao: value });
      carregarFiltrosStatus(
        value,
        this.state.selectAtuacao,
        this.state.selectConcluido
      );
      if (value && (this.state.selectConcluido || this.state.selectAtuacao)) {
        this.setState({ selectedDisabled: true });
      }
      if (
        !value &&
        (!this.state.selectConcluido || !this.state.selectAtuacao)
      ) {
        this.setState({ selectedDisabled: false });
      }
    };
    const mudarSwitchAtuacao = (value: boolean): void => {
      this.setState({ selectAtuacao: value });
      carregarFiltrosStatus(
        this.state.selectContratacao,
        value,
        this.state.selectConcluido
      );
      if (
        value &&
        (this.state.selectConcluido || this.state.selectContratacao)
      ) {
        this.setState({ selectedDisabled: true });
      }
      if (
        !value &&
        (!this.state.selectConcluido || !this.state.selectContratacao)
      ) {
        this.setState({ selectedDisabled: false });
      }
    };
    const mudarSwitchConcluido = (value: boolean): void => {
      this.setState({ selectConcluido: value });
      carregarFiltrosStatus(
        this.state.selectContratacao,
        this.state.selectAtuacao,
        value
      );
      if (value && (this.state.selectAtuacao || this.state.selectContratacao)) {
        this.setState({ selectedDisabled: true });
      }
      if (
        !value &&
        (!this.state.selectAtuacao || !this.state.selectContratacao)
      ) {
        this.setState({ selectedDisabled: false });
      }
    };
    const mudarDataInicial = (value: string): void => {
      if (value) {
        this.setState({ valueDatainicial: value });
        const filtroAux: IFiltros = this.state.filtros;
        filtroAux.dataInicio = value;
        this.setState({ filtros: filtroAux });
      } else if (!value || value === "") {
        const filtroAux: IFiltros = this.state.filtros;
        filtroAux.dataInicio = undefined;
        this.setState({ filtros: filtroAux });
      }

      this.montarTabelaPadrao();
    };
    const mudarDataFinal = (value: string): void => {
      if (value) {
        this.setState({ valueDataFinal: value });
        const filtroAux: IFiltros = this.state.filtros;
        filtroAux.dataFinal = value;
        this.setState({ filtros: {} });
        this.setState({ filtros: filtroAux });
      } else if (!value || value === "") {
        const filtroAux: IFiltros = this.state.filtros;
        filtroAux.dataFinal = undefined;
        this.setState({ filtros: filtroAux });
      }
      this.montarTabelaPadrao();
    };
    const mudarTipoParaHoras = (): void => {
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.tipo = "horas";
      this.setState({ filtros: {} });
      this.setState({
        tipoGrafico: 1,
        filtros: filtroAux,
      });
      this.montarTabelaPadrao();
    };
    const mudarTipoParaProjeto = (): void => {
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.tipo = "projeto";
      this.setState({ filtros: {} });
      this.setState({
        tipoGrafico: 0,
        filtros: filtroAux,
      });
      this.montarTabelaPadrao();
    };

    const mudarArquitetoParaLder = (): void => {
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.arquiteto = "lider";
      this.setState({ filtros: {} });
      this.setState({
        arquitetoSelect: 0,
        filtros: filtroAux,
        tabelaNamesIndex: 0,
        tabelaNames: this.state.gestoresNomes,
      });
      this.montarTabelaPadrao();
    };
    const mudarArquitetoParaContratado = (): void => {
      const filtroAux: IFiltros = this.state.filtros;
      filtroAux.arquiteto = "contratado";
      this.setState({ filtros: {} });
      this.setState({
        arquitetoSelect: 1,
        filtros: filtroAux,
        tabelaNamesIndex: 17,
        tabelaNames: this.state.arquitetoContratado,
      });
      this.montarTabelaPadrao();
    };

    return (
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          <div style={{ width: "30%" }}>
            <div
              style={{
                marginTop: "15px",
              }}
            >
              <ArqText text="Tipo Gráfico:" fontSize="26" fontWeight="bold" />
            </div>
            <div
              style={{
                marginTop: "15px",
              }}
            >
              <ArqRadioButton
                label={
                  this.state.button === 4
                    ? "Total Contratações"
                    : "Total Projeto"
                }
                value="totalProjeto"
                name="tipoGrafico"
                isSelected={this.state.tipoGrafico === 0}
                onChange={() => {
                  if (this.state.tipoGrafico !== 0) {
                    mudarTipoParaProjeto();
                  }
                }}
              />
            </div>
            <div
              style={{
                marginTop: "15px",
              }}
            >
              <ArqRadioButton
                label="Quantidade Horas Arquitetura"
                value="quantidadeHoras"
                name="tipoGrafico"
                isSelected={this.state.tipoGrafico === 1}
                onChange={() => {
                  if (this.state.tipoGrafico !== 1) {
                    mudarTipoParaHoras();
                  }
                }}
              />
            </div>
            {this.state.frente && (
              <div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <div
                    style={{
                      marginTop: "15px",
                    }}
                  >
                    <ArqText text="Frente:" fontSize="26" fontWeight="bold" />
                  </div>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <div
                    style={{
                      marginTop: "15px",
                    }}
                  >
                    <ArqSelectInput
                      options={[
                        { value: "Delivery", label: "DELIVERY" },
                        { value: "Informação", label: "INFORMAÇÃO" },
                      ]}
                      onChange={setarSelectFrente}
                      selectedOption={this.state.selectedFrente}
                    />
                  </div>
                </div>
              </div>
            )}

            <div style={!this.state.status ? { display: "none" } : {}}>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqText text="Status: " fontSize="26" fontWeight="bold" />
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqSwitch
                    isOn={this.state.isOn}
                    label="Contratação"
                    onChange={mudarSwitchContratacao}
                    disabled={this.state.selectedDisabled}
                  />
                </div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqSwitch
                    isOn={this.state.isOn}
                    label="Atuação"
                    onChange={mudarSwitchAtuacao}
                    disabled={this.state.selectedDisabled}
                  />
                </div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqSwitch
                    isOn={this.state.isOn}
                    label="Concluído"
                    onChange={mudarSwitchConcluido}
                    disabled={this.state.selectedDisabled}
                  />
                </div>
              </div>
            </div>
            {this.state.arquiteto && (
              <div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqText text="Arquiteto: " fontSize="26" fontWeight="bold" />
                </div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqRadioButton
                    label={"Líder"}
                    value="lider"
                    name="arquiteto"
                    isSelected={this.state.arquitetoSelect === 0}
                    onChange={() => {
                      if (this.state.arquitetoSelect !== 0) {
                        mudarArquitetoParaLder();
                      }
                    }}
                  />
                </div>
                <div
                  style={{
                    marginTop: "15px",
                  }}
                >
                  <ArqRadioButton
                    label="Contratado"
                    value="contratado"
                    name="arquiteto"
                    isSelected={this.state.arquitetoSelect === 1}
                    onChange={() => {
                      if (this.state.arquitetoSelect !== 1) {
                        mudarArquitetoParaContratado();
                      }
                    }}
                  />
                </div>
              </div>
            )}
            <div style={!this.state.data ? { display: "none" } : {}}>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqText
                  text="Data do Término do Projeto: "
                  fontSize="24"
                  fontWeight="bold"
                />
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                  marginTop: 15,
                }}
              >
                <ArqDatePicker
                  label="De:"
                  value={this.state.valueDatainicial}
                  onChange={mudarDataInicial}
                />
                <ArqDatePicker
                  label="Até:"
                  value={this.state.valueDataFinal}
                  onChange={mudarDataFinal}
                />
              </div>
            </div>
            <div style={this.state.trimestre ? {} : { display: "none" }}>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqText text="Trimestre" fontSize="26" fontWeight="bold" />
              </div>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqRadioButton
                  label={"Todos"}
                  value="todos"
                  name="trimestre"
                  isSelected={this.state.trimestreSelect === 0}
                  onChange={() => {
                    if (this.state.trimestreSelect !== 0) {
                      this.setState({
                        tabelaItens: this.state.meses[0],
                        tabelaNames: this.state.gestores,
                        tabelaNamesIndex: 8,
                        tabelaItensIndex: -1,
                        trimestreSelect: 0,
                      });
                    }
                  }}
                />
              </div>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqRadioButton
                  label="Janeiro - Março"
                  value="jan-mar"
                  name="trimestre"
                  isSelected={this.state.trimestreSelect === 1}
                  onChange={() => {
                    if (this.state.trimestreSelect !== 1) {
                      this.setState({
                        tabelaItens: this.state.meses[1],
                        tabelaNames: this.state.gestores,
                        tabelaNamesIndex: 8,
                        tabelaItensIndex: -1,
                        trimestreSelect: 1,
                      });
                    }
                  }}
                />
              </div>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqRadioButton
                  label={"Abril - Junho"}
                  value="abr-jun"
                  name="trimestre"
                  isSelected={this.state.trimestreSelect === 2}
                  onChange={() => {
                    if (this.state.trimestreSelect !== 2) {
                      this.setState({
                        tabelaItens: this.state.meses[2],
                        tabelaNames: this.state.gestores,
                        tabelaNamesIndex: 8,
                        tabelaItensIndex: -1,
                        trimestreSelect: 2,
                      });
                    }
                  }}
                />
              </div>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqRadioButton
                  label="Julho - Setembro"
                  value="jul-set"
                  name="trimestre"
                  isSelected={this.state.trimestreSelect === 3}
                  onChange={() => {
                    if (this.state.trimestreSelect !== 3) {
                      this.setState({
                        tabelaItens: this.state.meses[3],
                        tabelaNames: this.state.gestores,
                        tabelaNamesIndex: 8,
                        tabelaItensIndex: -1,
                        trimestreSelect: 3,
                      });
                    }
                  }}
                />
              </div>
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqRadioButton
                  label="Outubro - Dezembro"
                  value="out-dez"
                  name="trimestre"
                  isSelected={this.state.trimestreSelect === 4}
                  onChange={() => {
                    if (this.state.trimestreSelect !== 4) {
                      this.setState({
                        tabelaItens: this.state.meses[4],
                        tabelaNames: this.state.gestores,
                        tabelaNamesIndex: 8,
                        tabelaItensIndex: -1,
                        trimestreSelect: 4,
                      });
                    }
                  }}
                />
              </div>
            </div>

            {this.state.button !== 4 && (
              <div
                style={{
                  marginTop: "15px",
                }}
              >
                <ArqButton
                  text="Limpar"
                  color="#cc092f"
                  onClick={resetarCampos}
                />
              </div>
            )}
          </div>

          <div
            style={{
              width: "60%",
              marginLeft: "40px",
              display: "flex",
              flexDirection: "column",
            }}
          >
            <ArqInformationCard>
              <div style={{ padding: "20px" }}>
                <div
                  style={{
                    display: "flex",
                    gap: "20px",
                    marginBottom: "20px",
                    width: "100%",
                  }}
                >
                  {chartData.map((data, index) => {
                    return (
                      <div
                        key={index}
                        style={
                          this.state.button === index
                            ? {
                                border: "1px solid #cc092f",
                                borderRadius: "4px",
                                background: "#cc092f",
                                padding: "10px 20px",
                                cursor: "pointer",
                              }
                            : {
                                border: "1px solid #e8e9ec",
                                borderRadius: "4px",
                                padding: "10px 20px",
                                cursor: "pointer",
                              }
                        }
                        onClick={() => {
                          clickButton(index);
                        }}
                      >
                        <ArqText
                          text={data.name}
                          color={
                            this.state.button === index ? "white" : "#484848"
                          }
                          fontWeight="bold"
                        />
                      </div>
                    );
                  })}
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginBottom: "20px",
                  }}
                >
                  <ArqText
                    text={
                      this.state.tipoGrafico === 0
                        ? this.state.button === 3 &&
                          this.state.arquitetoSelect === 1
                          ? topLabel[6]
                          : this.state.button === 4 &&
                            this.state.trimestreSelect > 0
                          ? topLabel[5]
                          : topLabel[this.state.button]
                        : this.state.button === 3 &&
                          this.state.arquitetoSelect === 1
                        ? topLabelsQuant[6]
                        : this.state.button === 4 &&
                          this.state.trimestreSelect > 0
                        ? topLabelsQuant[5]
                        : topLabelsQuant[this.state.button]
                    }
                    fontWeight="bold"
                    color="#484848"
                    fontSize="18px"
                  />
                </div>
                {this.state.selectedChart &&
                  this.state.selectedChart.length > 0 && (
                    <ArqNewBarChart
                      width={600}
                      height={200}
                      data={this.state.selectedChart}
                      names={this.state.tabelaItens}
                      colors={["#6444bc", "#ff605e", "#123459", "#ad0a1d"]}
                    />
                  )}
                {(!this.state.selectedChart ||
                  this.state.selectedChart.length === 0) && (
                  <ArqText
                    text="Não temos dados para carregar o gráfico."
                    fontSize="24px"
                    color="#4d4e53"
                  />
                )}
              </div>
            </ArqInformationCard>

            <div
              style={{
                padding: "0px",
                borderRadius: "5px",
                marginTop: "30px",
              }}
            >
              {this.state.tableItems && this.state.tableItems.length > 0 && (
                <table style={{ width: "100%", borderCollapse: "collapse" }}>
                  <thead>
                    <tr>
                      <th
                        colSpan={this.state.tableHeaders.length}
                        style={{
                          color: "white",
                          backgroundColor: "#cc092f",
                          textAlign: "center",
                          padding: "10px",
                          borderRadius: "10px 10px 0 0",
                        }}
                      >
                        {topLabel[this.state.button]}
                      </th>
                    </tr>
                    <tr>
                      {this.state.tableHeaders &&
                        this.state.tableHeaders.length > 0 &&
                        this.state.tableHeaders.map((header, index) => {
                          return (
                            <th
                              style={{
                                textAlign: "left",
                                backgroundColor: "#f2f2f2",
                                color: "black",
                                fontWeight: "bold",
                                padding: "10px",
                              }}
                              key={index}
                            >
                              {header}
                            </th>
                          );
                        })}
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.tableItems &&
                      this.state.tableItems.length > 0 &&
                      this.state.tableItems.map((row, index) => (
                        <tr
                          key={index}
                          style={{
                            backgroundColor:
                              index % 2 === 0 ? "white" : "#f2f2f2",
                          }}
                        >
                          {row.map((cell: string, cellIndex: number) => (
                            <td
                              key={cellIndex}
                              style={{
                                padding: "10px",
                                borderRadius: "0",
                                color: "darkgrey",
                              }}
                            >
                              {cell}
                            </td>
                          ))}
                        </tr>
                      ))}
                  </tbody>
                </table>
              )}
              {(!this.state.tableItems ||
                this.state.tableItems.length === 0) && (
                <div
                  style={{
                    border: "1px solid #e8e9ec",
                    padding: 20,
                    borderRadius: 4,
                    boxSizing: "border-box",
                    width: "100%",
                  }}
                >
                  <ArqText
                    text="Não temos dados para carregar a tabela."
                    fontSize="24px"
                    color="#4d4e53"
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
