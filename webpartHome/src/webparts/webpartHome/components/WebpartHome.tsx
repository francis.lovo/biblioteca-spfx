import React from "react";
import { IWebpartHomeProps } from "./IWebpartHomeProps";
import { Notification } from "./Notification";
import { ArqMenuDropdown } from "biblioteca-portal";

export default class WebpartHome extends React.Component<
  IWebpartHomeProps,
  { isDropdownOpen: boolean }
> {
  state = {
    isDropdownOpen: false,
  };

  handleDropdownClick = (): void => {
    this.setState((prevState) => ({
      isDropdownOpen: !prevState.isDropdownOpen,
    }));
  };

  render(): React.ReactElement<IWebpartHomeProps> {
    return (
      <div>
        <div
          style={{
            width: "100%",
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              fontSize: "18px",
              paddingLeft: "10px",
            }}
          >
            <Notification />
            {/* Botão hamburguer */}
            <div
              onClick={this.handleDropdownClick}
              style={{
                marginLeft: "10px",
                cursor: "pointer",
                userSelect: "none",
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                fill="white"
                viewBox="0 0 16 16"
              >
                <path
                  d="M1 3h14v1H1zm0 5h14v1H1zm0 5h14v1H1z"
                  fillRule="evenodd"
                />
              </svg>
            </div>
          </div>
        </div>
        {/* Renderizar o componente ArqMenuDropdown */}
        <div
          style={{
            marginTop: this.state.isDropdownOpen ? "10px" : "-1000px",
            marginLeft: "-49px",
            background: "white",
            width: "108.5%",
            paddingBottom: "15px",
          }}
        >
          <ArqMenuDropdown
            items={[
              {
                name: "Página Inicial",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa",
              },
              {
                name: "Arquitetura Delivery",
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Arquitetura-Delivery.aspx",
                dropdownItems: [],
              },
              {
                name: "Arquitetura Corporativa",
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Arquitetura-Corporativa.aspx",
                dropdownItems: [],
              },
              {
                name: "Arquitetura da Informação",
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Arquitetura-da-Informa%C3%A7%C3%A3o.aspx",
                dropdownItems: [],
              },
              {
                name: "Comunicados",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Comunicados.aspx",
              },
              {
                name: "Repositório da Arquitetura",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Repositorio-da-Arquitetura.aspx",
              },
              {
                name: "Social",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Social.aspx",
              },
              {
                name: "Eventos",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Eventos.aspx",
              },
              {
                name: "Sou Novo Por Aqui",
                dropdownItems: [],
                link: "https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Sou-Novo-Por-Aqui.aspx",
              },
            ]}
            title="Menu"
          />
        </div>
      </div>
    );
  }
}
