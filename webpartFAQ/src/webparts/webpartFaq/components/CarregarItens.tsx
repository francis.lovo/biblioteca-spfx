/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";

interface ICarregarItems {
  setFaqData: any;
  setButtonNames: any;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const CarregarItems = ({
  setFaqData,
  setButtonNames,
}: ICarregarItems) => {
  React.useEffect(() => {
    async function getNotification() {
      const webUrl =
        "https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/Lists/GetByTitle('FAQ')/items";

      try {
        const response = await fetch(webUrl, {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        });

        if (response.ok) {
          const data = await response.json();

          console.log("Lista: ", data.d.results);
          const aux: string[] = [];
          data.d.results.forEach((items: any) => {
            items.Sobre.results.forEach((sobre: string) => {
              if (aux && aux.length > 0) {
                let existe = false;
                aux.forEach((sobreAux) => {
                  if (sobre === sobreAux) {
                    existe = true;
                  }
                });
                if (!existe) {
                  aux.push(sobre);
                }
              } else {
                aux.push(sobre);
              }
            });
          });
          const abas: any[] = [];
          aux.forEach((aba: any) => {
            const perguntaPorAba: string[] = [];
            const respostaPorAba: string[] = [];
            data.d.results.forEach((items: any) => {
              items.Sobre.results.forEach((sobre: string) => {
                if (sobre === aba) {
                  perguntaPorAba.push(items.Title);
                  respostaPorAba.push(items.Resposta);
                }
              });
            });
            abas.push({ itemNames: perguntaPorAba, itemTexts: respostaPorAba });
          });
          setFaqData(abas);
          setButtonNames(aux);
        } else {
          console.error("Falha na requisição");
        }
      } catch (error) {
        console.error("Erro na requisição: ", error);
      }
    }
    getNotification();
  }, []);

  return <></>;
};
