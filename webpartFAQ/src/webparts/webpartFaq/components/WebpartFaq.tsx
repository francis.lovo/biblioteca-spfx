/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { IWebpartFaqProps } from "./IWebpartFaqProps";
import { ArqText, ArqFaq } from "biblioteca-portal";
import { CarregarItems } from "./CarregarItens";

export default class WebpartFaq extends React.Component<IWebpartFaqProps, {}> {
  state = {
    faqData: [],
    buttonNames: [],
  };
  public render(): React.ReactElement<IWebpartFaqProps> {
    const setarData = (data: any): void => {
      this.setState({ faqData: data });
    };
    const setarButtons = (buttonNames: any): void => {
      this.setState({ buttonNames: buttonNames });
    };

    return (
      <div>
        <CarregarItems setFaqData={setarData} setButtonNames={setarButtons} />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
         
          <div
            style={{
              width: "100% !important",
            }}
          >
            <div
              style={{
                background: "#CC092f",
                padding: "10px",
                textAlign: "center",
                color: "white",
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
                height: "40px",
              }}
            >
              <ArqText
                fontSize="24px"
                color="#ffffff"
                fontWeight="bold"
                text="Perguntas Frequentes"
              />
            </div>
            <div
              style={{
                backgroundColor: "#f2f2f2",
                padding: "10px",
                display: "flex",
                alignItems: "center",
                width: "460px",
                fontFamily: "Bradesco Regular Sans",
                fontWeight: "bold",
                marginTop: "15px",
              }}
            >
              <span style={{ color: "black", fontSize: "16px" }}>
                Está com dúvida sobre algum termo? Acesse nosso
                <a
                  href="https://bancobradesco.sharepoint.com/teams/5800-pa/SitePages/Gloss%C3%A1rio.aspx"
                  style={{
                    color: "#cc092f",
                    textDecoration: "underline",
                    marginLeft: "5px",
                  }}
                >
                  Glossário
                </a>
              </span>
            </div>
            <div
              style={{
                paddingTop: "15px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-start",
              }}
            >
              {this.state.buttonNames &&
                this.state.buttonNames.length > 0 &&
                this.state.faqData &&
                this.state.faqData.length > 0 && (
                  <ArqFaq
                    buttons={this.state.buttonNames}
                    data={this.state.faqData}
                  />
                )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
