import { ArqIconButton, ArqInformationCard, ArqText } from "biblioteca-portal";
import * as React from "react";

interface IData {
  title: string;
  description: string;
}

interface IUpdateCard {
  updateData: IData[];
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const UpdateCard = ({ updateData }: IUpdateCard) => {
  return (
    <ArqInformationCard>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          marginBottom: 10,
        }}
      >
        <ArqText
          text="Atualizações"
          color="#484848"
          fontSize="18px"
          fontWeight="bold"
        />
        <ArqIconButton
          icon="icon-icon-seta-direita-c"
          color="#0000EE"
          hoverColor="#0000EE"
          fontSize="18px"
          fontWeight="bold"
        />
      </div>
      <div style={{ overflowY: "auto", maxHeight: 240 }}>
        {updateData.map((update, index) => {
          return (
            <div
              key={index}
              style={
                index > 0
                  ? {
                      display: "flex",
                      flexDirection: "column",
                      padding: 10,
                      borderTop: "1px solid #cacaca",
                    }
                  : {
                      display: "flex",
                      flexDirection: "column",
                      padding: 10,
                    }
              }
            >
              <div style={{ marginBottom: 5 }}>
                <ArqText
                  text={update.title}
                  fontSize="14px"
                  color="#484848"
                  fontWeight="bold"
                />
              </div>
              <ArqText
                text={update.description}
                fontSize="10px"
                color="#484848"
              />
            </div>
          );
        })}
      </div>
    </ArqInformationCard>
  );
};
