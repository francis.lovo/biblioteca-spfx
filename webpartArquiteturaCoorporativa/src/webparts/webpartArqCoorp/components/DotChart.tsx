import { ArqInformationCard, ArqLineChart, ArqText } from "biblioteca-portal";
import * as React from "react";

interface IData {
  width: number;
  height: number;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
}

interface IDotChar {
  dataDotChart: IData;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const DotChart = ({ dataDotChart }: IDotChar) => {
  return (
    <ArqInformationCard>
      <div style={{ marginBottom: 15 }}>
        <ArqText
          text="Lorem Ipsum"
          fontSize="18px"
          fontWeight="bold"
          color="#484848"
        />
      </div>
      <ArqLineChart
        chartWidth={dataDotChart.width}
        chartHeight={dataDotChart.height}
        dots={dataDotChart.data}
      />
    </ArqInformationCard>
  );
};
