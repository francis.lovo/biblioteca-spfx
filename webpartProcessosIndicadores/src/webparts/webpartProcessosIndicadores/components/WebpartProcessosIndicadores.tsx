import * as React from "react";
import { IWebpartProcessosIndicadoresProps } from "./IWebpartProcessosIndicadoresProps";
import {
  ArqDonutCharts,
  ArqInformationCard,
  ArqSelectInput,
  ArqText,
} from "biblioteca-portal";
import { HorizontalChart } from "./HorizontalChart";
import { BarChart } from "./BarChart";
import { UpdateCard } from "./UpdateCard";

export default class WebpartProcessosIndicadores extends React.Component<
  IWebpartProcessosIndicadoresProps,
  {}
> {
  state = {
    dataHorzontal: {
      labels: ["Lorem", "Lorem", "Lorem", "Lorem"],
      porcent: [90, 40, 60, 96],
    },
    dataDualBars: [
      {
        data: [
          { label: "Label X1", value1: 1, value2: 1.5 },
          { label: "Label X2", value1: 2, value2: 2.5 },
          { label: "Label X3", value1: 2.5, value2: 3 },
          { label: "Label X4", value1: 3.5, value2: 4 },
          { label: "Label X5", value1: 4.5, value2: 5 },
          { label: "Label X6", value1: 5.5, value2: 6 },
          { label: "Label X7", value1: 6.5, value2: 7 },
        ],
        y: [0, 1, 2, 3, 4, 5, 6, 7],
        label1: "Lorem",
        label2: "Lorem2",
      },
      {
        data: [
          { label: "Label Y1", value1: 1, value2: 1.5 },
          { label: "Label Y2", value1: 2, value2: 2.5 },
          { label: "Label Y3", value1: 2.5, value2: 3 },
          { label: "Label Y4", value1: 3.5, value2: 4 },
        ],
        y: [0, 1, 2, 3, 4],
        label1: "Lorem3",
        label2: "Lorem4",
      },
    ],
    selectedButtonBar: 0,
    updateData: [
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
      {
        title: "Lorem ipsum",
        description: "Neque podna quis dolorem ipsum seda..",
      },
    ],
  };
  public render(): React.ReactElement<IWebpartProcessosIndicadoresProps> {
    const handleClickButtonBar = (buttonNumber: number): void => {
      this.setState({ selectedButtonBar: buttonNumber });
    };
    return (
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 25,
          }}
        >
          <div style={{ width: "42%" }}>
            <HorizontalChart
              labels={this.state.dataHorzontal.labels}
              porcent={this.state.dataHorzontal.porcent}
            />
          </div>
          <div style={{ width: "57%" }}>
            <BarChart
              changeChart={handleClickButtonBar}
              selectedButton={this.state.selectedButtonBar}
              selectedChartData={
                this.state.dataDualBars[this.state.selectedButtonBar]
              }
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 10,
          }}
        >
          <div style={{ width: "68%" }}>
            <ArqInformationCard>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginBottom: 20,
                }}
              >
                <ArqText
                  text="Lorem Ipsum"
                  color="#484848"
                  fontSize="18px"
                  fontWeight="bold"
                />
                <div style={{ width: 200 }}>
                  <ArqSelectInput
                    options={[
                      { value: 1, label: "Lorem" },
                      { value: 2, label: "Ipsum" },
                      { value: 3, label: "Podna", disabled: true },
                    ]}
                  />
                </div>
              </div>
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  paddingLeft: 20,
                  paddingRight: 20,
                  marginBottom: 10,
                }}
              >
                <ArqDonutCharts
                  data={[
                    { label: "Label X%", value: 75, color: "#fa4160" },
                    { label: "Label Y%", value: 25, color: "#cc092f" },
                  ]}
                  radius={65}
                  strokeWidth={20}
                />
                <ArqDonutCharts
                  data={[
                    { label: "Label X%", value: 20, color: "#fa4160" },
                    { label: "Label Y%", value: 50, color: "#cc092f" },
                    { label: "Label K%", value: 30, color: "#d63a41" },
                  ]}
                  radius={65}
                  strokeWidth={20}
                />
                <ArqDonutCharts
                  data={[
                    { label: "Label X%", value: 96, color: "#fa4160" },
                    { label: "Label Y%", value: 4, color: "#cc092f" },
                  ]}
                  radius={65}
                  strokeWidth={20}
                />
              </div>
            </ArqInformationCard>
          </div>
          <div style={{ width: "31%" }}>
            <UpdateCard updateData={this.state.updateData} />
          </div>
        </div>
      </div>
    );
  }
}
