import * as React from "react";
import {
  ArqText,
  ArqInformationCard,
  ArqHorizontalChart,
} from "biblioteca-portal";

interface IHorizontalchartProps {
  labels: string[];
  porcent: number[];
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const HorizontalChart = ({ labels, porcent }: IHorizontalchartProps) => {
  return (
    <ArqInformationCard>
      <div>
        <div style={{ marginBottom: 20 }}>
          <ArqText
            text="Demandas"
            color="#484848"
            fontWeight="bold"
            fontSize="16"
          />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div style={{ width: "55%" }}>
            <ArqHorizontalChart labels={labels} porcents={porcent} />
          </div>
          <div
            style={{
              width: "36%",
              alignItems: "start",
            }}
          >
            <div style={{ marginBottom: 10 }}>
              <ArqText text="Lorem ipsum" fontSize="14px" fontWeight="900" />
              <ArqText
                text="Neque podna quis doloren ipsum seda volupta..."
                fontSize="10px"
              />
            </div>
            <ArqText text="Lorem ipsum" fontSize="14px" fontWeight="900" />
            <ArqText
              text="Neque podna quis doloren ipsum seda volupta..."
              fontSize="10px"
            />
          </div>
        </div>
      </div>
    </ArqInformationCard>
  );
};
