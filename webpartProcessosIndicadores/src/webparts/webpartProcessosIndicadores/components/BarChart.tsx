import {
  ArqChartCaption,
  ArqChartDualBars,
  ArqInformationCard,
  ArqText,
} from "biblioteca-portal";
import * as React from "react";

interface IChartData {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any;
  y: number[];
  label1: string;
  label2: string;
}

interface IBarChart {
  changeChart: (num: number) => void;
  selectedButton: number;
  selectedChartData: IChartData;
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export const BarChart = ({
  changeChart,
  selectedButton,
  selectedChartData,
}: IBarChart) => {
  return (
    <ArqInformationCard>
      <div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            marginBottom: 20,
          }}
        >
          <div style={{ marginRight: 10 }}>
            <div
              onClick={() => changeChart(0)}
              style={
                selectedButton === 0
                  ? {
                      backgroundColor: "#B4092A",
                      border: "1px solid #B4092A",
                      padding: 5,
                      borderRadius: 4,
                      cursor: "pointer",
                    }
                  : {
                      border: "1px solid #cacaca",
                      padding: 5,
                      borderRadius: 4,
                      cursor: "pointer",
                    }
              }
            >
              <ArqText
                text="Lorem"
                color={selectedButton === 0 ? "white" : "#484848"}
                fontSize="12px"
              />
            </div>
          </div>

          <div
            onClick={() => changeChart(1)}
            style={
              selectedButton === 1
                ? {
                    backgroundColor: "#B4092A",
                    border: "1px solid #B4092A",
                    padding: 5,
                    borderRadius: 4,
                    cursor: "pointer",
                  }
                : {
                    border: "1px solid #cacaca",
                    padding: 5,
                    borderRadius: 4,
                    cursor: "pointer",
                  }
            }
          >
            <ArqText
              text="Ipsum"
              color={selectedButton === 1 ? "white" : "#484848"}
              fontSize="12px"
            />
          </div>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <div style={{ width: "80%", marginBottom: 15 }}>
            <ArqChartDualBars
              chartData={selectedChartData.data}
              chartYData={selectedChartData.y}
              firstColor="#6444bc"
              secColor="#ff605e"
            />
          </div>
          <div style={{ width: "16%" }}>
            <div style={{ marginBottom: 10 }}>
              <ArqChartCaption
                color="#6444bc"
                label={selectedChartData.label1}
              />
            </div>

            <ArqChartCaption color="#ff605e" label={selectedChartData.label2} />
          </div>
        </div>
      </div>
    </ArqInformationCard>
  );
};
