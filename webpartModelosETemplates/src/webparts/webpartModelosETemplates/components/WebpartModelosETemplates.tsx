import * as React from 'react';
import { IWebpartModelosETemplatesProps } from './IWebpartModelosETemplatesProps';
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubMenuBar";

export default class WebpartModelosETemplates extends React.Component<IWebpartModelosETemplatesProps, {}> {
  public render(): React.ReactElement<IWebpartModelosETemplatesProps> {
   

    return (
      <div>
      <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb items={["Modelos e Templates"]} links={["/"]} />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Modelos e Templates"
            fontSize="20px"
            color="#484848"
            fontWeight="bold"
          />
        </div>
        <div>
        <SubMenuBar />
        </div>
     </div>
    );
  }
}
