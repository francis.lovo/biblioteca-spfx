import * as React from 'react';
import { IWebpartArquiteturaCorporativaSobreProps } from './IWebpartArquiteturaCorporativaSobreProps';
import {
  ArqBreadcrumb,
  ArqText,
} from "biblioteca-portal"

export default class WebpartArquiteturaCorporativaSobre extends React.Component<IWebpartArquiteturaCorporativaSobreProps, {}> {
  public render(): React.ReactElement<IWebpartArquiteturaCorporativaSobreProps> {


    return (
      <div>
      <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb items={["Arquitetura Corporativa","Sobre"]} links={["/"]} />
      </div>
      <div style={{ marginBottom: 15 }}>
      <ArqText
          fontSize="20px"
          color="#000000"
          fontWeight="bold"
          text="Sobre"/>
       </div>
      <div
      style={{
        background: "#CC092f",
        padding: "10px",
        textAlign: "center",
        color: "white",
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "center",
        height:"40px",
        }}>
          <ArqText
            fontSize="24px"
            color="#ffffff"
            fontWeight="bold"
            text="Arquitetura Corporativa"/>
        </div>
      </div>
    );
  }
}
