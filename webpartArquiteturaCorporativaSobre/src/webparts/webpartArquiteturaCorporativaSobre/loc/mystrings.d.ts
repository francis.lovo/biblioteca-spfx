declare interface IWebpartArquiteturaCorporativaSobreWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  AppLocalEnvironmentSharePoint: string;
  AppLocalEnvironmentTeams: string;
  AppLocalEnvironmentOffice: string;
  AppLocalEnvironmentOutlook: string;
  AppSharePointEnvironment: string;
  AppTeamsTabEnvironment: string;
  AppOfficeEnvironment: string;
  AppOutlookEnvironment: string;
}

declare module 'WebpartArquiteturaCorporativaSobreWebPartStrings' {
  const strings: IWebpartArquiteturaCorporativaSobreWebPartStrings;
  export = strings;
}
