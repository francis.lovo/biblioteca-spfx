import * as React from "react";
import { IWebpartGestFuncionariosProps } from "./IWebpartGestFuncionariosProps";
import { ArqBreadcrumb, ArqText } from "biblioteca-portal";
import { SubMenuBar } from "./SubmenuBar";

export default class WebpartGestFuncionarios extends React.Component<
  IWebpartGestFuncionariosProps,
  {}
> {
  public render(): React.ReactElement<IWebpartGestFuncionariosProps> {
    return (
      <div>
        <div style={{ marginBottom: 15 }}>
          <ArqBreadcrumb
            items={["Gestão", "Gestão de Funcionários"]}
            links={["/", "/SitePages/Gestao.aspx"]}
          />
        </div>
        <div
          style={{
            borderBottom: "1px solid #cacaca",
            paddingBottom: 15,
            marginBottom: 10,
          }}
        >
          <ArqText
            text="Gestão de Funcionários"
            fontSize="20px"
            color="#484848"
            fontWeight={"bold"}
          />
        </div>
        <SubMenuBar />
      </div>
    );
  }
}
