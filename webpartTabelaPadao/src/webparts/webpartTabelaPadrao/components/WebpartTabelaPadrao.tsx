/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import { IWebpartTabelaPadraoProps } from "./IWebpartTabelaPadraoProps";
import styles from "./WebpartTabelaPadrao.module.scss";
//import { escape } from "@microsoft/sp-lodash-subset";
import {
  ArqPagination,
  ArqTable,
  ArqProgressBar,
  ArqInformationCard,
  ArqText,
  ArqSelectInput,
  ArqChartCaption,
  ArqTextField,
} from "biblioteca-portal";
interface ISelect {
  value: string;
  label: string;
}

interface MyState {
  currentPagePagination: string;
  currentTableData: any[];
  tableData: any[];
  headerTable: any;
  changed: boolean;
  total: number;
  chartDescription: any[];
  porcentBlue: number;
  porcentYellow: number;
  porcentGreen: number;
  porcentRed: number;
  totalPorcent: number;
  textUserFilter: string;
  selectedFilter: ISelect;
}

export default class WebpartTabelaPadrao extends React.Component<
  IWebpartTabelaPadraoProps,
  {}
> {
  state: MyState = {
    currentPagePagination: "1",
    currentTableData: this.props.tableData,
    tableData: this.props.tableData,
    headerTable: this.props.headerTable,
    changed: false,
    total: this.props.total,
    chartDescription: this.props.chartDescription,
    porcentBlue: this.props.porcentBlue,
    porcentYellow: this.props.porcentYellow,
    porcentGreen: this.props.porcentGreen,
    porcentRed: this.props.porcentRed,
    totalPorcent: this.props.totalPorcent,
    textUserFilter: "",
    selectedFilter: { value: "Nenhum", label: "Nenhum" },
  };

  passaFiltro = (filtro: string, filtros: string[]): boolean => {
    let passou = false;
    filtros.map((filt: string) => {
      if (filtro + "" === filt) {
        passou = true;
      }
    });
    return passou;
  };
  retirarMascaras = (valor: any): number => {
    if (typeof valor === "string") {
      const numeroLimpo = valor.replace(/[^-0-9.,]/g, "");
      const numeroPonto = numeroLimpo.replace(/,/, ".");
      return parseFloat(numeroPonto);
    } else if (typeof valor === "number") {
      return valor;
    } else {
      return NaN;
    }
  };
  moutChart = (value: any, text: string): void => {
    let aux: string[] = [];
    //montar grafico
    let totalSum = 0;
    let blueSum = 0;
    let greenSum = 0;
    let yellowSum = 0;
    let restSum = 0;
    let filterA: string[] | undefined;
    if (this.props.filterTf) {
      filterA = this.props.filterTf.split(", ");
    }

    if (text !== "") {
      aux = text.split(", ");
    }
    if (
      this.props.totalProperties &&
      this.props.totalProperties !== "" &&
      this.props.totalProperties !== "Nenhum"
    ) {
      this.props.fileData.forEach((data: any) => {
        if (
          (filterA && this.passaFiltro(data[this.props.filter], filterA)) ||
          !filterA ||
          filterA.length === 0
        ) {
          if (
            this.props.userFilter1 === "Nenhum" ||
            (value.value !== "Nenhum" &&
              data[this.props.userFilter1] === value.value) ||
            value.value === "Nenhum"
          ) {
            let passou = false;
            if (aux.length > 0) {
              aux.forEach((dataaux: string) => {
                if (data[this.props.userFilter2] === dataaux) {
                  passou = true;
                }
              });
            }
            if (
              this.props.userFilter2 === "Nenhum" ||
              !this.props.userFilter2 ||
              !this.props.showFilter
            ) {
              passou = true;
            }
            if (passou) {
              if (
                !Number.isNaN(
                  this.retirarMascaras(data[this.props.totalProperties])
                )
              ) {
                totalSum =
                  totalSum +
                  this.retirarMascaras(data[this.props.totalProperties]);
              }
              if (
                !Number.isNaN(
                  this.retirarMascaras(data[this.props.blueProperties])
                )
              ) {
                blueSum =
                  blueSum +
                  this.retirarMascaras(data[this.props.blueProperties]);
              }
              if (
                !Number.isNaN(
                  this.retirarMascaras(data[this.props.greenProperties])
                )
              ) {
                if (
                  this.retirarMascaras(data[this.props.greenProperties]) >= 0
                ) {
                  greenSum =
                    greenSum +
                    this.retirarMascaras(data[this.props.greenProperties]);
                } else {
                  restSum =
                    restSum +
                    this.retirarMascaras(data[this.props.greenProperties]);
                }
              }
              if (
                !Number.isNaN(
                  this.retirarMascaras(data[this.props.yellowProperties])
                )
              ) {
                yellowSum =
                  yellowSum +
                  this.retirarMascaras(data[this.props.yellowProperties]);
              }
            }
          }
        }
      });
      const totalPorcent = totalSum + restSum * -1;

      this.setState({
        total: totalSum,
        porcentBlue: (100 * blueSum) / totalPorcent,
        porcentGreen: (100 * greenSum) / totalPorcent,
        porcentYellow: (100 * yellowSum) / totalPorcent,
        porcentRed: (100 * (restSum * -1)) / totalPorcent,
        totalPorcent: 100 + (restSum * -1 * 100) / totalSum,
      });
      const auxChartDescription: any[] = [];
      this.state.chartDescription.forEach((data: any, index: number) => {
        if (index === 0) {
          auxChartDescription.push({
            nome: "Orçado",
            custo: aplicarMascara(totalSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 1) {
          auxChartDescription.push({
            nome: "Comprometido",
            custo: aplicarMascara(yellowSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 2) {
          auxChartDescription.push({
            nome: "Disponível",
            custo: aplicarMascara(greenSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 3) {
          auxChartDescription.push({
            nome: "% Comprometida",
            custo:
              +((100 * yellowSum) / totalPorcent).toFixed(2).split(".")[1] === 0
                ? ((100 * yellowSum) / totalPorcent).toFixed(0)
                : ((100 * yellowSum) / totalPorcent).toFixed(2),
            mascara: "%",
          });
        } else if (index === 4) {
          auxChartDescription.push({
            nome: "% Disponível",
            custo:
              greenSum > 0
                ? +((100 * greenSum) / totalPorcent)
                    .toFixed(2)
                    .split(".")[1] === 0
                  ? ((100 * greenSum) / totalPorcent).toFixed(0)
                  : ((100 * greenSum) / totalPorcent).toFixed(2)
                : 0,
            mascara: "%",
          });
        }
      });
      this.setState({ chartDescription: auxChartDescription });
    }

    function aplicarMascara(valor: string): string {
      const valorNumerico = parseFloat(valor.replace(",", "."));
      const valorFormatado = valorNumerico.toLocaleString("pt-BR");

      return valorFormatado;
    }
  };

  mountTable = (value: any, text: string): void => {
    let filterA: string[] | undefined = [];
    let aux: string[] = [];
    if (this.props.filterTf) {
      filterA = this.props.filterTf.split(", ");
    }
    const auxTable: any[] = [];
    if (this.props.headerType === "Dados em Colunas") {
      //header com o selectedColumns
      this.setState({ headerTable: this.props.selectedColumns });
    } else {
      //header com o titulo

      this.setState({ headerTable: [this.props.title] });
      auxTable.push(this.props.selectedColumns);
    }
    if (text !== "") {
      aux = text.split(", ");
    }
    this.props.fileData.forEach((data: any) => {
      const auxRow: any[] = [];
      this.props.selectedColumns.forEach((selected: string) => {
        if (
          (filterA && this.passaFiltro(data[this.props.filter], filterA)) ||
          !filterA ||
          filterA.length === 0
        ) {
          if (
            this.props.userFilter1 === "Nenhum" ||
            (value.value !== "Nenhum" &&
              data[this.props.userFilter1] === value.value) ||
            value.value === "Nenhum"
          ) {
            let passou = false;
            if (aux.length > 0) {
              aux.forEach((dataaux: string) => {
                if (data[this.props.userFilter2] === dataaux) {
                  passou = true;
                }
              });
            }
            if (
              this.props.userFilter2 === "Nenhum" ||
              !this.props.userFilter2
            ) {
              passou = true;
            }
            if (passou) {
              if (data[selected] + "") {
                auxRow.push(data[selected]);
              } else {
                auxRow.push("");
              }
            }
          }
        }
      });
      if (auxRow.length > 0) {
        auxTable.push(auxRow);
      }
    });
    if (auxTable.length > 0) {
      this.setState({ tableData: auxTable, currentPagePagination: "1" });

      //logica para separar
      const startIndex = 0 * +this.props.pagination;
      const endIndex =
        this.props.headerType === "Título"
          ? startIndex + 1 + +this.props.pagination
          : startIndex + +this.props.pagination;
      // Dados para a página atual
      const aux =
        this.props.headerType === "Título"
          ? auxTable.slice(startIndex + 1, endIndex)
          : auxTable.slice(startIndex, endIndex);
      if (this.props.headerType === "Título") {
        aux.unshift(auxTable[0]);
      }

      this.setState({
        currentTableData:
          +this.props.pagination && +this.props.pagination > 0 ? aux : auxTable,
      });
    }
  };

  componentDidMount(): void {
    if (
      +this.props.pagination > 0 &&
      this.state.tableData &&
      this.state.tableData.length > 0
    ) {
      //logica para separar
      const startIndex =
        (+this.state.currentPagePagination - 1) * +this.props.pagination;
      const endIndex =
        this.props.headerType === "Título"
          ? startIndex + 1 + +this.props.pagination
          : startIndex + +this.props.pagination;
      // Dados para a página atual
      const aux =
        this.props.headerType === "Título"
          ? this.state.tableData.slice(startIndex + 1, endIndex)
          : this.state.tableData.slice(startIndex, endIndex);
      if (this.props.headerType === "Título") {
        aux.unshift(this.state.tableData[0]);
      }

      this.setState({
        currentTableData: aux,
      });
    }
    if (
      this.props.userFilter2 &&
      this.props.userFilter2 !== "" &&
      this.state.tableData &&
      this.state.tableData.length > 0
    ) {
      let filterA: string[] | undefined = [];
      if (this.props.filterTf) {
        filterA = this.props.filterTf.split(", ");
      }
      const auxRow: any[] = [];

      this.props.fileData.forEach((data: any) => {
        if (
          (filterA && this.passaFiltro(data[this.props.filter], filterA)) ||
          !filterA ||
          filterA.length === 0
        ) {
          if (
            (this.state.selectedFilter.value !== "Nenhum" &&
              data[this.props.userFilter1] ===
                this.state.selectedFilter.value) ||
            this.state.selectedFilter.value === "Nenhum"
          ) {
            if (this.props.userFilter2 !== "Nenhum") {
              if (auxRow.length === 0) {
                auxRow.push(data[this.props.userFilter2] + "");
              } else {
                let achou = false;
                auxRow.forEach((aux: string) => {
                  if (data[this.props.userFilter2] + "" === aux) {
                    achou = true;
                  }
                });
                if (!achou) {
                  auxRow.push(data[this.props.userFilter2] + "");
                }
              }
            }
          }
        }
      });
      this.setState({
        textUserFilter: auxRow.length > 0 ? auxRow.join(", ") : "",
      });
    }
  }
  componentDidUpdate(
    prevProps: Readonly<IWebpartTabelaPadraoProps>,
    prevState: Readonly<MyState>,
    snapshot?: any
  ): void {
    if (this.props.total && prevProps.total !== this.props.total) {
      this.setState({
        total: this.props.total,
        chartDescription: this.props.chartDescription,
        porcentBlue: this.props.porcentBlue,
        porcentYellow: this.props.porcentYellow,
        porcentGreen: this.props.porcentGreen,
        porcentRed: this.props.porcentRed,
        totalPorcent: this.props.totalPorcent,
      });
    }
    if (
      this.props.tableData &&
      this.props.tableData.length > 0 &&
      prevProps.tableData !== this.props.tableData
    ) {
      this.setState({
        currentTableData: this.props.tableData,
      });
      this.setState({
        tableData: this.props.tableData,
      });
    }
    if (
      +this.props.pagination > 0 &&
      this.props.tableData &&
      this.props.tableData.length > 0 &&
      prevProps !== this.props &&
      this.state.tableData === prevState.tableData
    ) {
      //logica para separar
      const startIndex =
        (+this.state.currentPagePagination - 1) * +this.props.pagination;
      const endIndex =
        this.props.headerType === "Título"
          ? startIndex + 1 + +this.props.pagination
          : startIndex + +this.props.pagination;
      // Dados para a página atual
      const aux =
        this.props.headerType === "Título"
          ? this.props.tableData.slice(startIndex + 1, endIndex)
          : this.props.tableData.slice(startIndex, endIndex);
      if (this.props.headerType === "Título") {
        aux.unshift(this.props.tableData[0]);
      }

      this.setState({
        currentTableData: aux,
        currentPagePagination: "1",
      });
    }
    if (
      prevProps !== this.props &&
      (!this.props.pagination ||
        this.props.pagination + "" === "" ||
        +this.props.pagination === 0)
    ) {
      this.setState({
        currentTableData: this.props.tableData,
        currentPagePagination: "1",
      });
    }
    if (
      prevProps.showFilter !== this.props.showFilter &&
      !this.props.showFilter
    ) {
      //logica para separar
      const startIndex = 0 * +this.props.pagination;
      const endIndex =
        this.props.headerType === "Título"
          ? startIndex + 1 + +this.props.pagination
          : startIndex + +this.props.pagination;
      // Dados para a página atual
      const aux =
        this.props.headerType === "Título"
          ? this.props.tableData.slice(startIndex + 1, endIndex)
          : this.props.tableData.slice(startIndex, endIndex);
      if (this.props.headerType === "Título") {
        aux.unshift(this.props.tableData[0]);
      }

      console.log(
        +this.props.pagination && +this.props.pagination > 0
          ? aux
          : this.props.tableData
      );

      this.setState({
        currentTableData:
          +this.props.pagination && +this.props.pagination > 0
            ? aux
            : this.props.tableData,
        currentPagePagination: "1",
        tableData: this.props.tableData,
      });

      this.moutChart({ value: "Nenhum", label: "Nenhum" }, "");
    }
    if (
      ((this.props.userFilter2 && this.props.userFilter2 !== "") ||
        (this.props.userFilter1 && this.props.userFilter1 !== "")) &&
      this.props.showFilter &&
      (prevProps.userFilter2 !== this.props.userFilter2 ||
        this.state.selectedFilter !== prevState.selectedFilter ||
        prevProps.userFilter1 !== this.props.userFilter1 ||
        prevProps.showFilter !== this.props.showFilter)
    ) {
      let filterA: string[] | undefined = [];
      if (this.props.filterTf) {
        filterA = this.props.filterTf.split(", ");
      }
      const auxRow: any[] = [];

      this.props.fileData.forEach((data: any) => {
        if (
          (filterA && this.passaFiltro(data[this.props.filter], filterA)) ||
          !filterA ||
          filterA.length === 0
        ) {
          if (
            this.props.userFilter1 === "Nenhum" ||
            (this.state.selectedFilter.value !== "Nenhum" &&
              data[this.props.userFilter1] ===
                this.state.selectedFilter.value) ||
            this.state.selectedFilter.value === "Nenhum"
          ) {
            if (this.props.userFilter2 !== "Nenhum" && this.props.showFilter) {
              if (auxRow.length === 0) {
                auxRow.push(data[this.props.userFilter2] + "");
              } else {
                let achou = false;
                auxRow.forEach((aux: string) => {
                  if (data[this.props.userFilter2] + "" === aux) {
                    achou = true;
                  }
                });
                if (!achou) {
                  auxRow.push(data[this.props.userFilter2] + "");
                }
              }
            }
          }
        }
      });
      this.setState({
        textUserFilter: auxRow.length > 0 ? auxRow.join(", ") : "",
      });
      this.moutChart(
        this.state.selectedFilter,
        auxRow.length > 0 ? auxRow.join(", ") : ""
      );
      this.mountTable(
        this.state.selectedFilter,
        auxRow.length > 0 ? auxRow.join(", ") : ""
      );
    }
    if (
      prevProps.userFilter1 !== this.props.userFilter1 ||
      (prevProps.showFilter !== this.props.showFilter && !this.props.showFilter)
    ) {
      this.setState({ selectedFilter: { label: "Nenhum", value: "Nenhum" } });
    }
  }

  public render(): React.ReactElement<IWebpartTabelaPadraoProps> {
    const {
      hasTeamsContext,
      headerTable,
      pagination,
      headerType,
      chartTitle,
      toggleTable,
      showFilter,
      userFilter1,
      userFilterData,
      userFilter2,
      fontSize,
    } = this.props;

    const totalPages =
      headerType === "Título"
        ? (this.state.tableData.length - 1) / +pagination + ""
        : this.state.tableData.length / +pagination + "";

    const changePage = (currentPage: number): void => {
      //logica para separar
      const startIndex = (+currentPage - 1) * +this.props.pagination;
      const endIndex =
        this.props.headerType === "Título"
          ? startIndex + 1 + +this.props.pagination
          : startIndex + +this.props.pagination;
      // Dados para a página atual
      const aux =
        this.props.headerType === "Título"
          ? this.state.tableData.slice(startIndex + 1, endIndex)
          : this.state.tableData.slice(startIndex, endIndex);
      if (this.props.headerType === "Título") {
        aux.unshift(this.state.tableData[0]);
      }

      this.setState({
        currentTableData: aux,
      });
    };
    const onClickPageUp = (): void => {
      if (+this.state.currentPagePagination - 1 >= 1) {
        this.setState({
          currentPagePagination: +this.state.currentPagePagination - 1 + "",
        });
        changePage(+this.state.currentPagePagination - 1);
      }
    };
    const onClickPageDown = (): void => {
      const totalPagesAux =
        +totalPages < 1
          ? headerType === "Título"
            ? totalPages.length - 1 + ""
            : totalPages.length + ""
          : totalPages.split(".").length > 1
          ? +totalPages.split(".")[0] + 1 + ""
          : totalPages;

      if (+this.state.currentPagePagination + 1 <= +totalPagesAux) {
        this.setState({
          currentPagePagination: +this.state.currentPagePagination + 1 + "",
        });
        changePage(+this.state.currentPagePagination + 1);
      }
    };
    const onClickFirstPage = (): void => {
      this.setState({
        currentPagePagination: "1",
      });
      changePage(1);
    };
    const onClickLastPage = (): void => {
      const totalPagesAux =
        +totalPages < 1
          ? headerType === "Título"
            ? totalPages.length - 1 + ""
            : totalPages.length + ""
          : totalPages.split(".").length > 1
          ? +totalPages.split(".")[0] + 1 + ""
          : totalPages;
      this.setState({
        currentPagePagination: totalPagesAux,
      });
      changePage(+totalPagesAux);
    };

    const onchangeSelect = (value: any): void => {
      this.setState({ selectedFilter: value });
      this.mountTable(value, this.state.textUserFilter);
      this.moutChart(value, this.state.textUserFilter);
    };
    const changeTextUser = (text: string): void => {
      this.setState({ textUserFilter: text });
      this.mountTable(this.state.selectedFilter, text);
      this.moutChart(this.state.selectedFilter, text);
    };

    return (
      <section
        className={`${styles.webpartTabelaPadrao} ${
          hasTeamsContext ? styles.teams : ""
        }`}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            gap: 20,
            marginBottom: 35,
          }}
        >
          {showFilter && (
            <div
              style={{ display: "flex", flexDirection: "column", width: "30%" }}
            >
              {showFilter && userFilter1 && userFilter1 !== "Nenhum" && (
                <div>
                  <ArqText
                    text={userFilter1}
                    fontSize="14px"
                    fontWeight="bold"
                    color="#4d4e53"
                  />
                  <div style={{ marginTop: 20, marginBottom: 20 }}>
                    <ArqSelectInput
                      options={[
                        ...userFilterData.map((data) => {
                          return { value: data, label: data };
                        }),
                      ]}
                      onChange={onchangeSelect}
                    />
                  </div>
                </div>
              )}
              {showFilter && userFilter2 && userFilter2 !== "Nenhum" && (
                <div>
                  <ArqText
                    text={userFilter2}
                    fontSize="14px"
                    fontWeight="bold"
                    color="#4d4e53"
                  />
                  <div style={{ marginTop: 20 }}>
                    <ArqTextField
                      value={this.state.textUserFilter}
                      onChange={changeTextUser}
                    />
                  </div>
                </div>
              )}
            </div>
          )}

          {toggleTable && (
            <div
              style={{
                display: "flex",
                width: "65%",
              }}
            >
              <div style={{ width: "90%" }}>
                <ArqInformationCard>
                  <div
                    style={
                      !!chartTitle && chartTitle !== ""
                        ? {
                            margin: 20,
                          }
                        : { margin: 30 }
                    }
                  >
                    <div style={{ marginBottom: 35 }}>
                      <ArqText
                        text={chartTitle}
                        fontSize="14px"
                        fontWeight="bold"
                        color="#4d4e53"
                      />
                    </div>
                    <div style={{ width: "95%", marginBottom: 8 }}>
                      <ArqProgressBar
                        porcentBlue={this.state.porcentBlue}
                        porcentGreen={this.state.porcentGreen}
                        porcentYellow={this.state.porcentYellow}
                        //porcentRed={porcentRed}
                        total={this.state.totalPorcent}
                        fontSize={!isNaN(+fontSize) ? +fontSize : 12}
                      />
                    </div>
                    {this.state.porcentRed > 0 && (
                      <div style={{ width: "95%", marginTop: 30 }}>
                        <ArqProgressBar
                          porcentRed={this.state.porcentRed}
                          total={this.state.totalPorcent}
                          fontSize={!isNaN(+fontSize) ? +fontSize : 12}
                        />
                      </div>
                    )}
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        gap: 20,
                        justifyContent: "space-between",
                        marginTop: 30,
                      }}
                    >
                      <ArqChartCaption label="Faturado" color="#007bff" />
                      <ArqChartCaption label="Disponível" color="#28a745" />
                      <ArqChartCaption label="Comprometido" color="#ffc107" />
                      <ArqChartCaption label="Excedente" color="#dc3545" />
                    </div>
                    <div
                      style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "center",
                        gap: 30,
                        marginTop: 20,
                        flexWrap: "wrap",
                      }}
                    >
                      {this.state.chartDescription &&
                        this.state.chartDescription.length > 0 &&
                        this.state.chartDescription.map((data, index) => {
                          return (
                            <div
                              style={{
                                display: "flex",
                                flexDirection: "column",
                                justifyContent: "center",
                              }}
                              key={index}
                            >
                              <div
                                style={{
                                  display: "flex",
                                  justifyContent: "center",
                                }}
                              >
                                <ArqText
                                  text={data.nome}
                                  fontSize="14px"
                                  color="#4d4e53"
                                  fontWeight="bold"
                                />
                              </div>
                              <div
                                style={{
                                  display: "flex",
                                  flexDirection: "row",
                                  gap: 10,
                                  justifyContent: "center",
                                }}
                              >
                                {data.mascara === "R$" && (
                                  <div>
                                    <ArqText
                                      text={data.mascara}
                                      fontSize="14px"
                                      color="#4d4e53"
                                      fontWeight="bold"
                                    />
                                  </div>
                                )}
                                <div>
                                  <ArqText
                                    text={
                                      data.mascara === "%"
                                        ? data.custo + data.mascara
                                        : data.custo
                                    }
                                    fontSize="14px"
                                    color="#4d4e53"
                                  />
                                </div>
                              </div>
                            </div>
                          );
                        })}
                    </div>
                  </div>
                </ArqInformationCard>
              </div>
            </div>
          )}
        </div>

        {this.state.tableData &&
          this.state.tableData.length > 0 &&
          headerTable &&
          headerTable.length > 0 && (
            <div style={{ overflowX: "auto" }}>
              <ArqTable
                tableItems={this.state.currentTableData}
                topLabels={headerTable}
              />
              {pagination &&
                +pagination > 0 &&
                !isNaN(+totalPages) &&
                (headerType === "Título"
                  ? +pagination < this.state.tableData.length - 1
                  : +pagination < this.state.tableData.length) && (
                  <ArqPagination
                    currentPage={this.state.currentPagePagination}
                    totalPages={
                      +totalPages < 1
                        ? this.state.tableData.length - 1 + ""
                        : totalPages.split(".").length > 1
                        ? +totalPages.split(".")[0] + 1 + ""
                        : totalPages
                    }
                    onClickPageUp={onClickPageUp}
                    onClickPageDown={onClickPageDown}
                    onClickLastPage={onClickLastPage}
                    onClickFirstPage={onClickFirstPage}
                  />
                )}
            </div>
          )}
      </section>
    );
  }
}
