/* eslint-disable @typescript-eslint/no-explicit-any */
export interface IWebpartTabelaPadraoProps {
  isDarkTheme: boolean;
  environmentMessage: string;
  hasTeamsContext: boolean;
  userDisplayName: string;
  fileData: any[];
  headerTable: any[];
  tableData: any[];
  pagination: string;
  headerType: string;
  chartTitle: string;
  toggleTable: boolean;
  chartDescription: any[];
  porcentBlue: number;
  porcentYellow: number;
  porcentGreen: number;
  porcentRed: number;
  totalPorcent: number;
  total: number;
  showFilter: boolean;
  userFilter1: string;
  userFilter2: string;
  userFilterData: string[];
  selectedColumns: any[];
  filter: string;
  filterTf: string;
  title: string;
  totalProperties: string;
  blueProperties: any;
  greenProperties: any;
  yellowProperties: any;
  fontSize: string;
}
