/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-explicit-any */
import * as React from "react";
import * as ReactDom from "react-dom";
import { PropertyFieldFilePicker } from "@pnp/spfx-property-controls/lib/PropertyFieldFilePicker";
import { Version } from "@microsoft/sp-core-library";
import {
  IPropertyPaneConfiguration,
  PropertyPaneButton,
  PropertyPaneButtonType,
  PropertyPaneChoiceGroup,
  IPropertyPaneDropdownOption,
  PropertyPaneDropdown,
  PropertyPaneTextField,
  PropertyPaneCheckbox,
  PropertyPaneLabel,
  PropertyPaneToggle,
} from "@microsoft/sp-property-pane";
import { BaseClientSideWebPart } from "@microsoft/sp-webpart-base";
import { IReadonlyTheme } from "@microsoft/sp-component-base";

import * as strings from "WebpartTabelaPadraoWebPartStrings";
import WebpartTabelaPadrao from "./components/WebpartTabelaPadrao";
import { IWebpartTabelaPadraoProps } from "./components/IWebpartTabelaPadraoProps";
import * as XLSX from "xlsx";

export interface IWebpartTabelaPadraoWebPartProps {
  filePickerResult: any;
  fileData: any[];
  headerTable: any;
  filterTf: string;
  filter: string;
  title: string;
  selectedColumns: string[];
  tableData: any[];
  headerData: string[];
  pagination: string;
  chartFields: any;
  chartTitle: string;
  toggleTable: boolean;
  total: string;
  blue: string;
  yellow: string;
  green: string;
  chartDescription: any[];
  userFilterData: string[];
  chosenUserFilter: string;
}

export default class WebpartTabelaPadraoWebPart extends BaseClientSideWebPart<any> {
  private showTable: boolean = true;
  private lastModifiedTime: Date | null = null;
  private _isDarkTheme: boolean = false;
  private _environmentMessage: string = "";
  private fileData: any;
  private columnNames: string[] = [];
  private filters: any[] = [];
  private filterTfText = "";
  private selectedColumns: string[] = [];
  private headerTable: string[] = [];
  private tableData: any[] = [];
  private paginationTfText = "";
  private chartFields: any = [];
  private total: number = 0;

  private salvarConfiguracoes(): void {
    this.properties.fileData = this.fileData;
    this.properties.selectedColumns = this.selectedColumns;
    this.properties.headerData = this.headerTable;
    this.properties.tableData = this.tableData;
    this.properties.chartFields = this.chartFields;
    this.context.propertyPane.refresh();
  }
  private loadTemplates(): void {
    //carregar propriedades com as colunas
    this.filters = [];
    const cbFilter: IPropertyPaneDropdownOption[] = [];
    this.columnNames.map(function (item) {
      cbFilter.push({
        key: item,
        text: item,
      });
    });
    cbFilter.unshift({
      key: "Nenhum",
      text: "Nenhum",
    });

    this.filters.push(
      PropertyPaneDropdown("filter", {
        label: "Filtro:",
        options: cbFilter,
      })
    );
    this.filters.push(
      PropertyPaneTextField("filterTf", {
        label: "Filtro Selecionado: ",
        value: this.filterTfText,
      })
    );
    this.filters.push(
      PropertyPaneToggle("showFilter", {
        label: "Mostrar Filtros para o usuário",
      })
    );
    if (this.properties.showFilter) {
      this.filters.push(
        PropertyPaneDropdown("userFilter1", {
          label: "Primeiro filtro mostrado ao usuário",
          options: cbFilter,
        })
      );
      this.filters.push(
        PropertyPaneDropdown("userFilter2", {
          label: "Segundo filtro mostrado ao usuário",
          options: cbFilter,
        })
      );
    }
  }
  private passaFiltro(filtro: string, filtros: string[]): boolean {
    let passou = false;
    filtros.map((filt: string) => {
      if (filtro + "" === filt) {
        passou = true;
      }
    });
    return passou;
  }
  private retirarMascaras = (valor: any): number => {
    if (typeof valor === "string") {
      const numeroLimpo = valor.replace(/[^-0-9.,]/g, "");
      const split = numeroLimpo.split(".");
      const numeroPonto = split.join("").replace(/,/, ".");
      return parseFloat(numeroPonto);
    } else if (typeof valor === "number") {
      return valor;
    } else {
      return NaN;
    }
  };
  private moutChart(): void {
    //montar grafico
    this.properties.chartDescription = [
      { nome: "Orçado", custo: "00,00", mascara: "R$" },
      { nome: "Comprometido", custo: "00,00", mascara: "R$" },
      { nome: "Disponível", custo: "00,00", mascara: "R$" },
      { nome: "% Comprometida", custo: "0", mascara: "%" },
      { nome: "% Disponível", custo: "0", mascara: "%" },
    ];
    let totalSum = 0;
    let blueSum = 0;
    let greenSum = 0;
    let yellowSum = 0;
    let restSum = 0;
    this.properties.sumTotal = 0;
    let filter: string[] | undefined;
    if (this.properties.filterTf) {
      filter = this.properties.filterTf.split(", ");
    }
    if (
      this.properties.total &&
      this.properties.total !== "" &&
      this.properties.total !== "Nenhum"
    ) {
      this.fileData.forEach((data: any) => {
        if (
          (filter && this.passaFiltro(data[this.properties.filter], filter)) ||
          !filter ||
          filter.length === 0
        ) {
          if (
            !Number.isNaN(this.retirarMascaras(data[this.properties.total]))
          ) {
            totalSum =
              totalSum + this.retirarMascaras(data[this.properties.total]);
          }
          if (!Number.isNaN(this.retirarMascaras(data[this.properties.blue]))) {
            blueSum =
              blueSum + this.retirarMascaras(data[this.properties.blue]);
          }
          if (
            !Number.isNaN(this.retirarMascaras(data[this.properties.green]))
          ) {
            if (this.retirarMascaras(data[this.properties.green]) >= 0) {
              greenSum =
                greenSum + this.retirarMascaras(data[this.properties.green]);
            } else {
              restSum =
                restSum + this.retirarMascaras(data[this.properties.green]);
            }
          }
          if (
            !Number.isNaN(this.retirarMascaras(data[this.properties.yellow]))
          ) {
            yellowSum =
              yellowSum + this.retirarMascaras(data[this.properties.yellow]);
          }
        }
      });
      this.total = totalSum;
      const totalPorcent = totalSum + restSum * -1;
      this.properties.porcentBlue = (100 * blueSum) / totalPorcent;
      this.properties.porcentGreen = (100 * greenSum) / totalPorcent;
      this.properties.porcentYellow = (100 * yellowSum) / totalPorcent;
      this.properties.porcentRed = (100 * (restSum * -1)) / totalPorcent;
      this.properties.totalPorcent = 100 + (restSum * -1 * 100) / totalSum;
      const auxChartDescription: any[] = [];
      this.properties.chartDescription.forEach((data: any, index: number) => {
        if (index === 0) {
          auxChartDescription.push({
            nome: "Orçado",
            custo: aplicarMascara(totalSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 1) {
          auxChartDescription.push({
            nome: "Comprometido",
            custo: aplicarMascara(yellowSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 2) {
          auxChartDescription.push({
            nome: "Disponível",
            custo: aplicarMascara(greenSum.toFixed(2).split(".").join(",")),
            mascara: "R$",
          });
        } else if (index === 3) {
          auxChartDescription.push({
            nome: "% Comprometida",
            custo:
              this.properties.porcentYellow.toFixed(2).split(".")[1] === 0
                ? this.properties.porcentYellow.toFixed(0)
                : this.properties.porcentYellow.toFixed(2),
            mascara: "%",
          });
        } else if (index === 4) {
          auxChartDescription.push({
            nome: "% Disponível",
            custo:
              this.properties.porcentGreen.toFixed(2).split(".")[1] === 0
                ? this.properties.porcentGreen.toFixed(0)
                : this.properties.porcentGreen.toFixed(2),
            mascara: "%",
          });
        }
      });
      this.properties.chartDescription = auxChartDescription;
    }

    function aplicarMascara(valor: string): string {
      const valorNumerico = parseFloat(valor.replace(",", "."));
      const valorFormatado = valorNumerico.toLocaleString("pt-BR");

      return valorFormatado;
    }
  }
  private mountTable(): void {
    let filter: string[] | undefined;
    if (this.properties.filterTf) {
      filter = this.properties.filterTf.split(", ");
    }
    const auxTable: any[] = [];
    if (this.properties.headerTable === "Dados em Colunas") {
      //header com o selectedColumns
      this.headerTable = this.selectedColumns;
      this.properties.headerData = this.headerTable;
    } else {
      //header com o titulo
      this.headerTable = [this.properties.title];
      this.properties.headerData = this.headerTable;
      auxTable.push(this.selectedColumns);
    }

    this.fileData.forEach((data: any) => {
      const auxRow: any[] = [];
      this.selectedColumns.forEach((selected: string) => {
        if (
          (filter && this.passaFiltro(data[this.properties.filter], filter)) ||
          !filter ||
          filter.length === 0
        ) {
          if (data[selected] + "") {
            auxRow.push(data[selected]);
          } else {
            auxRow.push("");
          }
        }
      });
      if (auxRow.length > 0) {
        auxTable.push(auxRow);
      }
    });

    if (auxTable.length > 0) {
      this.tableData = auxTable;
      this.properties.tableData = auxTable;

      this.loadTemplates();
      this.onPropertyPaneFieldChanged("readFile", null, true);
      this.getPropertyPaneConfiguration();
      this.salvarConfiguracoes();
    }
  }

  private async readThisFile(): Promise<void> {
    try {
      this.fileData = [];

      await this.onInit();
    } catch (error) {
      console.error("Erro ao ler o arquivo: ", error);
    }
  }

  async carregarArquivo(): Promise<void> {
    try {
      const response = await fetch(
        this.properties.filePickerResult.fileAbsoluteUrl,
        {
          method: "GET",
          headers: {
            accept: "application/json;odata=verbose",
            contentType: "application/json;odata=verbose",
          },
        }
      );

      if (response.ok) {
        const arrayBuffer = await response.arrayBuffer(); // tem que fazer isso pra processar o arquivo XLSX
        const data = new Uint8Array(arrayBuffer);
        const workbook = XLSX.read(data, { type: "array" }); // pra ler e pegar as planilhas do arquivo

        const planilhaX = workbook.Sheets[workbook.SheetNames[0]]; // pra acessar a planilha desejada do arquivo

        //const jsonData: any[] = XLSX.utils.sheet_to_json(planilhaX); // converter dados da planilha em json

        const jsonData: any[] = XLSX.utils.sheet_to_json(planilhaX, {
          raw: false, // isso garante que as datas sejam interpretadas corretamente
        });

        this.fileData = jsonData;
        console.log("DADOS JSON: ", jsonData);
        if (jsonData && jsonData.length > 0) {
          this.columnNames = Object.keys(jsonData[0]);
          this.onPropertyPaneFieldChanged("toggleTable", false, true);
          const auxSelected: any[] = [];
          let naoAddAinda = false;
          this.selectedColumns.forEach((item) => {
            this.columnNames.forEach((column) => {
              auxSelected.forEach((add) => {
                if (add === column) {
                  naoAddAinda = true;
                }
              });
              if (item === column && !naoAddAinda) {
                auxSelected.push(column);
              } else {
                if (this.properties[column] && !naoAddAinda) {
                  auxSelected.push(column);
                }
              }
            });
          });
          this.selectedColumns = auxSelected;
          this.salvarConfiguracoes();
        }
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  }

  public render(): void {
    const element: React.ReactElement<IWebpartTabelaPadraoProps> =
      React.createElement(WebpartTabelaPadrao, {
        isDarkTheme: this._isDarkTheme,
        environmentMessage: this._environmentMessage,
        hasTeamsContext: !!this.context.sdks.microsoftTeams,
        userDisplayName: this.context.pageContext.user.displayName,
        fileData: this.fileData,
        headerTable: this.headerTable,
        tableData: this.properties.tableData ? this.properties.tableData : [],
        pagination: this.properties.pagination,
        headerType: this.properties.headerTable,
        chartTitle: this.properties.chartTitle,
        toggleTable: this.properties.toggleTable,
        chartDescription: this.properties.chartDescription,
        porcentBlue: this.properties.porcentBlue,
        porcentYellow: this.properties.porcentYellow,
        porcentGreen: this.properties.porcentGreen,
        porcentRed: this.properties.porcentRed,
        totalPorcent: this.properties.totalPorcent,
        total: this.total,
        showFilter: this.properties.showFilter,
        userFilter1: this.properties.userFilter1,
        userFilter2: this.properties.userFilter2,
        userFilterData: this.properties.userFilterData,
        selectedColumns: this.selectedColumns,
        filter: this.properties.filter,
        filterTf: this.properties.filterTf,
        title: this.properties.title,
        totalProperties: this.properties.total,
        blueProperties: this.properties.blue,
        greenProperties: this.properties.green,
        yellowProperties: this.properties.yellow,
        fontSize: this.properties.fontSize,
      });

    ReactDom.render(element, this.domElement);
  }

  private carregarConfiguracoes(): void {
    if (this.properties.fileData) {
      this.fileData = this.properties.fileData;
      this.columnNames = Object.keys(this.properties.fileData[0]);
      this.loadTemplates();

      if (this.showTable) {
        this.moutChart();
      }
    }
    if (this.properties.selectedColumns) {
      this.selectedColumns = this.properties.selectedColumns;
    }
    if (this.properties.headerData) {
      this.headerTable = this.properties.headerData;
    }
    if (this.properties.tableData) {
      this.tableData = this.properties.tableData;
    }
    if (this.properties.chartFields) {
      this.chartFields = this.properties.chartFields;
    }
  }

  protected onPropertyPaneFieldChanged(
    propertyPath: string,
    oldValue: any,
    newValue: any
  ): void {
    if (propertyPath === "showFilter") {
      this.loadTemplates();
    }
    if (propertyPath === "readFile") {
      this.loadTemplates();
    }
    if (propertyPath === "showFilter" && newValue === false) {
      this.salvarConfiguracoes();
    }
    if (
      (propertyPath === "userFilter1" || propertyPath === "filterTf") &&
      newValue !== oldValue
    ) {
      let filter: string[] | undefined;
      const auxUserFilters: string[] = ["Nenhum"];
      if (this.properties.filterTf) {
        filter = this.properties.filterTf.split(", ");
      }
      this.fileData.map((data: any) => {
        if (
          (filter && this.passaFiltro(data[this.properties.filter], filter)) ||
          !filter ||
          filter.length === 0
        ) {
          if (auxUserFilters.length === 0) {
            auxUserFilters.push(data[this.properties.userFilter1] + "");
          } else {
            let achou = false;
            auxUserFilters.forEach((filter: string) => {
              if (filter === data[this.properties.userFilter1] + "") {
                achou = true;
              }
            });
            if (!achou) {
              auxUserFilters.push(data[this.properties.userFilter1] + "");
            }
          }
        }
      });
      this.properties.userFilterData = auxUserFilters;
    }
    if (propertyPath === "headerTable") {
      this.mountTable();
    }
    if (
      (propertyPath === "toggleTable" || propertyPath === "total") &&
      newValue !== oldValue
    ) {
      if (this.properties.toggleTable) {
        const cbFilter: IPropertyPaneDropdownOption[] = [];
        this.columnNames.map(function (item) {
          cbFilter.push({
            key: item,
            text: item,
          });
        });
        cbFilter.unshift({
          key: "Nenhum",
          text: "Nenhum",
        });
        this.chartFields = [
          PropertyPaneTextField("chartTitle", { label: "Título do Gráfico" }),
          PropertyPaneTextField("fontSize", {
            label: "Tamanho da fonte",
          }),
          PropertyPaneDropdown("total", {
            label: "Orçado total (100%)*",
            options: cbFilter,
          }),
          PropertyPaneDropdown("blue", {
            label: "Faturado",
            options: cbFilter,
          }),
          PropertyPaneDropdown("green", {
            label: "Disponível",
            options: cbFilter,
          }),
          PropertyPaneDropdown("yellow", {
            label: "Comprometido",
            options: cbFilter,
          }),
          PropertyPaneLabel("warningChart", {
            text:
              this.properties.total &&
              this.properties.total !== "" &&
              this.properties.total !== "Nenhum"
                ? ""
                : "Gráficos sem valor total não podem ser montados",
          }),
          PropertyPaneButton("moutChart", {
            text: "Montar Gráfico",
            onClick: this.moutChart.bind(this),
            disabled: this.properties.filePickerResult ? false : true,
          }),
        ];
      } else {
        this.chartFields = [PropertyPaneLabel("labelTable", { text: "" })];
      }
      this.salvarConfiguracoes();
    }

   if (propertyPath === "filter" && newValue !== oldValue) {
      const auxStr: string[] = [];
      if (this.properties.filter !== "Nenhum") {
        this.filterTfText = "";
        this.fileData.map((item: any) => {
          if (auxStr.length === 0) {
            auxStr.push(item[this.properties.filter]);
          } else {
            let achou = false;
            auxStr.map((rotulo: string) => {
              if (rotulo === item[this.properties.filter]) {
                achou = true;
              }
            });
            if (!achou) {
              auxStr.push(item[this.properties.filter]);
            }
          }
        });
      }
      if (auxStr.length > 0) {
        this.properties.filterTf = auxStr.sort().join(", ");
        this.filterTfText = auxStr.sort().join(", ");
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      } else {
        this.properties.filterTf = "";
        this.filterTfText = "";
        this.loadTemplates();
        this.getPropertyPaneConfiguration();
        this.salvarConfiguracoes();
      }
    }
    if (this.columnNames && this.columnNames.length > 0) {
      this.columnNames.forEach((item) => {
        if (propertyPath + "" === item + "") {
          const auxColumns: string[] = [];
          if (this.selectedColumns && this.selectedColumns.length > 0) {
            let achou = false;
            this.selectedColumns.forEach((selected) => {
              if (selected + "" === propertyPath + "") {
                achou = true;
              } else {
                auxColumns.push(selected);
              }
            });
            if (!achou) {
              auxColumns.push(propertyPath + "");
            }
          } else {
            auxColumns.push(propertyPath + "");
          }
          this.selectedColumns = auxColumns;
          this.properties.selectedColumns = this.selectedColumns;
        }
      });
    }
    super.onPropertyPaneFieldChanged(propertyPath, oldValue, newValue);
  }

  protected async onInit(): Promise<void> {
    this.showTable = this.properties.toggleTable;
    this.carregarConfiguracoes();

    if (
      this.properties.filePickerResult &&
      this.properties.filePickerResult.fileAbsoluteUrl
    ) {
      await this.carregarArquivo(); // Aguarde a conclusão da leitura do arquivo
      this.carregarConfiguracoes();
      this.loadTemplates();
      this.onPropertyPaneFieldChanged("readFile", null, true);
      this.getPropertyPaneConfiguration();

      if(this.properties.userFilter1){
        this.onPropertyPaneFieldChanged("userFilter1", null, this.properties.userFilter1)
      }

      if(this.properties.userFilter2){
        this.onPropertyPaneFieldChanged("userFilter2", null, this.properties.userFilter2)
      }

      this.salvarConfiguracoes();
      this.mountTable();
    }
    this.properties.headerTable =
      this.properties.headerTable && this.properties.headerTable !== ""
        ? this.properties.headerTable
        : "Dados em Colunas";
    return super.onInit();
  }

  protected onThemeChanged(currentTheme: IReadonlyTheme | undefined): void {
    if (!currentTheme) {
      return;
    }

    this._isDarkTheme = !!currentTheme.isInverted;
    const { semanticColors } = currentTheme;

    if (semanticColors) {
      this.domElement.style.setProperty(
        "--bodyText",
        semanticColors.bodyText || null
      );
      this.domElement.style.setProperty("--link", semanticColors.link || null);
      this.domElement.style.setProperty(
        "--linkHovered",
        semanticColors.linkHovered || null
      );
    }
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse("1.0");
  }

  public getUrlModification(): void {
    const url = this.properties.filePickerResult.fileAbsoluteUrl;
    const parts = url.split("/");
    const lastPart = parts[parts.length - 1];

    const regex =
      /https:\/\/bancobradesco\.sharepoint\.com\/teams\/5800-pa\/(.*?)(?=\/[^/]+$)/;

    const match = url.match(regex);

    if (match) {
      const listName = match[1];
      const fullUrl = `https://bancobradesco.sharepoint.com/teams/5800-pa/_api/web/GetFolderByServerRelativeUrl('${listName}')/files?$filter=Name eq '${lastPart}'`;

      this.getModification(fullUrl);
    }
  }

  async getModification(url: string) {
    try {
      const response = await fetch(url, {
        method: "GET",
        headers: {
          accept: "application/json;odata=verbose",
          contentType: "application/json;odata=verbose",
        },
      });

      if (response.ok) {
        const data = await response.json();
        const results = data.d.results;
        results.forEach((item: any) => {
          const timeLastModified = item.TimeLastModified;

          if (
            !this.lastModifiedTime ||
            timeLastModified > this.lastModifiedTime
          ) {
            this.lastModifiedTime = timeLastModified;
            this.readThisFile();
          }
        });
      } else {
        console.error("Falha na requisição");
      }
    } catch (error) {
      console.error("Erro na requisição: ", error);
    }
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription,
          },
          groups: [
            {
              groupName: "Arquivo:",
              groupFields: [
                PropertyFieldFilePicker("filePicker", {
                  onPropertyChange: this.onPropertyPaneFieldChanged.bind(this),
                  properties: this.properties,
                  onSave: (e: any) => {
                    this.properties.filePickerResult = e;

                    this.getUrlModification();
                  },
                  onChanged: (e: any) => {
                    this.properties.filePickerResult = e;
                  },
                  key: "filePickerId",
                  buttonLabel: this.properties.filePickerResult
                    ? "Mudar Arquivo"
                    : "Escolher Arquivo",
                  label: "Arquivo Excel",
                  context: this.context as any,
                  filePickerResult: this.properties.filePickerResult,
                }),
                PropertyPaneButton("readFile", {
                  text: "Ler Arquivo Excel",
                  buttonType: PropertyPaneButtonType.Primary,
                  onClick: this.readThisFile.bind(this),
                  disabled: this.properties.filePickerResult ? false : true,
                }),
              ],
            },
            {
              groupName: "Cabeçalho da Tabela",
              groupFields: [
                PropertyPaneChoiceGroup("headerTable", {
                  label: "Tipo",
                  options: [
                    { key: "Título", text: "Título" },
                    {
                      checked: true,
                      key: "Dados em Colunas",
                      text: "Dados em Colunas",
                    },
                  ],
                }),
                this.properties.headerTable === "Título"
                  ? PropertyPaneTextField("title", {
                      label: "Título da Tabela",
                    })
                  : PropertyPaneLabel("label", { text: "" }),
              ],
            },
            {
              groupName: "Colunas",
              groupFields: [
                ...this.columnNames.map((item) =>
                  PropertyPaneCheckbox(item, {
                    text: item,
                  })
                ),
              ],
            },
            {
              groupName: "Filtros",
              groupFields: this.filters,
            },
            {
              groupName: "Paginação",
              groupFields: [
                PropertyPaneTextField("pagination", {
                  label: "Quantidade de linhas por página:",
                  description: "Campo numérico",
                  value: this.paginationTfText,
                }),
              ],
            },
            {
              groupName: "Gráfico",
              groupFields: [
                PropertyPaneLabel("exclusiveLabel", {
                  text: "(exclusivo para tabelas de Orçamentos e Contratos)",
                }),
                PropertyPaneToggle("toggleTable", { label: "Mostrar Gráfico" }),
                ...this.chartFields,
              ],
            },
            {
              groupName: "",
              groupFields: [
                PropertyPaneButton("moutTable", {
                  text: "Montar Tabela",
                  onClick: this.mountTable.bind(this),
                  disabled: this.properties.filePickerResult ? false : true,
                }),
                PropertyPaneButton("salvarConfiguracoes", {
                  text: "Salvar Configurações",
                  buttonType: PropertyPaneButtonType.Primary,
                  onClick: this.salvarConfiguracoes.bind(this),
                }),
              ],
            },
          ],
        },
      ],
    };
  }
}
